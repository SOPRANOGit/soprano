@ECHO OFF

echo.
echo ========== UPDATEMANIFEST.XML GENERATOR ==========
echo.
echo This will generate updatemanifest.xml in current folder %CD%
echo Make sure the current folder is directly under in C:\ drive and Adeko installed properly.
echo Don't forget to delete nameless and extensionless files before you continue or else AdekoUpdater will be stuck.
echo.
echo This batch works on the following command example but detects folders automaticly.
echo "AdekoUpdater.exe --server=http://localhost --folder=C:/Firmam --generate"
echo.
echo visit for more information 
echo http://wiki.adeko.com:8090/display/ADY/AdekoUpdater
echo.
echo ==========


pause

echo.
Taskkill /F /IM icad.exe
Taskkill /F /IM dnetfaces.exe
Taskkill /F /IM billtool.exe
Taskkill /F /IM browse.exe
Taskkill /F /IM CabinCut.exe
Taskkill /F /IM cfp.exe
Taskkill /F /IM cfs.exe
Taskkill /F /IM AdekoUpdater.exe
Taskkill /F /IM makensis.exe
Taskkill /F /IM adeko_render_viewer.exe
echo ==========

@ECHO ON
FOR /F "skip=2 tokens=2,*" %%A IN ('reg.exe query "HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Adeko 17" /v "Install_Dir"') DO set "path=%%B"
START "%path%\" AdekoUpdater.exe --server=http://localhost --folder=%CD% --generate
@ECHO OFF

echo.
echo ========== DONE
echo Please check UpdateManifest.xml file under %CD%
echo Don't forget to manually remove lines from xml for files with .db extensions or else AdekoUpdater will be stuck.

echo.
echo ========== Exit
pause