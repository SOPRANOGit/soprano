;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun AL15_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "AL15" "_" x "KANATSETS")) AL15_BKANATSETS) ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL15" "_" x "_MAT_LAYER")) "ALUMINIUM_FRAME") ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL15" "_" x "_GLASS_MAT_LAYER")) "ALUMINIUM_DOOR_GLASS") ) (list "B" "K" "J" "F" "T"))

(defun AL16_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "AL16" "_" x "KANATSETS")) AL16_BKANATSETS) ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL16" "_" x "_MAT_LAYER")) "ALUMINIUM_FRAME") ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL16" "_" x "_GLASS_MAT_LAYER")) "ALUMINIUM_DOOR_GLASS") ) (list "B" "K" "J" "F" "T"))

(defun AL17_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "AL17" "_" x "KANATSETS")) AL17_BKANATSETS) ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL17" "_" x "_MAT_LAYER")) "ALUMINIUM_FRAME") ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL17" "_" x "_GLASS_MAT_LAYER")) "ALUMINIUM_DOOR_GLASS") ) (list "B" "K" "J" "F" "T"))

(defun AL18_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "AL18" "_" x "KANATSETS")) AL18_BKANATSETS) ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL18" "_" x "_MAT_LAYER")) "ALUMINIUM_FRAME") ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL18" "_" x "_GLASS_MAT_LAYER")) "ALUMINIUM_DOOR_GLASS") ) (list "B" "K" "J" "F" "T"))

(defun AL19_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "AL19" "_" x "KANATSETS")) AL19_BKANATSETS) ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL19" "_" x "_MAT_LAYER")) "ALUMINIUM_FRAME") ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL19" "_" x "_GLASS_MAT_LAYER")) "ALUMINIUM_DOOR_GLASS") ) (list "B" "K" "J" "F" "T"))

(defun AL21_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "AL21" "_" x "KANATSETS")) AL21_BKANATSETS) ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL21" "_" x "_MAT_LAYER")) "ALUMINIUM_FRAME") ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL21" "_" x "_GLASS_MAT_LAYER")) "ALUMINIUM_DOOR_GLASS") ) (list "B" "K" "J" "F" "T"))

(defun AL30_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "AL30" "_" x "KANATSETS")) AL30_BKANATSETS) ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL30" "_" x "_MAT_LAYER")) "ALUMINIUM_FRAME") ) (list "B" "K" "J" "F" "T"))
(mapcar '(lambda (x) (set (read (strcat "AL30" "_" x "_GLASS_MAT_LAYER")) "ALUMINIUM_DOOR_GLASS") ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun GR31_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "GR31" "_" x "KANATSETS")) GR31_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;

(defun GC31_BKANATSETS (x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "GC31" "_" x "KANATSETS")) GC31_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun GR3R_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "GR3R" "_" x "KANATSETS")) GR3R_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;

(defun GC3R_BKANATSETS (x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "GC3R" "_" x "KANATSETS")) GC3R_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun SW32_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "SW32" "_" x "KANATSETS")) SW32_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;

(defun SC32_BKANATSETS (x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "SC32" "_" x "KANATSETS")) SC32_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun SW3R_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "SW3R" "_" x "KANATSETS")) SW3R_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;

(defun SC3R_BKANATSETS (x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "SC3R" "_" x "KANATSETS")) SC3R_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun JY33_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "JY33" "_" x "KANATSETS")) JY33_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun JY3R_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "JY3R" "_" x "KANATSETS")) JY3R_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun LG30_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "LG30" "_" x "KANATSETS")) LG30_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun LG3R_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "LG3R" "_" x "KANATSETS")) LG3R_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun LG40_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "LG40" "_" x "KANATSETS")) LG40_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun LG4R_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "LG4R" "_" x "KANATSETS")) LG4R_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun BL50_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "BL50" "_" x "KANATSETS")) BL50_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun BL53_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "BL53" "_" x "KANATSETS")) BL53_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun BL51_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "BL51" "_" x "KANATSETS")) BL51_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun SF70_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "SF70" "_" x "KANATSETS")) SF70_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


 (defun FR70_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "FR70" "_" x "KANATSETS")) FR70_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun FR73_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "FR73" "_" x "KANATSETS")) FR73_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun NT80_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "NT80" "_" x "KANATSETS")) NT80_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun NT55_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "NT55" "_" x "KANATSETS")) NT55_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun IN10_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "IN10" "_" x "KANATSETS")) IN10_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun IN11_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "IN11" "_" x "KANATSETS")) IN11_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun IN20_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "IN20" "_" x "KANATSETS")) IN20_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun NE30_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "NE30" "_" x "KANATSETS")) NE30_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun FR34_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "FR34" "_" x "KANATSETS")) FR34_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun LC38_BKANATSETS (x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "LC38" "_" x "KANATSETS")) LC38_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun LK33_AKANATSETS (x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list "PVC_" "PVC_" "PVC_" "PVC_"))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(mapcar '(lambda (x) (set (read (strcat "LK33" "_" x "KANATSETS")) LK33_AKANATSETS) ) (list "A" "C" "D" "E" "H" "L" "M" "R" "S" "Z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;