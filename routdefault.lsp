(setq rout_chain_name0 (xstr "Bottom cover"))
(setq rout_chain_name1 (xstr "Botton cover material"))
(setq rout_chain_name2 (xstr "Top cover"))
(setq rout_chain_name3 (xstr "Top cover material"))
(setq rout_chain_name4 (xstr "Column cover"))
(setq rout_chain_name5 (xstr "Column cover material"))
(setq rout_chain_name6 (xstr "Bottom cover edge band ")) ; rout_chains_display_index ten kapat�ld�
(setq rout_chain_name7 (xstr "Gola Profile color"))
(setq rout_chain_name8 (xstr "Base Unit Body Material"))
(setq rout_chain_name9 (xstr "Aluminium cover profile color")) ; rout_chains_display_index ten kapat�ld�
(setq rout_chain_name10 (xstr "Aluminium cover glass color"))
(setq rout_chain_name11 (xstr "Classic cover glass color"))
(setq rout_chain_name12 (xstr "Plinth Material"))
(setq rout_chain_name13 (xstr "Base Height"))
(setq rout_chain_name14 (xstr "Top cover edge band")) ; rout_chains_display_index ten kapat�ld�
(setq rout_chain_name15 (xstr "Project Type"))
(setq rout_chain_name16 (xstr "Top cover handle type"))
(setq rout_chain_name17 (xstr "Bottom cover handle type"))
(setq rout_chain_name18 (xstr "Column cover handle type"))
(setq rout_chain_name19 (xstr "Column cover edge band")) ; rout_chains_display_index ten kapat�ld�
(setq rout_chain_name20 (xstr "Glass cover"))
(setq rout_chain_name21 (xstr "Glass cover material"))
(setq rout_chain_name22 (xstr "Wall Unit Body Material"))
(setq rout_chain_name23 (xstr "Tall Unit Body Material"))
(setq rout_chain_name24 (xstr "Project Feature"))
(setq rout_chain_name25 (xstr "Hinge"))
(setq rout_chain_name26 (xstr "Drawer Rail"))
(setq rout_chain_name27 (xstr "Glass Shelf Material"))

(setq firm_mat_prefix "")
(setq rout_handle_askForModuleType nil)
(setq c:matmgr->selective_doors T)
(setq c:matmgr->selective_vside T)
(setq c:matmgr->selective_PLINTHS T)
(setq rout_chains_display_index (list 0 1 17 2 3 16 4 5 18 20 21 11 10 7 8 22 23 27 12 13 24 25 26 15))
(setq rout_chains_base_unit_change_event_index (list 0 1 17))
(setq rout_chains_wall_unit_change_event_index (list 2 3 16))
(setq rout_chains_tall_unit_change_event_index (list 4 5 18))
;routchain2 yok ��nk� handle i�in  gerek yok

(setq g_SopranoEntegreKulpList (list "ENTEGRE" "KE0103" "KE0113" "KE0214" "KE0203" "KE0215" "KE0206" "KE0216" "KE0314" "KE0303" "KE0315"))
(setq g_sopranoSimetrikKulpList (list "KA1806403" "KA1816003" "KA1828803" "KA1806405" "KA1816005" "KA1828805"))

;(c:cdoor->c:handle)
(defun routchain0 ( / tempData)
	(if (setq tempData (rout_chain_seperated_doors_V2 (list "princ" "alt") 0 nil nil))
		(progn
			(CreateXRef "AD_BASE_UNIT_DOOR_FUNC$$" (convertStr (getnth 1 tempData)))
			(CreateXRef "AD_TALL_UNIT_DOOR_FUNC$$" (convertStr (getnth 1 tempData)))
			
			(set_tile "sel1" "")
			(setq rout_sel1 "")
			(set_tile "sel2" "")
			(setq rout_sel2 "")
			(set_tile "sel17" "")
			(setq rout_sel17 "")
			(set_tile "sel4" (getnth 0 tempData))
			(setq rout_sel4 (getnth 0 tempData))
			
			(set_tile "sel5" "")
			(setq rout_sel5 "")
			(set_tile "sel18" "")
			(setq rout_sel18 "")
		)
	)
)

(defun routchain1 ( / baseUnitsSet baseUnit selectedMat)
	(if (equal (get_tile "sel0") "")
		(alert  (xstr "Choose cover first"))
		(progn
			(setmatmgrsearchfilter (strcat (get_tile "sel0")  "DOOR"))
			
			(setq selectedMat (car (choosematerial)))
			
			(if (and selectedMat (not (equal selectedMat "")))
				(progn
					(if (equal (get_tile "sel5") "") (progn (set_tile "sel5" selectedMat) (setq rout_sel5 selectedMat)))
					(setq rout_sel1 selectedMat)
					(set_tile "sel1" selectedMat)
				)
			)
			(setq forceChangeBaseUnitAttiributes T)
		)
	)	
)

;routchain2 yok ��nk� handle i�in  gerek yok

(defun routchain2 ( / tempres tempData)
	(if (setq tempData (rout_chain_seperated_doors_V2 (list "princ" "ust") 2 nil nil))
		(progn
			(CreateXRef "AD_WALL_UNIT_DOOR_FUNC$$" (convertStr (getnth 1 tempData)))
			(set_tile "sel3" "")
			(setq rout_sel3 "")
			(set_tile "sel16" "")
			(setq rout_sel16 "")
		)
	)
)

(defun routchain3 ( / wallUnit wallUnitsSet selectedMat)		
	(if (equal (get_tile "sel2") "")
		(alert  (xstr "Choose cover first"))
		(progn
			(setmatmgrsearchfilter (strcat (get_tile "sel2")  "DOOR"))
			
			(setq selectedMat (car (choosematerial)))
			
			(if (and selectedMat (not (equal selectedMat "")))
				(progn
					(setq rout_sel3 selectedMat)
					(set_tile "sel3" selectedMat)
				)
			)
			(setq forceChangeWallUnitAttiributes T)
		)
	)
)

(defun routchain4 ( / tempres)			
	(rout_arrange_doors "")	
	(if (setq tempData (rout_chain_seperated_doors_V2 (list "princ" "boy") 4 nil nil))
		(progn
			(CreateXRef "AD_TALL_UNIT_DOOR_FUNC$$" (convertStr (getnth 1 tempData)))
			
			(set_tile "sel5" "")
			(setq rout_sel5 "")
			(setq rout_sel18 "")
			(set_tile "sel18" "")
		)
	)
	(rout_arrange_doors "")
)

(defun routchain5 ( / tempres selectedKapakMat tallUnitsSet tallUnit selectedMat)		
	(if (equal (get_tile "sel4") "")
		(alert  (xstr "Choose cover first"))
		(progn
			(setmatmgrsearchfilter (strcat (get_tile "sel4")  "DOOR"))
			
			(setq selectedMat (car (choosematerial)))
			
			(if (and selectedMat (not (equal selectedMat "")))
				(progn
					(setq rout_sel5 selectedMat)
					(set_tile "sel5" selectedMat)
				)
			)
			(setq forceChangeTallUnitAttiributes T)
		)
	)
)

(defun routchain6 ( / kenarbandimat tempres selectedMat)		
	(princ)
)




(defun getDOOREDGESTRIPCODE ()
	(ifnull (getXRef "DOOR_EDGESTRIP_CODE") "")
)

(defun routchain7 ()
	(rout_chain_seperated_layer_mat (list "GOLAPROFILE" "GOLAVERTICALPROFILE") "GOLAPROFILE" 7 nil nil)
)

(defun routchain8 ()
	(rout_chain_seperated_layer_mat (list "CAB_BODY_BASE") "CAB_BODY" 8 nil nil)
)

(defun routchain9 ()
	(rout_chain_seperated_layer_mat (list "ALUMINIUM_FRAME") "ALUMINIUM_FRAME" 9 nil nil)
)

(defun routchain10 ()
	(rout_chain_seperated_layer_mat (list "ALUMINIUM_DOOR_GLASS") "ALUMINIUM_DOOR_GLASS" 10 nil nil)
)

(defun routchain11 ()
	(rout_chain_seperated_layer_mat (list "CAB_DOOR_GLASS") "CAB_DOOR_GLASS" 11 nil nil)
	
)

(defun routchain12 ()
	(soprano_choosePlinthMatType)
	(setq rout_sel12 (soprano_getPlinthMatType))
	(soprano_removePlinths)
	(genAllModuls)
)

(defun routchain13 ()
	(soprano_choosePlinthH)
	(setq rout_sel13 (rtos ad_bazah 2 0))
	(soprano_removePlinths)
	(genAllModuls)
)

(defun routchain14 ( / kenarbandimat tempres selectedMat)		
	(princ)
)

(defun routchain15 ( / optionList)
	(setq optionList (list (list "SOPRANO_OFFER" (Xstr "Offering")) (list "SOPRANO_ORDER" (Xstr "Order")) ))
	(setq rout_sel15 (rout_fastchooser optionList "SOPRANO_PROJECT_TYPE" (Xstr "")))
)

(defun routchain24 ( / optionList)
	(setq optionList (list (list "PERAKENDE" (Xstr "Perakende")) (list "TOPLU_IS" (Xstr "Toplu Is"))))
	(setq rout_sel24 (rout_fastchooser optionList "SOPRANO_PROJECT_FEATURE" ""))

	(if (equal (getxref "SOPRANO_PROJECT_FEATURE") "PERAKENDE")
		(progn	
			(setq 
				rout_sel25 "HT"
				rout_sel26 "HT"
			)

			(set_tile "sel25" "HT")
			(set_tile "sel26" "HT")

			(CreateXRef "SOPRANO_HINGE_TYPE" "HT")
			(CreateXRef "SOPRANO_DRAWER_RAIL_TYPE" "HT")
		)
		(progn
			(setq 
				rout_sel25 ""
				rout_sel26 ""
			)

			(set_tile "sel25" "")
			(set_tile "sel26" "")

		)
	)
)

(defun routchain25 ( / optionList)
	(if (equal rout_sel24 "")
		(alert (xstr "Choose Project Feature first!"))
		(progn
			(setq 
				optionList
				(if (equal (getxref "SOPRANO_PROJECT_FEATURE") "PERAKENDE")
			 		(list (list "HT" "Hettich"))
			 		(list (list "HT" "Hettich") (list "BL" "Blum") (list "HF" "Hafele"))
				)
			)

			(setq rout_sel25 (rout_fastchooser optionList "SOPRANO_HINGE_TYPE" (Xstr "")))
		)
	)
)

(defun routchain26 ( / optionList)
	(if (equal rout_sel24 "")
		(alert (xstr "Choose Project Feature first!"))
		(progn
			(setq 
				optionList
				(if (equal (getxref "SOPRANO_PROJECT_FEATURE") "PERAKENDE")
					(list (list "HT" "Hettich"))
					(list (list "HT" "Hettich") (list "BL" "Blum") (list "HF" "Hafele"))
				)
			)

			(setq rout_sel26 (rout_fastchooser optionList "SOPRANO_DRAWER_RAIL_TYPE" (Xstr "")))
		)
	)
)

(defun routchain16 ( / tempData) ;�st kulp
	(loadkulps T)
	(rout_arrange_handles (strcat (get_tile "sel2") "_HANDLE_MODELS"))
	(setq rout_sel16 (getnth 0 (setq tempData (nesne_yer_AssociationModule "kulp" (Xstr "Handle Types") nil 1))))
	
	(if tempData
		(progn
			(CreateXRef "AD_WALL_UNIT_HANDLE_RET$$" (convertStr (getnth 1 tempData)))
			(CreateXRef "AD_WALL_UNIT_HANDLE_MLOFL$$" (convertStr (getnth 2 tempData)))
		)
	)
	
	(rout_arrange_handles "")
)
(defun routchain17 ( / tempData) ;alt kulp
	(loadkulps T)
	(rout_arrange_handles (strcat (get_tile "sel0") "_HANDLE_MODELS"))		
	(setq rout_sel17 (getnth 0 (setq tempData (nesne_yer_AssociationModule "kulp" (Xstr "Handle Types") nil 2))))
	(if tempData
		(progn
			(CreateXRef "AD_BASE_UNIT_HANDLE_RET$$" (convertStr (getnth 1 tempData)))
			(CreateXRef "AD_BASE_UNIT_HANDLE_MLOFL$$" (convertStr (getnth 2 tempData)))
		)
	)
	(if (equal (get_tile "sel18") "") 
		(progn
			(set_tile "sel18" rout_sel17)
			
			(if tempData
				(progn
					(CreateXRef "AD_TALL_UNIT_HANDLE_RET$$" (convertStr (getnth 1 tempData)))
					(CreateXRef "AD_TALL_UNIT_HANDLE_MLOFL$$" (convertStr (getnth 2 tempData)))
				)
			)
		)
	)
	(rout_arrange_handles "")
)
(defun routchain18 ( / tempData) ;boy kulp
	(loadkulps T)
	(rout_arrange_handles (strcat (get_tile "sel4") "_HANDLE_MODELS"))
	(setq rout_sel18 (getnth 0 (setq tempData (nesne_yer_AssociationModule "kulp" (Xstr "Handle Types") nil 3))))

	(if tempData
		(progn
			(CreateXRef "AD_TALL_UNIT_HANDLE_RET$$" (convertStr (getnth 1 tempData)))
			(CreateXRef "AD_TALL_UNIT_HANDLE_MLOFL$$" (convertStr (getnth 2 tempData)))
		)
	)
	(rout_arrange_handles "")
)

(defun routchain19 ( / kenarbandimat tempres selectedMat)
	(princ)
)

(defun routchain20 () 
	(rout_arrange_doors "GLASS")
	(rout_chain_seperated_doors (list "SetGlsKapakName" "tumcamli") 20 nil nil)		
	(rout_arrange_doors "")
)

(defun routchain21 ( / selectedMat) 
	(if (equal (get_tile "sel20") "")
		(alert  (xstr "Choose cover first"))
		(progn
			(setmatmgrsearchfilter (strcat (get_tile "sel20")  "DOOR"))
		
			(setq selectedMat (car (choosematerial)))
			
			(if (and selectedMat (not (equal selectedMat "")))
				(progn
					(SetGlsKapakMat selectedMat)
					(setq rout_sel21 selectedMat)
					(set_tile "sel21" selectedMat)
				)
			)
		)
	)
)

(defun routchain22 ()
	(rout_chain_seperated_layer_mat (list "CAB_BODY_WALL") "CAB_BODY" 22 nil nil)
)

(defun routchain23 ()
	(rout_chain_seperated_layer_mat (list "CAB_BODY_TALL") "CAB_BODY" 23 nil nil)
)

(defun routchain27 ( / optionList)
	(setq optionList (list (list "CLEAR" "CLEAR") (list "FUME" "FUME")))
	(setq rout_sel27 (rout_fastchooser optionList "SOPRANO_GLASS_SHELF_MAT" (Xstr "")))
)

(defun rout_saveChain ()
	(setq iii 0 resultChainList nil)
	(while (eval (read (setq tempchainname (strcat "rout_chain_name" (rtos iii 2 0)))))
			(setq resultChainList (cons (ifnull (get_tile (strcat "sel" (rtos iii 2 0))) "") resultChainList))
			(setq  iii (1+ iii))
	)
	(setq resultChainList (reverse resultChainList))	
	(if resultChainList (CreateXRef "ROUT_CHAIN_LIST$$" (convertstr resultChainList)))
	(redraw)	
	(adeforceupdate)
)

(defun showRouterChainDialog_controlComplete ( / iiis tempResult)
	(setq  tempResult (list "r") iiis 0)
	
	(while (eval (read (setq tempchainname (strcat "rout_chain_name" (rtos iiis 2 0)))))					
			(if (eval (read tempchainname))
				(progn					
					(if (or (and rout_chains_display_index (member iiis rout_chains_display_index)) (not rout_chains_display_index)) (setq tempResult (append (list  (eval (read (strcat "rout_sel" (rtos iiis 2 0))))) tempResult )))
				)				
			)
			(setq iiis (1+ iiis))
	)				
	(if (or (member nil tempResult) (member "" tempResult))
			nil
			T
	)    	
)

(defun showRouterChainDialog ( / compDataBase compDataWall compDataTall)
	(if (not (new_dialog "routerchainSelector" (load_dialog "router.dcl"))) (exit))
	(cond 
		((equal (getxref "SOPRANO_PROJECT_TYPE") "SOPRANO_ORDER")
			(mode_tile "15" 1)
		)
		(T
			(mode_tile "15" 0)
			(set_tile "sel15" "SOPRANO_OFFER")
			(CreateXRef "SOPRANO_PROJECT_TYPE" "SOPRANO_OFFER")
		)
	)
	(setq rout_sel15 "SOPRANO_ORDER")
	(mode_tile "accept" 1)
	
	(action_tile "accept" "(rout_saveChain) (mode_tile \"accept\" 1) (disableAllRoutChains) (startChangingAccordingToRouter) (done_dialog 50)")
	
	(rout_loadChain)
	
	(foreach compIndex rout_chains_base_unit_change_event_index
		(setq compDataBase (append compDataBase (list (get_tile (strcat "sel" (itoa compIndex))))))
	)
	(foreach compIndex rout_chains_wall_unit_change_event_index
		(setq compDataWall (append compDataWall (list (get_tile (strcat "sel" (itoa compIndex))))))
	)
	(foreach compIndex rout_chains_tall_unit_change_event_index
		(setq compDataTall (append compDataTall (list (get_tile (strcat "sel" (itoa compIndex))))))
	)
	
	(mode_tile "accept" 0)
	(setq dialogresult (start_dialog))	
	(if (equal dialogresult 50) (if rout_chain_applier (rout_chain_applier)))
)

(defun disableAllRoutChains ( / i)
	(setq i 0)
	(repeat 100
		(mode_tile (rtos i) 1)
		(i++)
	)
)

(defun startChangingAccordingToRouter ( / refList compIndex i changeableBase changeableWall changeableTall)
	(setq refList (read (getXRef "ROUT_CHAIN_LIST$$")))
	(setq changeableBase T changeableWall T changeableTall T)
	(setq i 0)
	(foreach compIndex rout_chains_base_unit_change_event_index
		(if (or (not (equal (getnth i compDataBase) (getnth compIndex refList))) forceChangeBaseUnitAttiributes)
			(if changeableBase
				(progn
					(setq changeableBase nil)
					(changeBaseUnitAttiributes)
				)
			)
		)
		(i++)
	)
	
	(setq i 0)
	(foreach compIndex rout_chains_wall_unit_change_event_index
		(if (or (not (equal (getnth i compDataWall) (getnth compIndex refList))) forceChangeWallUnitAttiributes)
			(if changeableWall
				(progn
					(setq changeableWall nil)
					(changeWallUnitAttiributes)
				)
			)
		)
		(i++)
	)
	
	(setq i 0)
	(foreach compIndex rout_chains_tall_unit_change_event_index
		(if (or (not (equal (getnth i compDataTall) (getnth compIndex refList))) forceChangeTallUnitAttiributes)
			(if changeableTall
				(progn
					(setq changeableTall nil)
					(changeTallUnitAttiributes)
				)
			)
		)
		(i++)
	)
	
	
	(if (apply 'OR (mapcar '(lambda(x) (null x)) (list changeableBase changeableWall changeableTall)))
		(if (not (member (get_tile "sel21") (list nil ""))) (SetGlsKapakMat (get_tile "sel21")))
	)
)

(defun changeBaseUnitAttiributes ( / baseUnitsSet baseUnit tempLast orjFunc ad_KapakDefGrup flin tile_0 mLofL mLofLeval tile_1 ret mlofl ssList tempEnt)
	;kapak modeli de�i�tiriliyor
	(setq doorFunc (existXref "AD_BASE_UNIT_DOOR_FUNC$$"))
	(if doorFunc
		(progn
			(setq 	ad_KapakDefGrup (cadddr (read doorFunc))
									mLofL (read (strcat ad_KapakDefGrup ":mLofL"))
									mLofLeval (eval mLofL))
			(setq flin (nth (caddr (read doorFunc)) mLofLeval))

			(eval (list (cadr flin) (car flin) (caddr flin) (cadddr flin) "alt"))
		)
	)
	(if (not (member (setq tile_0 (get_tile "sel0")) (list nil ""))) (SetKapakName tile_0))
	(removeAllVisibleSide)
	
	;kapak materyali de�i�tiriliyor
	(if (not (member (setq tile_1 (get_tile "sel1")) (list nil "")))
		(progn
			(SetKapakMat tile_1)
			(grx_setmaterial "CAB_DOORS" tile_1)

			(setq ssList
				(convertSStoList (ssget "x" 
									(list
											'(0 . "INSERT")
											'(-4 . "<OR")
												'(8 . "SHELVES")
												'(8 . "PANELS_WALL")
												'(8 . "PANELS_SIDE")
											'(-4 . "OR>")
									)
								)
				)
			)
			(if ssList
				(foreach tempEnt ssList
					(if (member (get_mdata tempEnt '(620 . 1000)) (list "independent_panelwall" "independent_shelf" "independent_side"))
						(set_block_material tempEnt T (dfetch 8 tempEnt) tile_1)
					)
				)
			)

			(if (tblsearch "layer" "LSIDE") (grx_setmaterial "LSIDE" tile_1))
			(if (tblsearch "layer" "RSIDE") (grx_setmaterial "RSIDE" tile_1))
			(if (tblsearch "layer" "PLINTHS") (grx_setmaterial "PLINTHS" tile_1))
			
			(setq baseUnitsSet (convertSStoList (ssget "X" (list '(0 . "INSERT") '(8 . "CAB_BODY_BASE")))))
			
			(foreach baseUnit baseUnitsSet
				(set_block_material baseUnit T "LSIDE" tile_1)
				(set_block_material baseUnit T "RSIDE" tile_1)
				(set_block_material baseUnit T "CAB_DOORS" tile_1)
			)
		)
	)
	
	;kulp modeli de�i�tiriliyor
	(if (and (setq ret (existXref "AD_BASE_UNIT_HANDLE_RET$$")) (setq mlofl (existXref "AD_BASE_UNIT_HANDLE_MLOFL$$")))
		(progn
			(setq tempLast (last (nth (caddr (read ret)) (read mlofl))))
			(setq orjFunc (nth (caddr (read ret)) (read mlofl)))
			(ins2Dwg (append (replace 2 (- (length orjFunc) 1) (replace (quote kulpXAssociationModule) 1 (nth (caddr (read ret)) (read mlofl)))) (list tempLast)))
		)
	)
	(setq forceChangeBaseUnitAttiributes nil)
)

(defun changeWallUnitAttiributes ( / tempLast orjFunc wallUnitsSet wallUnit tile_2 doorFunc ad_KapakDefGrup mLofL mLofLeval flin tile_3 ret mlofl)
	;kapak modeli de�i�tiriliyor
	(setq doorFunc (existXref "AD_WALL_UNIT_DOOR_FUNC$$"))
	(if doorFunc
		(progn
			(setq 	ad_KapakDefGrup (cadddr (read doorFunc))
									mLofL (read (strcat ad_KapakDefGrup ":mLofL"))
									mLofLeval (eval mLofL))
			(setq flin (nth (caddr (read doorFunc)) mLofLeval))

			(eval (list (cadr flin) (car flin) (caddr flin) (cadddr flin) "ust"))
		)
	)
	(if (not (member (setq tile_2 (get_tile "sel2")) (list nil ""))) (SetUpKapakName tile_2))
	(removeAllVisibleSide)
	
	;kapak materyali de�i�tiriliyor
	(if (not (member (setq tile_3 (get_tile "sel3")) (list nil "")))
		(progn
			(SetUpKapakMat tile_3)
			(if (tblsearch "layer" "CORNICES_LOWER") (grx_setmaterial "CORNICES_LOWER" tile_3))
			(if (tblsearch "layer" "CORNICES_UPPER") (grx_setmaterial "CORNICES_UPPER" tile_3))
			
			(setq wallUnitsSet (convertSStoList (ssget "X" (list '(0 . "INSERT") '(8 . "CAB_BODY_WALL")))))
			
			(foreach wallUnit wallUnitsSet
				(set_block_material wallUnit T "LSIDE" tile_3)
				(set_block_material wallUnit T "RSIDE" tile_3)
				(set_block_material wallUnit T "CAB_DOORS" tile_3)
			)
		)
	)
	
	;kulp modeli de�i�tiriliyor
	(if (and (setq ret (existXref "AD_WALL_UNIT_HANDLE_RET$$")) (setq mlofl (existXref "AD_WALL_UNIT_HANDLE_MLOFL$$")))
		(progn
			(setq tempLast (last (nth (caddr (read ret)) (read mlofl))))
			(setq orjFunc (nth (caddr (read ret)) (read mlofl)))
			(ins2Dwg (append (replace 1 (- (length orjFunc) 1) (replace (quote kulpXAssociationModule) 1 (nth (caddr (read ret)) (read mlofl)))) (list tempLast)))
		)
	)
	(setq forceChangeWallUnitAttiributes nil)
)

(defun changeTallUnitAttiributes ( / doorFunc ad_KapakDefGrup mLofL mLofLeval flin tile_4 ret mlofl)
	;kapak modeli de�i�tiriliyor
	(setq doorFunc (existXref "AD_TALL_UNIT_DOOR_FUNC$$"))
	(if doorFunc
		(progn
			(setq 	ad_KapakDefGrup (cadddr (read doorFunc))
									mLofL (read (strcat ad_KapakDefGrup ":mLofL"))
									mLofLeval (eval mLofL))
			(setq flin (nth (caddr (read doorFunc)) mLofLeval))

			(eval (list (cadr flin) (car flin) (caddr flin) (cadddr flin) "boy"))
		)
	)
	(if (not (member (setq tile_4 (get_tile "sel4")) (list nil ""))) (SetTallKapakName tile_4))
	
	;kapak materyali de�i�tiriliyor
	(if (not (member (setq tile_5 (get_tile "sel5")) (list nil "")))
		(progn
			(SetTallKapakMat tile_5)
					
			(setq tallUnitsSet (convertSStoList (ssget "X" (list '(0 . "INSERT") '(8 . "CAB_BODY_TALL")))))
			
			(foreach tallUnit tallUnitsSet
				(set_block_material tallUnit T "LSIDE" tile_5)
				(set_block_material tallUnit T "RSIDE" tile_5)
				(set_block_material tallUnit T "CAB_DOORS" tile_5)
			)
		)
	)
	
	;kulp modeli de�i�tiriliyor
	(if (and (setq ret (existXref "AD_TALL_UNIT_HANDLE_RET$$")) (setq mlofl (existXref "AD_TALL_UNIT_HANDLE_MLOFL$$")))
		(progn
			(setq tempLast (last (nth (caddr (read ret)) (read mlofl))))
			(setq orjFunc (nth (caddr (read ret)) (read mlofl)))
			(ins2Dwg (append (replace 3 (- (length orjFunc) 1) (replace (quote kulpXAssociationModule) 1 (nth (caddr (read ret)) (read mlofl)))) (list tempLast)))
		)
	)
	(setq forceChangeTallUnitAttiributes nil)
)

;if funcToSet is list then it can work with upmodes
(defun rout_chain_seperated_doors_V2 ( funcToSet index dependindex depenmsg / kapakname funcUpmode)
	
	(setq kapakname "")	
	(rout_chain_disableAll)
	(setq curDoorsList (list (GetKapakName) (GetGlsKapakName) (GetUpKapakName) (GetTallKapakName)))
	(if (isList funcToSet) 
		(progn
			(setq funcUpmode (getnth 1 funcToSet))
			(setq funcToSet (getnth 0 funcToSet))	
		)
	)
	(if (and dependindex (equal (get_tile (strcat "sel" (rtos dependindex 2 0))) ""))
		(alert  depenmsg)
		(progn	
			(setq g_noActionOnModulList 1.0)			
			(if dependindex (rout_arrange_doors (get_tile (strcat "sel" (rtos dependindex 2 0)))) (rout_arrange_doors "PLAIN"))	
			(if (setq ret (modulList 
							index ;(whichth kmodelold kapakgroupname:mLofL)
							ad_KapakGrupList
							ad_mLofKapakGrups))		
					(progn
						(setq 	ad_KapakDefGrup (cadddr ret)
									mLofL (read (strcat ad_KapakDefGrup ":mLofL"))
									mLofLeval (eval mLofL))
						(setq flin (nth (caddr ret) mLofLeval))		  		 

						(setq kapakname (car flin))
						
						(eval (read (strcat "(" funcToSet " kapakname)")))		
						
						
						(setq curDoorsList (list (GetKapakName) (GetGlsKapakName) (GetUpKapakName) (GetTallKapakName)))

						; (if funcUpmode
							; (eval (list (cadr flin) (car flin) (caddr flin) (cadddr flin) funcUpmode))
						; )
						(setq curDoorsList (list (GetKapakName) (GetGlsKapakName) (GetUpKapakName) (GetTallKapakName)))

					)
					
			)
			(set (read (strcat "rout_sel" (rtos index 2 0) )) kapakname) 
		)
	)
	(rout_chain_enableAll)
	(rout_arrange_doors "")
	(setq g_noActionOnModulList 0.0)
	(list kapakname ret)
)

(defun nesne_yer_AssociationModule (nesnetip nesnenot mode typee / QmLofL mLofL nesnepath mLofGrups ret resultName orjFunc tempLast)
    (setq QmLofL (read (strcat nesnetip ":mLofL"))
          mLofL (eval QmLofL)
    )
	
    (if (setq defFile (findfile (strcat ad_DefPath nesnetip "\\" nesnetip ".DEF")))
		(setq nesnepath (strcat ad_DefPath nesnetip "\\" nesnetip))
		(setq nesnepath (strcat ad_AdekoPath nesnetip "\\" nesnetip)
			  defFile (strcat nesnepath (if ad_XTable ".ENG" "")  ".def")
		)
	)

    (if (null mLofL)
        (progn
            (set_mLofL QmLofL nil defFile)
            (setq mLofL (eval QmLofL))
        )
    )
    (setq mLofGrups
        (list
          (mapcar
              '(lambda (modul)
                  (list (car modul) (last modul))
              )
              mLofL
          )
      )
    )
    (if (setq ret (modulList (GetKulpModel) (list (list nesnepath nesnenot)) mLofGrups))  ;T means zis is kulp
		(progn		
			(setq  resultName (car (nth (caddr ret) mLofL)))
			(setq tempLast (last (nth (caddr ret) mLofL)))
			(setq orjFunc (nth (caddr ret) mLofL))
		)
    )
	(list resultName ret mLofL)
)

(defun kulpXAssociationModule (kulpModel kulpData typee note) ; / needARegen
	(displaySuppress)
	;uncommet reason default
	;(replaceKulp kulpModel)
	(SetKulpModelAssociationModule kulpModel typee)
    ;(setKulpPozisyon kulpModel kulpData)
	(changeAllKulpsLikeSKulpAssociationModule kulpModel typee)
	
	(displayFlush) ;;liberate i�levsel olsun diye daha �nceden liberate yap�lm��sa diye tekrar suppress
	(displayLiberate)
	
)

(defun changeAllKulpsLikeSKulpAssociationModule (newModel typee  / kpk  blk newPositionPars ssDoors ssDoor i kanatRegex kanatEntList)
	;(up_kulpPos newModel )	
	;;old uncomment reason:  kulbukoy'da ��zd���m�z i�in a�a��dakine gerek kalmad� (up_kulpPos 'tan kulbukoy'a gidiyor) ama yine de �imdilik kodun durmas�nda yarar var 
	;; recomment reason: up_kulpPos kapaklarla birlikte ve �ok yava� yapt��� i�in yeniden a��p d�zenledik a�a��dakini , up_kulpPos'u kapad�k bu sefer
	
	(cond 
		((equal typee 1)
			(setq modulLayer "CAB_BODY_WALL")
		)
		((equal typee 2)
			(setq modulLayer "CAB_BODY_BASE")
		)
		((equal typee 3)
			(setq modulLayer "CAB_BODY_TALL")
		)
	)
	(princ (Xstr "\nChanging handle model. Please wait..."))
	;;�al��ma mant��� : kulp bulmak i�in kulplu(kulp pozisyonu i�eren) kanat inserti ar�yoruz. bunu bulmak i�in bloklardan gidiyoruz. bir blo�un i�erdi�i t�m elemanlara gidiyoruz. kanad� bulunca onun blo�una gidiyoruz ve onun i�inde kulp inserti ar�yor ve ona ula��yoruz. (BUNU YAPMA NEDEN�M�Z: ��nk� selection set ile bir insertin alt insertlerine ula�am�yoruz, ancak bloktan gidince ula�abiliyoruz)
	
	(foreach kpkara (getKanatBlockNames modulLayer)
		(if (not (equal (get_Mdata kpkara k:isProfileHandle) 1.0) )
			(progn			
			;�nce kanatl�lar� pozisyonlayarak de�i�tir						
				(changeKanatsHandleAssociationModule kpkara newModel)
				(changeAllKulpsLikeSKulp_reserved kpkara newModel)
			)
		)				
	)		

	;;raydolap gibi yap�larda do�rudan kapak kulp kanat insertleri oldu�u i�in onlar i�in bir de ayr�ca selection setli d�zenleme yap�yoruz
	(setq ssDoors (ssget "X" (list 
								'(0 . "INSERT") 							
								'(8 . "CAB_DOORS") 
						     )
				  )
	)	
	(setq i 0 lenSS (if (null ssDoors) 0 (sslength ssDoors) ))
	(repeat lenSS
		(setq ssDoor (ssname ssDoors i))
		(changeKanatsHandle ssDoor newModel)
		(i++)
	)		
)

(defun changeKanatsHandleAssociationModule ( kulplukanat kulpNewModel / insTemp ins result dhandle)
	(setq result nil)
	(if (not (listp kulpNewModel)) (setq kulpNewModel (list kulpNewModel)))					; "KULP7"  -->  ("KULP7"), for to go easy with serial kulps
	; (if (regexMatch (dfetch 2 kulplukanat) doorKanatRegex)
	  ; (progn
		;kulp var m� diye kontrol ediyoruz					
		  (setq insTemp (entnext (cdr (assoc -2 (tblsearch "block" (dfetch 2 kulplukanat))))))		  
		  (if insTemp
			(progn
				  (if (equal (dfetch 8 (entget insTemp)) "HANDLES_AND_KNOBS") (setq ins (list insTemp)))  ;Blo�un ad�ndan giderek tabloyu sonras�nda ikinci eleman� bulup kulp olarak kabul et			  
				  ;birden fazla kulp varsa diye kontrol ediyoruz    		  
				  (while (and (entnext insTemp) (equal (dfetch 8 (entget (entnext insTemp))) "HANDLES_AND_KNOBS"))
					(setq insTemp (entnext insTemp))
					(setq ins (cons insTemp ins))
				  )					  
			)
		  )		
		 (kpk:getDataToList kulplukanat (list nil 'dhandle nil nil nil nil nil nil nil))
		 (if (not (member dhandle '("0-" "--"))) (changeGivenKulp ins kulpNewModel kulplukanat nil))
		  ; (setq result T)
	  ; )
	; )
	; result
)

(defun changeGivenKulp (sKulpEnt kulpSeries kapakEnt newPositionPars /  newKulpModel
						kapakTip kapakForm kulpSize sKulp kulp EN BOY handleScaleFactor operationPosition temp
						handlemodulCode kulpBlockName LRMode DRMode TaSa Ta Sa kulpRot_OVR i isSucceed kulpInsPt kulpRot isUsedRotation)
						
	(changeGivenKulp_pre_reserved sKulpEnt newKulpModel kapakEnt newPositionPars)

	(defun kulp:exception () nil)
	(kpk:getData (dfetch 2 (entget kapakEnt)) 'kapakTip 'kulp 'kapakForm 'EN 'BOY nil nil nil nil)
	(setq EN (mm2cu EN) BOY (mm2cu BOY)) ;;;;;;;;;-> SOPRANO K�T�PHANES�NDE (mm EN) YER�NE (mm2cu EN) KULLANILDI. FONKS�YON TANIMSIZ HATASI VER�YOR OLMASINA RAMEN. BEYIN ERROR VERD�!!

	(kulpPozParams->reset)
	
	(if (and (not g_OverrideDoorArgs) (setq temp (get_Mdata kapakEnt k:DoorLRMode))) (setq LRMode temp))	
	(if (and (not g_OverrideDoorArgs) (setq temp (get_Mdata kapakEnt k:DoorDRMode))) (setq DRMode temp))
	
	(setq TaSa (get_Mdata kapakEnt k:DoorFrameCenterLineOffsets))
	(if TaSa (setq Ta (car TaSa) Sa (cadr TaSa)))
	
	(setq sKulp (car sKulpEnt))
	(setq i 0 continue T) 

	(while (and continue (setq newKulpModel (nth i kulpSeries)))
		(setq kulpRot_OVR (caddr (assoc newKulpModel kulp:mLofL)))
		(setq kulpAdres (getKulpAddress newKulpModel))
		(setq kulpSize (getDwgSize kulpAdres))
	
		(setq isSucceed (calculateProfessionalSKulpPos))
		(if (and isSucceed kulpRot)
			(setq continue nil)
		)
		(i++)
	)
	(cond
		((null isSucceed)	(alert "Unhandled handle positioning criteria!"))
		((and kulpRot (null isUsedRotation))		
							(setq kulpBlockName (strcat "HANDLES_AND_KNOBS_" newKulpModel))
							(INSERTAS kulpAdres kulpBlockName)	
							(setq handleScaleFactor (getScaleFactorFromBlock kulpBlockName)) ;<-------- disk access possible loss of performance
							
							(changeEntPro sKulp 2 kulpBlockName)													;<--- too many entmod
							(mapcar '(lambda (x) (changeEntPro sKulp x handleScaleFactor)) (list 41 42 43))

							(changeEntPro sKulp 10 kulpInsPt)
							(changeEntPro sKulp 50 kulpRot)
		)
							;if kulpRot is nil, demek ki kulp s��mam�� onun yerine red cross �izile
		(T					(if (null (tblsearch "BLOCK" "HANDLES_AND_KNOBS_XROS"))
								(INSERTAS (getKulpAddress "XROS") "HANDLES_AND_KNOBS_XROS")
							)
							(changeEntPro sKulp 2 "HANDLES_AND_KNOBS_XROS")
		)
	)
	(changeGivenKulp_reserved sKulp newKulpModel kapakEnt)
)

(defun getKanatBlockNames (moduleType / moduleList modul kanatEntList)
	(setq kanatEntList (list ))
	(setq moduleList 	(convertSStoList(ssget "X" 	(list 
														'(0 . "INSERT") 							
														(cons 8 moduleType)
													)
										)
						)
	)
	(foreach modul moduleList
		(setq blockModul (cdr (assoc -2 (tblsearch "block" (dfetch 2 modul)))))

		(while (setq blockModul (entnext blockModul))
			(if (and (equal (dfetch 8 blockModul) "CAB_DOORS") (regexMatch (dfetch 2 blockModul) "K.[^-].+[0-9]{4}X[0-9]{4}.*"))
				(progn
					(setq kanatEntList (append kanatEntList (list blockModul)))
				)
			)
		)
	)
	kanatEntList
)

(defun SetKulpModelAssociationModule (km typee)
	(cond 
		((equal typee 1)
			(CreateXRef "AD_KULP_COB_BODY_WALL$$" km)	
		)
		((equal typee 2)
			(CreateXRef "AD_KULP_COB_BODY_BASE$$" km)	
		)
		((equal typee 3)
			(CreateXRef "AD_KULP_COB_BODY_TALL$$" km)	
		)
	)
	(if (or (setq kulpAdres (findfileex (strcat ad_DefPath "KULP\\" km ".DWG")))
			(setq kulpAdres (findfileex (strcat ad_AdekoPath "KULP\\" km ".DWG"))))	
		(INSERTAS kulpAdres "HANDLES_AND_KNOBS")
	)	
)

(defun soprano_setPlinthH ( plinthh / spaceWallBase)	
	(if ad_UDY	
		(setq spaceWallBase (- ad_UDY ad_ADH ad_TezH ad_bazaH))
	)
	(if plinthh
		(progn			
			(CreateXRef "PLINTH_HEI" (rtos plinthh 2 0))			
			(iniwrite ad_MOD-INI "ebad" "bazaH" plinthh)			
			(setq ad_bazah plinthh)	
			(if (and ad_UDY ad_ADH ad_TezH ad_bazaH spaceWallBase)
				(progn
					(setq ad_UDY (+ ad_bazaH ad_ADH  ad_TezH spaceWallBase ))	
					(iniwrite ad_MOD-INI "ebad" "UDY" ad_UDY)
				)
			)
		)
	)
)

(defun soprano_getPlinthH ( /  km )
		(if (setq nmEnt (dictsearch (namedobjdict) "PLINTH_HEI"))
			(setq km (cdr (assoc 2 nmEnt)))
		)
		(ifnull km (iniread ad_MOD-INI "ebad" "bazaH" "15"))
)

(defun soprano_choosePlinthH ( / currentIndex tempCurrent optionList)
	(setq optionList (list (list 15.0 (Xstr "15.0") ) (list 12.0 (Xstr "12.0") )(list 10.0 (Xstr "10.0"))))
	(setq currentIndex (if (null  (setq tempCurrent (soprano_getPlinthH)))  0   (- (length optionList)  (length (member (assoc tempCurrent optionList) optionList))) ))			
	(soprano_setPlinthH (showGeneralChooserDialog optionList currentIndex (Xstr "Baza y�kseli�ini se�in:")))
)

(defun soprano_removePlinths ()

	(mapcar 'entdel (convertsstolist (ssget "x" (list '(8 . "PLINTHS") ))))	
)

(defun soprano_setPlinthMatType ( plinthtype )
		(CreateXRef "PLINTH_MATTYPE" plinthtype)				
)

(defun soprano_getPlinthMatType ( /  km )
		(if (setq nmEnt (dictsearch (namedobjdict) "PLINTH_MATTYPE"))
			(setq km (cdr (assoc 2 nmEnt)))
		)
		km
)

(defun soprano_getPlinthMatTypeChar ()
	(cond 
		((equal (soprano_getPlinthMatType) (Xstr "Door"))
			"BM"
		)
		((equal (soprano_getPlinthMatType) (strcat "ALU " (Xstr "Profile")))
			"BA"
		)
		((equal (soprano_getPlinthMatType) (strcat "ALU " (Xstr "Covering")))
			"BP"
		)
	)
)

(defun soprano_getPlinthCornerItem()
	(if (member (soprano_getPlinthMatType) (list (strcat "ALU " (Xstr "Profile")) (strcat "ALU " (Xstr "Covering"))))
		(progn
			(cond 
				((equal (soprano_getPlinthH) "10")
					"BKDA10"
				)
				((equal (soprano_getPlinthH) "12")
					"BKDA12"
				)
				((equal (soprano_getPlinthH) "15")
					"BKDA15"
				)
			)
		)
		(progn
			nil
		)
	)
)

(defun soprano_getMaxPlinthLength ()
	(cond 
		((equal (soprano_getPlinthMatType) (Xstr "Door"))
			275.0
		)
		((equal (soprano_getPlinthMatType) (strcat "ALU " (Xstr "Profile")))
			400.0
		)
		((equal (soprano_getPlinthMatType) (strcat "ALU " (Xstr "Covering")))
			400.0
		)
	)
)

(defun soprano_choosePlinthMatType ( / currentIndex tempCurrent optionList)
	(setq optionList (list (list (Xstr "Door") (Xstr "Door") ) (list (strcat "ALU " (Xstr "Profile")) (strcat "ALU " (Xstr "Profile")) )  (list (strcat "ALU " (Xstr "Covering")) (strcat "ALU " (Xstr "Covering")) )))
	(setq currentIndex (if (null  (setq tempCurrent (soprano_getPlinthMatType)))  0   (- (length optionList)  (length (member (assoc tempCurrent optionList) optionList))) ))
	(soprano_setPlinthMatType (showGeneralChooserDialog optionList currentIndex (Xstr "Baza yap� malzemesini se�in:")))
	(if (tblsearch "layer" "PLINTHS")  
		(cond 
			((equal (soprano_getPlinthMatType) (Xstr "Door"))
				(grx_setmaterial "PLINTHS" (GetKapakMat))
			)
			((equal (soprano_getPlinthMatType) (strcat "ALU " (Xstr "Profile")))
				(grx_setmaterial "PLINTHS" "Aluminum PRF")
			)
			((equal (soprano_getPlinthMatType) (strcat "ALU " (Xstr "Covering")))
				(grx_setmaterial "PLINTHS" "Aluminum FOLYO")
			)
		)
	)
)

(defun soprano_setAccMatType ( acctype /  km )
		(CreateXRef "ACC_MATTYPE" acctype)
)

(defun soprano_getAccMatType  ( /  km )
		(if (setq nmEnt (dictsearch (namedobjdict) "ACC_MATTYPE"))
			(setq km (cdr (assoc 2 nmEnt)))
		)
		km
)

(defun soprano_chooseAccMatType ( / currentIndex tempCurrent optionList)
	(setq optionList (list (list "INOX" (Xstr "Inox") ) (list "ANTHRACITE" (Xstr "Antrasit") )  ))
	(setq currentIndex (if (null  (setq tempCurrent (soprano_getAccMatType)))  0   (- (length optionList)  (length (member (assoc tempCurrent optionList) optionList))) ))
	(soprano_setAccMatType (showGeneralChooserDialog optionList currentIndex (Xstr "Aksesuar malzeme grubunu se�in:")))
)

(defun funOnCloseAtMatManager ()
	(setmatmgrsearchfilter firm_mat_prefix)
	(setq g_matfilterSS nil)
)
	


;;yeni setup ile sil
(if (null getLayerMat)
(defun getLayerMat (layername / templayerent resultmat)
	(setq resultmat "")
	(if (setq templayerent (tblobjname "layer" layername) ) (setq resultmat (getnth 0 (getmatname (tblobjname "LAYER" layername)))))
	resultmat
)
)

(defun c:yusuf ()
	(setmatmgrsearchfilter "")
	(c:matmgr_orj)
)
(setq c:model c:sc)
(princ)
;(setq c:handle c:sc)


;(if (null c:minfo_orj) (setq c:minfo_orj c:minfo))
;(setq c:minfo c:dak)

;(Adeko �ifre)

(defun c:010 ()
	(setq c:kert c:kert_orj)
	(setq gm1_tanim_name gm1_tanim)
	(load "gm1.lsp")
	(load "ak1.lsp")
	(load "ak2.lsp")
	(load "uk1.lsp")
	(load "uk2.lsp")
	(Xstr "Done.")
	
	(setq g_DoNotOpenFirmSettings 0)
	
	(defun lmm_showOutPutDialog:initoverrides ()
		(princ)
	)
	(defun f:initoverrides ()
		(princ)
	)
)

(defun c:minfo (/  lstSS ssSel mUnit selectedEntity curcode)
    (setq lstSS (ssgetfirst))
    (init_it)
    (setq ssSel (cadr lstSS))
    (if (and ssSel (equal (sslength ssSel) 1))
        (progn
			(setq selectedEntity (ssname ssSel 0))
			(if (and g_moduleAccessoriesMatchingTable (get_mdata selectedEntity k:mData) (setq curcode (car (read (get_mdata selectedEntity k:mData)))) (setq curacc (assoc curcode  g_moduleAccessoriesMatchingTable)))
				(extraDefWindow selectedEntity (nth 1 curacc))
			)
            (minfo_name selectedEntity)			
        )
    )
    (end_it)
)




(defun c:matmgr->selective ( / selectedTemp kanattemp blkList fromEnt blk zEN tip kulp EN BOY bnam doorName tmp z_dCornAng Zbw2 Zdy ZWid yPa kulpSeri finalMode selectedLayer)
		
		(setmatmgrsearchenablestate 1)
		(setmatmgrsearchfilter "")
		(defun funOnCloseAtMatManager ()
			;(setmatmgrsearchenablestate 1)
			(setmatmgrsearchfilter "")
			(setq g_matfilterSS nil)
			(setLayersOn "")
			(displayFlush) 
			(displayLiberate)			
		)	
		
		(defun c:matmgr ( / selectedTemp myModulCode donotRunMatmgrOrj myDoorMat selectedModul myChosenMat onSelectionError doorInfo selectedMat indexx durdur)
				(setq selectedTempFull  (rout_pickEntWithParent) donotRunMatmgrOrj nil onSelectionError nil)		
				(if (null (getnth 0 selectedTempFull))
					(progn
						(setq onSelectionError T)
						(if (yesno "Se�tini�iz noktada nesne yok. Malzeme sorgulamas� yapmak ister misiniz?")
							(progn
								(c:mogren)							
							)
						)
					)
					(progn	
						(setq selectedModul (last (last (getnth 0 selectedTempFull))))	
						(if (not (equal (type selectedModul) 'ENAME)) (setq selectedModul nil))
						(setq selectedTemp	(getnth 1 selectedTempFull)   selectedFace (getnth 1 selectedTempFull)   )
						(setq g_matfilterSS (ssadd selectedTemp) )
						(if (setq selectedLayer (dfetch 8 (entget selectedTemp))) (ssadd (tblobjname "LAYER" selectedLayer) g_matfilterSS))
					
					)
				)
				(cond  
					( onSelectionError
						(setmatmgrsearchfilter "")
						(setq donotRunMatmgrOrj T)	
					)
					( 	(and	(member (dfetch 8 selectedFace)   (list "ALUMINIUM_FRAME" "ALUMINIUM_DOOR_GLASS" "CAB_DOOR_GLASS")) 
								T)
								(alert (xstr "You can change the color of this type of object from the general selections."))
								(setq donotRunMatmgrOrj T)
					)
					(	(member (dfetch 8 selectedFace)   (list "CAB_BODY_BASE" "CAB_BODY_TALL" "CAB_BODY_WALL"))
						(setmatmgrsearchfilter "CAB_BODY")
						(setq selectedMat (car (choosematerial)))
							(if selectedMat
								(progn
									(set_block_material selectedModul T (dfetch 8 selectedFace) selectedMat)
									(redraw)
									(while (null durdur)
										(setq selectedTempFull  (rout_pickEntWithParent) donotRunMatmgrOrj nil onSelectionError nil)		
										(if (null (getnth 0 selectedTempFull))
											(setq durdur T)
											(progn	
												(setq selectedModul (last (last (getnth 0 selectedTempFull))))	
												(if (not (equal (type selectedModul) 'ENAME)) (setq selectedModul nil))
												(setq selectedTemp	(getnth 1 selectedTempFull)   selectedFace (getnth 1 selectedTempFull)   )
												(setq g_matfilterSS (ssadd selectedTemp) )
												(if (setq selectedLayer (dfetch 8 (entget selectedTemp))) (ssadd (tblobjname "LAYER" selectedLayer) g_matfilterSS))
												
												(if (member (dfetch 8 selectedFace)   (list "CAB_BODY_BASE" "CAB_BODY_TALL" "CAB_BODY_WALL"))
													(progn
														(set_block_material selectedModul T (dfetch 8 selectedFace) selectedMat)
														(redraw)
													)
													(progn
														(alert "Se�ilen nesne devam etmeye uygun de�ildir.")
														(setq durdur T)
													)
												)
											)
										)
									)
								)
							)
							(redraw)
							(setq donotRunMatmgrOrj T)
						
					)
					((and c:matmgr->selective_PLINTHS (equal (dfetch 8 selectedFace) "PLINTHS"))
						(cond
							((and selectedModul (equal (get_mdata selectedModul k:mtyp) "UPANO"))
								(setmatmgrsearchfilter "SOPRANO CAB_BODY")
								(setq myDoorMat (car (choosematerial)))
								(if myDoorMat
									(progn
										(setq bazalar (convertSStoList (lmm_getPlinths)))
										(mapcar 
											'(lambda (x) 
												(if (equal (get_mdata x k:mtyp) "UPANO")
													(progn
														(set_block_material x T "PLINTHS" myDoorMat)
														
														(setq indexx 0)
														(repeat 3
															(set_block_material x T (strcat "PLINTHS_EDGE_" (rtos indexx)) myDoorMat)
															(setq indexx (+ indexx 1))
														)
													)
												)
											)
											bazalar
										)
									)
								)
								(redraw)
								(setq donotRunMatmgrOrj T)
								
								(displayFlush) 
								(displayLiberate)
							)
							((equal (soprano_getPlinthMatType) (Xstr "Door"))
								(setmatmgrsearchfilter "SOPRANO DOORMAT")
							)
							((equal (soprano_getPlinthMatType) (strcat "ALU " (Xstr "Profile")))
								(setmatmgrsearchfilter "ALUPRFPLINTHS")
							)
							((equal (soprano_getPlinthMatType) (strcat "ALU " (Xstr "Covering")))
								(setmatmgrsearchfilter "ALUFOLYOPLINTHS")
							)
						)
					)
					((equal (dfetch 8 selectedFace) "WORKTOPS")
						(setmatmgrsearchfilter "")
					)
					; ((and selectedModul (member (parse$ (dfetch 2 selectedModul)) (list "RF" "YNK" "PN")) (and (get_mdata selectedModul k:WPanelUsageType) (checkStrInStr "independent" (get_mdata selectedModul k:WPanelUsageType))))
						; (setmatmgrsearchfilter "SOPRANO PANELMAT")
						; ; (setq selectedMat (car (choosematerial)))
							; ; (if selectedMat
								; ; (mapcar '(lambda (x) (set_block_material selectedModul T x selectedMat)) (list "PANELS_SIDE" "SHELVES" "PANELS_WALL"))
							; ; )
							; ; (redraw)
							; ; (setq donotRunMatmgrOrj T)
							
							; ; (displayFlush) 
							; ; (displayLiberate)
					; )
					((member (dfetch 8 selectedFace) (list "PANELS_SIDE" "SHELVES" "PANELS_WALL_2ND_BACK" "PANEL_WALL_DRAWER" "WARDROBE_FRAME"))
						(setmatmgrsearchfilter "SOPRANO PANELMAT")
					)
					((equal (dfetch 8 selectedFace) "PANELS_WALL")
						(if (checkStrInStr "independent" (get_mdata selectedModul k:WPanelUsageType))
							(setmatmgrsearchfilter "SOPRANO PANELMAT")
							(setmatmgrsearchfilter "SOPRANO CAB_BACK")
						)
					)
					( 	(or (equal (dfetch 8 selectedFace) "CORNICES_UPPER")
							(equal (dfetch 8 selectedFace) "CORNICES_LOWER")
						)
						(setmatmgrsearchfilter "SOPRANO DOORMAT")
					)
					( 	(and (member (dfetch 8 selectedFace)   (list "RSIDE" "LSIDE" ""))	
								c:matmgr->selective_vside)								
								(alert (xstr "Change the cover color to change the visible side color."))
								(setq donotRunMatmgrOrj T)		
								
					)
					(	(or 
							(and (dfetch 330 (entget selectedTemp)) (equal (dfetch 8 (entget selectedTemp)) "CAB_DOORS") c:matmgr->selective_doors )
						)							
						(setq kanattemp  (dfetch 2 (dfetch 330 selectedTemp)))							
						(if (regexMatch kanattemp "K.{3,}_+[0-9]{4}X[0-9]{4}.*")
							(kpk:getData kanattemp 'tip 'kulp 'doorName 'EN 'BOY 'tmp 'Zbw2 'Zdy 'ZWid)
							(progn
								(setq doorInfo (getDoorModelOfAUnit selectedModul nil))
								(setq doorName 
									(if (getnth 2 doorInfo) 
										(getnth 2 doorInfo)
										(cond 
											((equal (parse$ (dfetch 2 selectedModul)) "YNK0018F")
												(setmatmgrsearchfilter "")
												nil
											)
											( (equal (substr (dfetch 2 selectedModul) 1 3) "B")
												(GetTallKapakName)
											)
											((equal (substr (dfetch 2 selectedModul) 1 3) "U")
												(GetUpKapakName)
											)
											(T
												(getkapakname)
											)
										)	
									)
								)
							)
						)
						(if doorName
							(progn
								(if (or (equal (substr doorname 1  3) "DBY") (equal doorname "ROZETKPK")) (setq doorName (getkapakname)))
								(setmatmgrsearchfilter (strcat (ifnull firm_mat_prefix "") " " doorName "DOOR"))
							)
						)
						(setq myDoorMat (car (choosematerial)))
						(if myDoorMat
							(progn
								(set_block_material selectedModul T "CAB_DOORS" myDoorMat)
								(set_block_material selectedModul T "RSIDE" myDoorMat)
								(set_block_material selectedModul T "LSIDE" myDoorMat)
							)
						)
						
						(redraw)
						(setq donotRunMatmgrOrj T)
						
						(displayFlush) 
						(displayLiberate)

					)
					(T
						(setmatmgrsearchfilter "")
					)
				)						
				(if (null donotRunMatmgrOrj) (c:matmgr_orj))				
		)
)

(defun drawVisibleSides (nmUnit side additionaldims / ptIns dWid dHei dDep dOff p1 p2 p3 p4 p11 p22 p33 p44 elast dWid2 dAng2 dDep2 rsidedims lsidedims bsidedims tsidedims entsforVsides offset
														additionaldepth additionalHeight additionalup additionalleftside additionalrightside cBoy)
														
	(setq
		additionaldepth		(ifnull  (getnth 0 additionaldims) 0.0)
		additionalHeight	(ifnull  (getnth 1 additionaldims) 0.0)
		additionalleftside	(ifnull  (getnth 2 additionaldims) 0.0)
		additionalrightside (ifnull  (getnth 3 additionaldims) 0.0)
		additionalup		(ifnull  (getnth 4 additionaldims) 0.0)
	)
	
	(setq ptIns '(0 0 0))
	(setq dWid  (get_Mdata nmUnit k:WID))
	(setq dHei  (get_Mdata nmUnit k:HEI))
	(setq dDep  (get_Mdata nmUnit k:DEP))
	(setq dOff  (cm 0.1))
	(setq rsidedims nil)
	(setq lsidedims nil)

    (if (or (isAltdolap nmUnit) (isBoydolap nmUnit))
        (setq ptIns (xyofz ptIns (+ (if (setq cBoy (get_mdata nmUnit k:CBoy)) (- (get_mdata nmUnit k:Hei) cBoy) ad_bazaH)  (caddr ptIns))))
    )

	(setq elast (entlast))
    (if (member side '(1 3))   ;LEFT
        (progn
        	(cond 
				((isKose nmUnit)
					(setq dWid2 (get_Mdata nmUnit k:WID2)
            		  dAng2 (- (get_Mdata nmUnit k:CornAng))
					  offset (* -2 additionaldepth)
					)
					(if (equal (get_Mdata nmUnit k:MTYP) "AK1")
						(setq dDep2	(get_Mdata nmUnit k:DEP))
						(setq dDep2	(get_Mdata nmUnit k:DEP2))
					)
				)
				((equal (get_mdata nmUnit k:mtyp) "BE")
					(setq dWid2 0.0
						  dAng2 pi
						  dDep2	dDep
						  dDep (get_Mdata nmUnit k:DEP2)
						  offset 0.0
					)
				)
				(T            	
					(setq dWid2 0.0
						  dAng2 pi
						  dDep2	dDep
						  offset 0.0
					)
				)
        	)

        	(if (null dDep2) (setq dDep2 dDep))
            (setq p1 (polar ptIns dAng2 (+ dWid2 dOff )) )			
			(setq p1 (polar p1 (* -1 (+ dAng2 pi/2) ) (+ additionaldepth offset dOff)) )
			(setq p1 (mapcar '+  (list 0.0 0.0 ( * -1 (+ additionalHeight dOff)))  p1))
			(setq  
				p2 (polar p1 (+ dAng2 pi/2) (+ additionaldepth dOff dOff dDep2))
				p3 (list (car p2) (cadr p2) (+ dHei additionalup dOff))      
				p4 (list (car p1) (cadr p1) (+ dHei additionalup dOff))
			)
        	(setlayer "LSIDE" 40)
        	(command "_.FACE" p1 p2 p3 p4 "")

			(setq p11 p1 p22 p2 p33 p3 p44 p4)
			
            (setq p1 (polar p1 (+ dAng2 pi) (+ ad_ET dOff dOff))
                  p2 (polar p2 (+ dAng2 pi) (+ ad_ET dOff dOff))
                  p3 (polar p3 (+ dAng2 pi) (+ ad_ET dOff dOff))
                  p4 (polar p4 (+ dAng2 pi) (+ ad_ET dOff dOff))
            )
        	(command "_.FACE" p1 p2 p3 p4 "")
			(command "_.FACE" p2 p22 p33 p3 "")
			(command "_.FACE" p3 p33 p44 p4 "")
			(command "_.FACE" p2 p22 p11 p1 "")
			(command "_.FACE" p1 p11 p44 p4 "")
			(setq lsidedims (list (- (distance p4 p3) (* dOff 2.0)) (- (distance p4 p1) (* dOff 2.0)) additionalup ) )
			(set_Mdata nmUnit (list (cons k:P2CVisibleLSideDims lsidedims)))
        )
    )
    (if (member side '(2 3))   ;RIGHT
        (progn
			(if (equal (get_mdata nmUnit k:mtyp) "BE")
				(setq 
					dWid2 0.0
					dAng2 pi
					dDep2	dDep
					dDep (get_Mdata nmUnit k:DEP2)
					offset 0.0
				)
			)


			(setq p1 (polar ptIns 0.0 (+ dWid dOff)))
			(setq p1 (polar p1 (+ pi/2)  (+ additionaldepth dOff)))
			(setq p1 (mapcar '+  (list 0.0 0.0 ( * -1 (+ additionalHeight dOff)))  p1))
            (setq 
                  p2 (polar p1 (- pi/2)  (+ additionaldepth dDep dOff dOff))
                  p3 (xyofz p2 (+ dHei additionalup dOff))                  
            )
			(setq p4 (xyofz p1 (+ dHei additionalup dOff)))
		
        	(setlayer "RSIDE" 40)
        	(command "_.FACE" p1 p4 p3 p2 "")
			(setq p11 p1 p22 p2 p33 p3 p44 p4)
			(setq p1 (polar p1 pi (+ ad_ET dOff dOff))
                  p2 (polar p2 pi (+ ad_ET dOff dOff))
                  p3 (polar p3 pi (+ ad_ET dOff dOff))
                  p4 (polar p4 pi (+ ad_ET dOff dOff))
            )
        	(command "_.FACE" p1 p2 p3 p4 "")
			(command "_.FACE" p2 p22 p33 p3 "")
			(command "_.FACE" p3 p33 p44 p4 "")
			(command "_.FACE" p2 p22 p11 p1 "")
			(command "_.FACE" p1 p11 p44 p4 "")
			(setq rsidedims (list (- (distance p4 p3) (* dOff 2.0)) (- (distance p4 p1) (* dOff 2.0)) additionalup ) )
			(set_Mdata nmUnit (list (cons k:P2CVisibleRSideDims rsidedims)))
        )
    )
	(if (member side '(4 6))   ;BOTTOM
        (progn
			(setq p1 ptIns)
			(setq p1 (polar p1 (+ pi/2)  additionaldepth) )
			(setq p1 (mapcar '+  (list ( * -1 additionalleftside) 0.0 ( * -1 dOff))  p1))
            (setq 
                  p2 (polar p1 (- pi/2)  (+ additionaldepth dDep))
                  p3 (mapcar '+  (list (+ dWid additionalleftside additionalrightside) 0.0 0.0)  p2)                  
            )
			
			(setq p4 (mapcar '+  (list (+ dWid additionalleftside additionalrightside) 0.0 0.0)  p1))
			
			(setlayer "BSIDE" 40)
			(command "_.FACE" p1 p4 p3 p2 "")
		
			(setq p11 p1 p22 p2 p33 p3 p44 p4)
			(setq p1 (mapcar '+  (list 0.0 0.0 (+ ad_ET dOff dOff))  p1)
				  p2 (mapcar '+  (list 0.0 0.0 (+ ad_ET dOff dOff))  p2)
				  p3 (mapcar '+  (list 0.0 0.0 (+ ad_ET dOff dOff))  p3)
				  p4 (mapcar '+  (list 0.0 0.0 (+ ad_ET dOff dOff))  p4)
            )
        	(command "_.FACE" p1 p2 p3 p4 "")
			(command "_.FACE" p2 p22 p33 p3 "")
			(command "_.FACE" p3 p33 p44 p4 "")
			(command "_.FACE" p2 p22 p11 p1 "")
			(command "_.FACE" p1 p11 p44 p4 "")
			
			(setq bsidedims (list (+ additionaldepth dDep) (- (nth 0 p4) (nth 0 p1))  0.0))
			(set_Mdata nmUnit (list (cons k:P2CVisibleBSideDims bsidedims)))
			(setq bsidedims (list additionaldepth additionalleftside additionalrightside))
			(set_Mdata nmUnit (list (cons k:P2CVisibleBSideDims2 bsidedims)))
		)
	)
	(if (member side '(5 6))   ;TOP
        (progn
			(setq p1 (list 0.0 0.0 dHei))
			(setq p1 (polar p1 (+ pi/2)  additionaldepth) )
			(setq p1 (mapcar '+  (list ( * -1 additionalleftside) 0.0 dOff)  p1))
            (setq 
                  p2 (polar p1 (- pi/2)  (+ additionaldepth dDep))
                  p3 (mapcar '+  (list (+ dWid additionalleftside additionalrightside) 0.0 0.0)  p2)                  
            )
			
			(setq p4 (mapcar '+  (list (+ dWid additionalleftside additionalrightside) 0.0 0.0)  p1))
			
			(setlayer "TSIDE" 40)
			(command "_.FACE" p1 p4 p3 p2 "")
	
			(setq p11 p1 p22 p2 p33 p3 p44 p4)
			(setq p1 (mapcar '+  (list 0.0 0.0 (* -1 (+ ad_ET dOff dOff)))  p1)
				  p2 (mapcar '+  (list 0.0 0.0 (* -1 (+ ad_ET dOff dOff)))  p2)
				  p3 (mapcar '+  (list 0.0 0.0 (* -1 (+ ad_ET dOff dOff)))  p3)
				  p4 (mapcar '+  (list 0.0 0.0 (* -1 (+ ad_ET dOff dOff)))  p4)
            )
        	(command "_.FACE" p1 p2 p3 p4 "")
			(command "_.FACE" p2 p22 p33 p3 "")
			(command "_.FACE" p3 p33 p44 p4 "")
			(command "_.FACE" p2 p22 p11 p1 "")
			(command "_.FACE" p1 p11 p44 p4 "")
			
			(setq tsidedims (list (+ additionaldepth dDep) (- (nth 0 p4) (nth 0 p1))  0.0))
			(set_Mdata nmUnit (list (cons k:P2CVisibleTSideDims tsidedims)))
			(setq tsidedims (list additionaldepth additionalleftside additionalrightside))
			(set_Mdata nmUnit (list (cons k:P2CVisibleTSideDims2 tsidedims)))
		)
	)
	(drawVisibleSides_reserved (setq entsforVsides (collectents elast)) nmUnit)
	(combinesub entsforVsides nmUnit)
)

(defun c:gyan (/ nmUnit whichSide res adddep layNam doorMatNamee doorsOnModule door doorRealMat)
	(init_it)

	(while (null nmUnit)
		(setq nmUnit (car (_initgetentselRAY 1 "" (Xstr "\nPick unit: ") 1)))
		(if (null nmUnit)
			(princ (Xstr "\nNo unit picked. Try again..."))
		)
	)
	(setq layNam (dfetch 8 nmUnit))
	(setq doorMatNamee
		(cond
			((equal layNam "CAB_BODY_BASE")
				(ifnull (GetBaseUnitsDoorMaterial) (car (getmatname (tblobjname "LAYER" "CAB_DOORS"))))
			)
			((equal layNam "CAB_BODY_WALL")
				(ifnull (GetWallUnitsDoorMaterial) (car (getmatname (tblobjname "LAYER" "CAB_DOORS"))))
			)
			((equal layNam "CAB_BODY_TALL")
				(ifnull (GetTallUnitsDoorMaterial) (car (getmatname (tblobjname "LAYER" "CAB_DOORS"))))
			)
			(T
				(car (getmatname (tblobjname "LAYER" "CAB_DOORS")))
			)
		)
	)
	(initget (strcat (Xstr "Left Right Both ~ Remove|reMove") " ~_Left ~_Right ~_Both ~_ ~_Remove"))
	(setq whichSide (getkword (Xstr "\nWhich side to change or Remove? Left/Right/Both/reMove: ")))

	(if (equal whichSide "Remove")
		(progn		
			(removesub nmUnit "LSIDE")
			(removesub nmUnit "RSIDE")
		)
		(progn
			(setq whichSide (cdr (assoc whichSide '(("Left" . 1) ("Right" . 2) ("Both" . 3)))))
			;(setq adddep (if (null (setq res (getreal  (sprintf (Xstr "\n Additional back extending <~d> :" ) (list (ifnull ad_wallOffsetBaseCab 0.0)))))) (ifnull ad_wallOffsetBaseCab 0.0) res))
			;(setq addhei (if (null (setq res (getreal  (sprintf (Xstr "\n Additional down extending <~d> :" ) (list 0.0))))) 0.0 res))
			
			(drawVisibleSides nmUnit whichSide (list 0.0 0.0))
			
			(setq doorsOnModule (listDoorEnamesIn nmUnit "." nil))
			(foreach door doorsOnModule
				(if (null doorRealMat)
					(setq doorRealMat (get_block_material door "CAB_DOORS"))
				)
			)
			(set_block_material nmUnit T "RSIDE" (ifnull doorRealMat doorMatNamee))
			(set_block_material nmUnit T "LSIDE" (ifnull doorRealMat doorMatNamee))
		)
	)
	(end_it)
)

(c:matmgr->selective)


(defun SetTallUnitDoorMats (inputMat / ssWhole) 
	(if inputMat (CreateXRef "AD_BODYDOOR_MATERIAL$$" inputMat) (delxref "AD_BODYDOOR_MATERIAL$$"))  
	(setq ssWhole (ssget "X" '((0 . "INSERT") (8 . "CAB_BODY_TALL") (67 . 0))))	
	(foreach myUnit (convertsstolist ssWhole)
	
		(set_block_material myUnit T "CAB_DOORS" inputMat)
	)
)

(defun GetTallUnitsDoorMaterial () (getXRef "AD_BODYDOOR_MATERIAL$$"))

(defun kulbuKoy:getOrientation (kapaksData kulpsData Qpt Qrot / kulpModel LengthMin LR:V LR:H LR:M status kulpSize 
																kulpRot_OVR forceOriginalRot rot?)
	(setKulpPosParameters)

	(setq status T)

	(if (and (not (equal kulpRot_OVR "0")) (not (member kulpModel g_SopranoEntegreKulpList))) ;g_SopranoEntegreKulpList deki kulplar�n yerle�imi �zel olarak de�erlendiriliyor.
		(progn
			(eval kulpRot_OVR)
			(setLRMode LRMode)
			; (set (read (strcat "LR:" LRMode)) T)  ;sets LR:x
			(if (not (or LR:V LR:H LR:M))
				(progn 
					(setq LR:V T)
					(princ (Xstr "\nNo valid left/right door handle mode. Set to default (V)."))
				)
			)
			(if (and (equal kulpRot_OVR "H") LR:V) (setq LR:V nil LR:H T))
			
			(if LR:V 
				(setq LengthMin (+ kulp:Length Tk Tk))
				(setq LengthMin (+ kulp:Length Sk Sk))
			)
			
			(if forceOriginalRot		;Do not try to fit handle onto the door the other way
				(defun rot? (s) nil)
				(defun rot? (s) s)
			)
				
			(cond
				((kulp:exception))
				(isBoyKapak												(D-V))
				((and isLarder (not isGlassDoor) (not isFramedDoor))	(O-V))
				((and isLR LR:V)
					(cond 
						((> DH LengthMin)			(if isBase (KD-V) (GD-V)))		;kulp kapak boyuna s���yorsa
						((rot? (> DW LengthMin))	(setq isUsedRotation T) (if isBase (KD-H) (GD-H)))		;kulp kapak enine s���yorsa
						(T							(setq isUsedRotation T) (if isBase (KD-?) (GD-?)))		;kulp ne boyuna ne enine s��m�yorsa ?
					)
				)
				((and isLR LR:H)
					(cond
						((> DW LengthMin)			(if isBase (KD-H) (GD-H)))		;kulp kapak enine s���yorsa
						((rot? (> DH LengthMin))	(setq isUsedRotation T) (if isBase (KD-V) (GD-V)))		;kulp kapak boyuna s���yorsa
						(T							(setq isUsedRotation T) (if isBase (KD-?) (GD-?)))		;kulp ne boyuna ne enine s��m�yorsa ?
					)
				)
				((and isLR LR:M) 
					(cond
						((> DW LengthMin)			(if isBase (K-H) (G-H)))		;kulp kapak enine s���yorsa
						((rot? (> DH LengthMin))	(setq isUsedRotation T) (if isBase (KD-V) (GD-V)))		;kulp kapak boyuna s���yorsa
						(T							(setq isUsedRotation T) (if isBase (K-?) (G-?)))		;kulp kapa�a hi� bir �ekilde s��m�yorsa
					)
				)
				((and isDrawer (equal DRMode "C"))
					(cond
						((> DW LengthMin)			(OO-H))
						(T 							(setq isUsedRotation T) (OO-?))
					)
				)
				(isDrawer
					(cond
						((> DW LengthMin)			(if isBase (K-H) (G-H)))		;kulp kapak enine s���yorsa
						((and 	(not isGlassDoor) 									;cam kapak de�ilse ve
								(not isFramedDoor)
								(> DH LengthMin)
						 )							(setq isUsedRotation T) (if isBase (K-V) (G-V))			;kulp kapak boyuna s���yorsa ?
						)
						(T							(setq isUsedRotation T) (if isBase (K-?) (G-?)))
					)
				)
				(isOpenUp
					(cond
						((> DW LengthMin)			(G-H))							;kulp kapak enine s���yorsa
						(T							(setq isUsedRotation T) (G-?))
					)
				)
				(T (setq status nil) )
			)
		)
		(progn
			(set Qpt '(0 0 0))
			(set Qrot 0)
		)
	)
	; (kulbuKoy:end)
	status
)

(defun setLRMode (to)
	(setq LR:V nil LR:H nil LR:M nil)
	(if to (set (read (strcat "LR:" to)) T))
)

(defun kulbuKoy (kapakForm kapakTip openDir DW DH zEN kapakBlockName kulp:Model / 
						i kulpIndex theKapak theKulp continue kulpAddress isBase isDkanat 
						isLR isDrawer isOpenUp isBoyKapak isLarder isGlassDoor isFramedDoor 
						oldlayer kulbuKoy:yRot kulbuKoy:xTrans kulp:Length kulp:HalfLength 
						kulp:Width kulp:HalfWidth orientation insPt rotAng kulpRot_OVR 
						isSuccess isSerial isUsedRotation)
	(if (not (or (equal openDir "--") (equal openDir "0-")))
		(progn
			(setq theKapak (list kapakForm kapakTip openDir DW DH kpkTck))
			(kulbuKoy:makeList)
			(setq i 0 continue T)
			(while (and continue (setq kulp:Model (nth i kulp:ModelList)))
				;sopranonun entegre kulplar� i�in
				(if (member (setq possibleHandleName (parse kulp:Model (rtos (cu2mm DW)))) g_SopranoEntegreKulpList)
					(setq kulp:Model possibleHandleName)
				)
				
				(setq kulpAddress (getKulpAddress kulp:Model)) 
				(setq kulpSize (getDwgSize kulpAddress))
				(setq kulpRot_OVR (caddr (assoc kulp:Model kulp:mLofL)))

				(setq theKulp (list kulp:Model kulpSize kulpRot_OVR isSerial))	;if isSerial then force to put kulp as intended by LRMode
				(setq isSuccess (kulbuKoy:getOrientation theKapak theKulp 'insPt 'rotAng))	
				(if (and isSuccess rotAng)
					(setq continue nil)
				)
				(i++)
			)
			(if (null isSuccess)
				(alert "Unhandled handle positioning criteria!")
				(progn
					(kulbuKoy:init)
					(if (and rotAng (null isUsedRotation))
						(progn 
							(setq kulp:BlockName (strcat "HANDLES_AND_KNOBS_" kulp:Model))
							(INSERTAS kulpAddress kulp:BlockName)
							(command cmdINSERT kulp:BlockName insPt "" "" rotAng)
						)
						(kulbuKoy:InsertXros)
					)
					(kulbuKoy:end)
					(changeGivenKulp_reserved (entlast) kulp:Model kapakBlockName)
				)
			)
		)
	)
	isSuccess
)

(defun changeGivenKulp_reserved (sKulp newKulpModel kapakEnt / blockEnt tip kulp mkg71block EN BOY newKulpModelnew isFixed rotProplist mykapak_animateScript ss slen i entName entType entLayer modulCode kapakName blokNam blokNumber entegreKulpStringSize entegreKulpString secondBlok firstBlok)
		
	(setq kapakName
		(if (equal (type kapakEnt) 'ENAME) 
			(if (equal "INSERT" (dfetch 0 kapakEnt))
				(dfetch 2 kapakEnt)
				""
			)
			(if (and kapakEnt (equal (type kapakEnt) 'STR)) kapakEnt "")
		)		
	)
	
	(if kapakName (kpk:getDataToList kapakEnt (list 'tip 'kulp nil 'EN 'BOY nil nil nil nil 'isFixed)))
	
	(setq rotProplist 
		(cond
			((checkStrInStr "G" kulp)
				(list (d2r 90.0)  -0.001 -1)
			)
			((checkStrInStr "K" kulp)
				(list (d2r 90.0) (+ ( * 0.1 BOY) 0.001) 1)
			)
			(T
				(if (< (getnth 1 (getEntPro sKulp 10)) ( * 0.05 BOY)) (list (d2r 90.0)  -0.001 -1) (list (d2r 90.0) (+ ( * 0.1 BOY) 0.001) 1))
			)
		)
	)
	
	(if (and (member newKulpModel  g_sopranoSimetrikKulpList) (checkStrInStr "G" kulp))
		(progn
			(changeEntPro sKulp 50 (getnth 0 rotProplist))
			(changeEntPro sKulp 41  (getnth 2 rotProplist))
		)
	)
	
	(setq regexMatchKulpModel (car (member newKulpModel g_SopranoEntegreKulpList)))
	(if (and newKulpModel (regexMatch newKulpModel regexMatchKulpModel))
		(progn
			(INSERTAS (strcat ad_DefPath "KULP\\" regexMatchKulpModel ".DWG") (strcat "HANDLES_AND_KNOBS" "_" regexMatchKulpModel (rtos EN 2 0)))
			(setq entegreKulpString "ENTEGRE_" entegreKulpStringSize (strlen entegreKulpString))
			(setq mkg71block (cdr (assoc -2 (tblsearch "BLOCK" (strcat "HANDLES_AND_KNOBS" "_" "_" regexMatchKulpModel (rtos EN 2 0) )))))
			(while mkg71block
				(setq firstBlok nil secondBlok nil)
				(if (and (setq blokNam (dfetch 2 mkg71block)) (> (strlen blokNam) entegreKulpStringSize))
					(progn
						(if (equal (substr blokNam 1 entegreKulpStringSize) entegreKulpString)
							(setq blokNumber (atoi (substr blokNam (+ entegreKulpStringSize 1) (-  (strlen blokNam) entegreKulpStringSize))))
							
							(cond 
								((equal (rem blokNumber) 0)
									(setq firstBlok T)
								)
								(T
									(setq secondBlok T)
								)
							)
						)
					)
				)
				(cond ((and (equal (dfetch 0 mkg71block) "INSERT") firstBlok)
					(changeEntPro mkg71block 43  (* (/ 45.0 ( * 0.1 EN) )  (/ (- ( * 0.1 EN) 10.0) 35.0)))
					
					)
					((and (equal (dfetch 0 mkg71block) "INSERT") secondBlok)
					
					(princ)
					)
				)
				(setq mkg71block (entnext mkg71block))
			)
			(changeEntPro sKulp 2 (strcat "HANDLES_AND_KNOBS" "_" regexMatchKulpModel (rtos EN 2 0) ))
			(changeEntPro sKulp 50 (getnth 0 rotProplist))
			(changeEntPro sKulp 10 (list (if (equal tip "D") (+ (+ g_ClearWallCorner90X (/ EN 20.0))) 0.0) (getnth 1 rotProplist) 2.0 ))
			(changeEntPro sKulp 42 (/ ( * 0.1 EN) 45.0))
			(changeEntPro sKulp 41  (getnth 2 rotProplist))
		)		
	)
)

(defun updateModelOfKapak_post_reserved (ins kpk doorName knam kmodel / tip kulp EN BOY isFixed)
	(if (equal "ozel" upmode)
		(progn
			(if (get_block_material kpk "CAB_DOORS")
				(progn
					(set_block_material kpk T "CAB_DOORS" g_myselectedDoorMat)
					(set_block_material ins T "RSIDE" g_myselectedDoorMat)
					(set_block_material ins T "LSIDE" g_myselectedDoorMat)
				)
			)
			(if (get_block_material ins "CAB_DOORS") (set_block_material ins T "CAB_DOORS" g_myselectedDoorMat))
		)
	)	
)

(defun replaceKapak_pre_reserved (input kulpPos upmode / mLofL mLofGrups newModel lenSS i nesnepath nesnetip)
	(setmatmgrsearchfilter (strcat (ifnull firm_mat_prefix "") "" input "DOOR"))
	(if (null (get_tile "sel0")) ;null sa router dan �ag�r�lmam��t�r. Tekli se�ime zorla
		(progn
			(setq g_IsspecialDoor T) ;replaceKapak fonksiyonunda sertbest b�rak�lm��t�r.
		)
	)
	(if (and (null (get_tile "sel0")) (null (setq g_myselectedDoorMat (car (choosematerial)))))
		(setq g_myselectedDoorMat (getnth 0 (getmatmgrfirstfilteredmatname)))	
		(princ)
	)	 
	(princ)
)

(defun replaceKapak (doorName kulpPos upmode / doorNamePrevious ssUnits kmodel validKapakTypes notForThisDoorModules AkanatDefined BkanatDefined g_IsspecialDoor)
	(replaceKapak_pre_reserved doorName kulpPos upmode)
	(setq doorNamePrevious (GetKapakName))
	(setq kmodel (GetKpkOfDoor doorName))
    (loadDoorModel kmodel)

	(setq validKapakTypes (rK:detectValidKapakTypes kmodel))
	(setq g_forbiddenModules (getForbiddenModulList doorName))
	(if g_IsspecialDoor (setq upmode "ozel")) ;�zel se�ime zorla 
	(cond	
		((rK:isGenel&BkanatOnly) (rK:selectAllUnits)			(rK:setKapakNamesTum))		;Special case
		((equal "genel" upmode)  (rK:selectAllUnits)			(rK:setKapakNamesGenel))
		((equal "tum" upmode) 	 (rK:selectAllUnits)			(rK:setKapakNamesTum))
		((equal "alt" upmode) 	 (rK:selectBase&TallUnits)		(rK:setKapakNamesAlt))
		((equal "ust" upmode) 	 (rK:selectWallUnits) 			(rK:setKapakNamesUst))
		((equal "boy" upmode) 	 (rK:selectTallUnits) 			(rK:setKapakNamesBoy))
		((equal "tumcamli" upmode)  (rK:selectAllUnits) 		(rK:setKapakNamesTumCamli))
		((equal "ozel" upmode) 	 (rK:selectInteractive))
	)	

	(if (and (/= "ozel" upmode) ssUnits)	
		(progn
		
			(if (and BkanatDefined (equal "tumcamli" upmode))
				(rK:reCreateKapaksIn ssUnits "[BTNI]")
				(rK:reCreateKapaksIn ssUnits validKapakTypes)			
			)
		
		)
	)
	(replaceKapak_post_reserved doorName kulpPos upmode  ssUnits validKapakTypes)
	(setq g_noActionOnModulList 0)
	(purgeUBlocks)
)

(defun rK:selectInteractive (/ m ins kpk ozel-er)
	(defun ozel-er (s)
		(EchoUnexpectedErr s)
		(facesel_exit)
		(end_it)
		(redraw)
		(prin1)
    )
	
	(setErrorFunc ozel-er)	
	
	(displayLiberate)
	(facesel_init)
	(while (setq m (facesel (Xstr "\nUnit or Door: ")))
		(displaySuppress)
		(if (equal 'ENAME (type (last (last m))))
			(progn
				(setq ins (last (last m)))
				(cond 	((isFixedDoorUnit ins)	nil)														;Skip if this unit is fixed
						((isRestrictedUnit (dfetch 2 ins)) nil)
						((isDoor (entget ins) validKapakTypes nil nil)   									;Ba��bo� tek bir kapak tutulduysa
							(updateModelOfKapak nil ins doorName)        									;Deni�tir bunu
						)
						((and
							(setq kpk (cadr (reverse (last m))))        									;Kapa��n bir yeri tutulduysa
							(isDoor (entget kpk) validKapakTypes nil nil)
						 )
							(updateModelOfKapak ins kpk doorName)      										;Sadece bu kapa�� de�i�tir
						)
						(T                                                									;Modul�n ba�ka bir yeri tutulduysa
							(setq kpk (cdr (assoc -2 (tblsearch "block" (dfetch 2 (last (last m)))))))  	;Blo�un ad�ndan giderek tabloyu, akabinde ilk eleman� bul kpk varsay
							
							; (set_block_material (tblobjname "block"  (dfetch 2 (last (last m)))) T "RSIDE" g_myselectedDoorMat) ;module t�kland�ysa gyanlar�n rengini degi�tir.
							; (set_block_material (tblobjname "block"  (dfetch 2 (last (last m)))) T "LSIDE" g_myselectedDoorMat) ;//
							; (set_block_material (tblobjname "block"  (dfetch 2 (last (last m)))) T "CAB_DOORS" g_myselectedDoorMat)
							(while (setq kpk (findKK kpk validKapakTypes nil nil))              			;T�m kapaklar� bul ve de�i�tir
								(updateModelOfKapak ins kpk doorName)
								(setq kpk (entnext kpk))
							)
							;TODO handle set_Mdata 
							; (set_Mdata ins (list (cons k:kpkName doorName)))
						)
				);cond
				(end_it)   ;Matches with init_it at the beginning of c:kapak
				(init_it)
				(setErrorFunc ozel-er)
			);progn
		);if
		(displayLiberate)
	);while
	(facesel_exit)
)

(if (null c:cdoor_orj) (setq (c:cdoor_orj) (c:cdoor)))
(setq rout_chain_door_empty_index nil)
(defun rout_chain_door ( index / indexx)
	(setq g_noActionOnModulList 1.0)
	(c:cdoor_orj)
	(set (read (strcat "rout_sel" (rtos index 2 0))) (GetKapakName)) 
	(foreach indexx rout_chain_door_empty_index
			(set_tile (strcat "sel" (rtos indexx 2 0)) "")
			(set (read (strcat "rout_sel" (rtos indexx 2 0))) "") 
	)
)

(defun c:cdoor (/ edata temp mLofGrups ret flin upmode found index kNameOld nmEnt mLofL mLofLeval)  ;kmodelold
    (init_it)
	(setq g_noActionOnModulList 1) ; m��teri i�in k�s�tlama getirildi.
	;lisans kontrol�
	(isKitchenWardrobeLicensed)   
	
	(if (null kapak:mLofL)  (loadKapaks))
	
	;Copy of GetKapakName to skip double foundDoorName call
	(if (setq nmEnt (dictsearch (namedobjdict) "AD_DOOR_NAME$$"))
		(setq kNameOld (cdr (assoc 2 nmEnt)))
	)	
	(setq index (ifnull (car (foundDoorName kNameOld)) 0))	
	(if (or (null kNameOld) (null (existDoorNameInAllDoors kNameOld)))
		(progn
			(setq kNameOld (caar kapak:mlofl)
				  index 0
			)
		    ;(CreateXRef "AD_DOOR_NAME$$" kNameOld)
			(SetKapakName kNameOld) 
        )
    )
	;--------
	
					
	;for (door)kapak group 			
	;ret return (1 upmode nth_kapak kapakgroupname) : upmode:  0=genel   1=�zel   2=t�m  3=�st dolaplar  4=Alt dolaplar
	(if (setq ret (modulList 
					index ;(whichth kmodelold kapakgroupname:mLofL)
					ad_KapakGrupList
					ad_mLofKapakGrups)
		) 			
		(progn			
			(setq 	ad_KapakDefGrup (cadddr ret)
					mLofL (read (strcat ad_KapakDefGrup ":mLofL"))
					mLofLeval (eval mLofL))
			;not needed cause of kapak group
			;(setq flin (nth (caddr ret) kapak:mLofL))
			(setq flin (nth (caddr ret) mLofLeval))		  
			(setq upmode (nth (cadr ret) '("genel" "ozel" "tum" "ust" "alt")))		 

			(setq selectedKpkModel (car flin))
			(setq g_forbiddenModules (getForbiddenModulList selectedKpkModel))

			
			(eval (list (cadr flin) (car flin) (caddr flin) (cadddr flin) "ozel")) ;swap kod <=> tip, such as (KAPAKX DOORMODEL ....)		  
		)
	)
	(setq g_noActionOnModulList 0)
    (end_it)
)

(defun GetKapakNameForCore (layer tip kulp / isWallUnit isTallUnit isBaseUnit)
	(setq isWallUnit (or (equal "CAB_BODY_WALL" layer) (member kulp '("GD" "G-" "G2"))))
	(setq isTallUnit (equal "CAB_BODY_TALL" layer))
	(setq isBaseUnit (equal "CAB_BODY_BASE" layer))
	(cond
		((regexMatch tip "[BITN]") 						(GetGlsKapakName))		; D�rtgen Cam kapaksa
		((and isTallUnit (regexMatch tip "[BITN]"))		(GetGlsKapakName))		; Boy dolapsa geneli kullan
		(isTallUnit										(GetTallKapakName))		; Boy dolapsa geneli kullan
		(isWallUnit										(GetUpKapakName))		; �st dolapsa 
		(isTallUnit										((GetKapakName)))     ;Alt dolapsa
		(T 												(GetKapakName)) 
	)
)

;Supply the doorType and KnobPosition, get the relevant doorName
(defun GetKapakNameFor (tip kulp / isWallUnit)
	(GetKapakNameForCore (ifnull gm1:layerName szBodyLayer) tip kulp) ; k�r k��elerde szBodyLayer a bak.
)

(defun kapakblkciz (qKpkBlkNam tip kulp doorName doorModel EN BOY bRedefine kulpModel
                    / oldThickness kfpkBlkNam kanatblk blklst kntlst ENstr BOYstr modelStr 
					zEN kulpAdres szMat kulptemp kapaktemp tempKanatEnt tempKulpEnt doorFlags::doorSupplier  
					doorFlags::removeDoor doorFlags::p2cTags doorFlags::p2cEdges doorFlags::p2cProfiles 
					doorFlags::p2cFrameThick doorFlags::p2cDoorSource doorFlags::p2cProfileHandles 
					doorFlags::p2cSpcRcp dkanat_entslist layNam
					)
					
;---- KISIM 1 Blok tablosuna Kanat'� olu�turup kaydet					
					
	(defun kulp:exception () nil)	;reset kulp position exception if any
	
	(setq oldThickness (getvar "THICKNESS"))
	(setvar "THICKNESS" 0.0)
	(setq kpkBlkNam (eval qKpkBlkNam))

    (if (equal "Z" tip)
        (setq zEN EN EN (car EN))
    )

	(if bRedefine
		(setq kanatblk (strcat (substr kpkBlkNam 1 2) "--" (substr kpkBlkNam 5)))
		(nxtDoorCodeEx qKpkBlkNam 'kanatblk)
	)
	(setq kpkBlkNam (eval qKpkBlkNam) tempKanatsList nil)
	
	; (setq modelStr (if byblock "________" (sub_ doorName 8)))
	(setq modelStr (sub_ doorName 8))

	(setq kntlst (ifnull kntWithDrawerBody (entlast)))
	 
	 ;Kapak bounding box
	(setq boundingInsPnt (list (* (/ EN 2.0) -1) 0.0 0.0))
	(setq boundingThck (convertLength 2.0 5))
	(drawWardobeMainPanel EN BOY boundingThck DEG_90 0.0 0.0 boundingInsPnt "PANELS_WALL")
	(changeEntPro (entlast) 60 1)
	 ;-----------------------
	(setq layNam (if (and ins (equal (type ins) 'ENAME)) (dfetch 8 ins) (ifnull gm1:layerName szBodyLayer)))
	
	(cond  ((or (and (equal "CAB_BODY_WALL" layNam) (regexMatch tip "[ACSMRD]"))
				(and (null GetTallUnitsDoorMaterial)   ; if tall behaves different then do not look handle pos
					(member kulp '("GD" "G-" "G2")) 
					(member tip  '("A" "C" "D" "E" "L" "R" "S" "M"))
					
				)
				;�st k��eler i�in workaround, daha  iyi bir ��z�m bulmak iyi olabilir					
				(and sub_tip (regexMatch tip "[ACSMRD]"))
			)
			(setq szMat (GetWallUnitsDoorMaterial))			
		   )
		   ((and (member tip '("B" "I" "K" "J" "F" "T" "N"))
				 (setq szMat (GetGlassDoorMaterial))
			)		
			
		   )
		   ((or (and (equal "CAB_BODY_TALL" layNam)  GetTallUnitsDoorMaterial) ; a kanat da olsa b kanat da olsa boy kapaktan als�n. k�t�phane i�in buras� revize edildi					
			)
			(setq szMat (GetTallUnitsDoorMaterial))			
		   )
		    ((and (equal "CAB_BODY_BASE" layNam)  GetBaseUnitsDoorMaterial) ; a kanat da olsa b kanat da olsa boy kapaktan als�n. k�t�phane i�in buras� revize edildi					
			(setq szMat (GetBaseUnitsDoorMaterial))			
		   )
		   ((and 
				(member tip  '("A" "C" "D" "E" "L" "R" "S" "M"))
				(setq szMat (GetBaseUnitsDoorMaterial))
		    )			
		   )		   		   
	)
		
	(kapakblkciz_szMat_reserved tip kulp doorName doorModel)
	(kulpPozParams->nullify)
	(if (equal "D" tip)
		(progn
			(setq tempKanatsList (Dkanat (read (strcat doorModel "_Akanat")) modelStr EN BOY (if (equal kulp "KD") g_ClearBaseCorner90X g_ClearWallCorner90X) force szMat)) ;ic kivrik	
			(f:exectip::runKapakModelFunctions doorName tip EN BOY)
			
		)
		(f:exectip)
	)
	(kulpPozParams->collectOverrides)	;is there any kulp posing parameters redefined after nulllified them?
	
	(if g_OverrideDoorArgs (kulpPozParams->reset))

	(idefBLOCKFromEnt kanatblk '(0 0 0) kntlst)
	
	
;---- KISIM 2 Blok tablosuna Kapa�� (Kanat+Kulp) olu�turup kaydet					
    (setq blklst (entlast))
    (setvar "CECOLOR" "BYBLOCK")
    (setvar "CLAYER" "CAB_DOORS")
    (kulpsuzKanadiKoy doorModel kanatblk kulp BOY)
    (setvar "CECOLOR" "BYLAYER")
	
	(setq tempKanatEnt (entlast))
	(kapaklsp_kanatRutn tempKanatEnt)
	
	(runKapakModelRutinFunctions doorName tip EN BOY)
	
	(if szMat (grx_setmaterial tempKanatEnt szMat))	
	
	(if (null kulpModel) 
		(setq kulpModel 
			(cond 
				((and (equal layNam "CAB_BODY_BASE") (not (member (getXref "AD_KULP_COB_BODY_BASE$$") (list nil ""))))
					(getXref "AD_KULP_COB_BODY_BASE$$")
				)
				((and (equal layNam "CAB_BODY_WALL") (not (member (getXref "AD_KULP_COB_BODY_WALL$$") (list nil ""))))
					(getXref "AD_KULP_COB_BODY_WALL$$")
				)
				((and (equal layNam "CAB_BODY_TALL") (not (member (getXref "AD_KULP_COB_BODY_TALL$$") (list nil ""))))
					(getXref "AD_KULP_COB_BODY_TALL$$")
				)
				(T
					(GetKulpModel)
				)
			)
		)
	)

	(kapakblkciz_mat_reserved doorName (entlast) szMat)
	(kapakblkciz_handle_reserved) 						
    (if (kulpKoycaksak tip doorModel)
        (progn
			(if (and (not (member kulp '("0-" "--"))) (null (kulbukoy doorModel tip kulp EN BOY zEN (eval qKpkBlkNam) kulpModel)))
				(progn
					(uyar (sprintf (Xstr "Cannot find a handle named '~s'!") kulpModel) "s")
					(exit)
				)
			)
			(setq tempKulpEnt (entlast))
        )
    )
	(if (equal (length (car kapakblkciz::kapakModelsTriggers)) 8)
		(kapakblkciz::kapakModelsTriggers doorName tempKanatEnt tempKulpEnt (eval qKpkBlkNam))
		(kapakblkciz::kapakModelsTriggers tempKanatEnt tempKulpEnt (eval qKpkBlkNam))
	)
	
	(if (not (equal tip "D")) (kapakblkciz::DoorOpeningSideSymbols tip EN BOY))
	(setvar "THICKNESS" oldThickness)
	
    (idefBLOCKFromEnt (eval qKpkBlkNam) '(0 0 0) blklst)	
)

(defun rK:reCreateKapaksIn_reserved_perDoor (nmUnit ofType doorName kpk )	
	(if (and nmUnit g_myselectedDoorMat (null (get_tile "sel0")))
		(progn			
			(set_block_material nmUnit T "CAB_DOORS" g_myselectedDoorMat)
			(princ (strcat "\nKapak \(" doorName "\) rengi de�i�tirildi. Yeni renk:" g_myselectedDoorMat))
		)
	)	
)

;;;;;;;;;;;;;;;;;;;ADEKO17.7.4.BETA4 ten sonraki setuplarda sil sil sil;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun SetKulpModel (kulpModel / kulpAdres)
	(CreateXRef "AD_KULP$$" kulpModel)	
)
;;;;;;;;;;;;;;;;;;;ADEKO17.7.4.BETA4 ten sonraki setuplarda sil sil sil;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun  prepareRouterChainDialog (dclname /  iii tempchainname dclnameFile rout_chainCount)
	(setq iii 0 rout_chainCount 0)	
	(setq dclnameFile (open (strcat ad_AdekoPath dclname) "w"))
	(write-line (strcat " routerchainSelector: dialog {  	label=\"" (xstr "Select your rule routes") "\"; :column { " ) dclnameFile)

		
	(while  (and (if rout_chains_display_index (getnth rout_chainCount rout_chains_display_index) T) (setq iii (ifnull (getnth rout_chainCount rout_chains_display_index) iii)) (eval (read (setq tempchainname (strcat "rout_chain_name" (rtos iii 2 0))))))						
			(write-line (sprintf ":row {   :button {label=\"~s\" ; key=\"~i\"; action=\"(~s ~i)\";  fixed_width=true; width= 35;}   :edit_box {key=\"sel~i\"; is_enabled=false; width= 45; } } " 
				(list (xstr (eval (read tempchainname)) )  iii "rout_chain_run" iii iii)) dclnameFile)
			
			(setq rout_chainCount (1+ rout_chainCount) iii (1+ iii))
	)	
	
	(write-line (strcat ":text {label=\" " (Xstr "Use command SC to change these selections") "\";}") dclnameFile)
	(write-line "	} ok_only; }" dclnameFile)
	(close dclnameFile)
)

(defun rout_chain_applier ()
	(if (equal (getxref "SOPRANO_PROJECT_TYPE") "SOPRANO_ORDER") (if firm_showCustomerInfo (firm_showCustomerInfo)))
	(if (findfile (strcat ad_DefPath "materials")) (setmatmgrmoduledir (strcat ad_DefPath "materials")))	
	(adeforceupdate)
	(redraw)
	(if (equal (getvar "BASEFILE")  (strcat  ad_defpath "adeko.dwg"))
		(progn
			(if (soprano_getPlinthH) (soprano_setPlinthH (atof (soprano_getPlinthH))))
			(loadKulps nil)
		)
	)	
	(princ)
)

(enableRoutChainMode)
(rout_chain_applier)

(princ)



