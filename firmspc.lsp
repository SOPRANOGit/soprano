(setq g_SopranoAcceptableVersion "SPR21_012 [2021 05 18]")
(setq g_SopranoCurrentVersion (iniread ad_MOD-INI "updater" "version" ""))
(setq g_sopranoVersionDictName "SOPRANO_PROJECT_VERSION")

(setq golaLprofileEndingItemCodeLeft "GUPBL")
(setq golaLprofileEndingItemCodeRight "GUPBR")
(setq golaCProfileEndingItem "GAPB")
(setq golaLProfileCornerItem "GIK")
(setq golaLProfileCornerItemOut "GDK")

(setq g_SopranoSinkCoverItems (list "Yok" "Plastik �rt� -PL" "Alm. Gofraj -GF"))
(setq g_SopranoBzdUstDolapMekanizmaItems (list "Yok" "Blum HK-S -HKS" "Gazl� Piston -PST"))
(setq g_SopranoRenkItems (list "KROM" "ANTRASIT"))
(defun isItNewProject( / firstPickStyle returnValue)
	(setq firstPickStyle (getvar "PICKSTYLE"))
	(setvar	"PICKSTYLE" 0)
	
	(setq returnValue (equal 0 (sslengthx (setq a (ssget "X" (list '(-3 ("AD_MODATA"))))))))
	
	(setvar	"PICKSTYLE" firstPickStyle)
	returnValue
)

(defun createVersionIfNew ( / )
	(if (isItNewProject)
		(CreateXRef g_sopranoVersionDictName g_SopranoCurrentVersion)
	)
)

(createVersionIfNew)

(defun virtual_doWhileNew ()
	(createVersionIfNew)
)

(defun virtual_doWhileOpen ()
	(createVersionIfNew)
	(rout_chain_applier)
)

(load (strcat ad_DefPath "Router\\OTHERS\\pistonModules.lsp"))
(load (strcat ad_DefPath "modulWHDlimit.lsp"))
(load (strcat ad_DefPath "sales.lsp"))
(load (strcat ad_DefPath "spots.lsp"))
(load (strcat ad_DefPath "ledlights.lsp"))

(defun WHDChanger_tanim (/ tempW tempH tempD neww newh newd doNotCloseBaseComp)
	(setq doNotCloseBaseComp T)
	(defun f:init ( )		
		 (if (and kod 
                 (setq mdata (assoc kod mLofL)) )          
			(progn						
				(setq tempW (ifnull (eval (read (strcat (lmm_sfRead kod)  "_WLIST"))) (ifnull (getnth 0 (eval (read (strcat (lmm_sfRead kod)  "_WHDNLIST")))) (ifnull WHDChanger_WLIST (list (lmm_atofFormula (nth 2 mdata)))))))
				(setq tempH (ifnull (eval (read (strcat (lmm_sfRead kod)  "_HLIST")))  (ifnull (getnth 1 (eval (read (strcat (lmm_sfRead kod)  "_WHDNLIST")))) (ifnull WHDChanger_HLIST  (list (lmm_atofFormula (nth 3 mdata)))))))
				(setq tempD (ifnull (eval (read (strcat (lmm_sfRead kod)  "_DLIST"))) (ifnull (getnth 2 (eval (read (strcat (lmm_sfRead kod)  "_WHDNLIST")))) (ifnull WHDChanger_DLIST  (list (lmm_atofFormula (nth 4 mdata)))))))	
				(mapcar '(lambda(x y) (if (null y) (mode_tile x 1)) ) (list "gen" "yuk" "der") (list (car tempW) (car tempH) (car tempD)))			
				(set_tile "kod" kod)
				(set_tile "aciklama" (xstr (last mdata)))				
				(if (getnth 0 tempW) (fillCombobox "gen" (mapcar  '(lambda(x) (convertstr (lmm_atofFormula x))) tempW)))	
				(if (getnth 0 tempH) (fillCombobox "yuk" (mapcar  '(lambda(x) (convertstr (lmm_atofFormula x))) tempH)))
				(if (getnth 0 tempD) (fillCombobox "der" (mapcar  '(lambda(x) (convertstr (lmm_atofFormula x))) tempD)))
				
				(if mozel_mUnit
					(progn
						(setq mOzelW (get_Mdata mozel_mUnit  k:CEn))
						(setq mOzelH (get_Mdata mozel_mUnit  k:CBoy))
						(setq mOzelD (get_Mdata mozel_mUnit  k:CDer))
						 
						(if (member (lmm_atofFormula mOzelW) tempW) (set_tile "gen" (convertstr (getItemIndex (lmm_atofFormula mOzelW) tempW))))
						(if (member (lmm_atofFormula mOzelH) tempH) (set_tile "yuk" (convertstr (getItemIndex (lmm_atofFormula mOzelH) tempH))))
						(if (member (lmm_atofFormula mOzelD) tempD) (set_tile "der" (convertstr (getItemIndex (lmm_atofFormula mOzelD) tempD))))
					)
				)
				
				(action_tile "accept" "(if (f:pick_tiles) (done_dialog 50))")
				(action_tile "cancel" "(done_dialog 0)")
			)
		)
	)
	
	(defun f:pick_tiles ()
			(WHDChanger_accept)
			T
	)
	
	
	(defun f:mdata ()			
		(if newW (setq mdata (replace newW 2 mdata)))
		(if newH (setq mdata (replace newH 3 mdata)))
		(if newD (setq mdata (replace newD 4 mdata)))			
		(if (member (get_Mdata mozel_mUnit  k:mtyp) '("AK1" "UK1"))
			(if newD 
				(progn
					(setq mdata (replace (cons 'QUOTE (list (list newD (get_Mdata mozel_mUnit  k:DEP2)))) 4 mdata)) ;;; derinlik de�i�iminde der2 nin sabit kalmas� istendi.
					(setq mdata (replace (- (getnth 7 mdata) (- mOzelD newD)) 7 mdata)) ;;;derinlik de�i�iminde takozun sabit kalmas� istendi. Yani derinlik de�i�imi kadar EN2 den de de�i�im yapmam gerekti.
				)
			)
		)
		mdata
	)	
	(what_to_do "comboWidHeiDepChanger")
)

(if (null gm1_tanim_name_orj) (setq gm1_tanim_name_orj gm1_tanim_name))
(defun  gm1_tanim_name ( / )
	(if (ifnull (eval (read (strcat (lmm_sfRead kod)  "_WLIST"))) (ifnull (getnth 0 (eval (read (strcat (lmm_sfRead kod)  "_WHDNLIST")))) (ifnull WHDChanger_WLIST nil)))
		(WHDChanger_tanim)
		(gm1_tanim_name_orj)
	)
)

(if (null ak1_tanim_orj) (setq ak1_tanim_orj ak1_tanim))
(defun  ak1_tanim ()
	(if (ifnull (eval (read (strcat (lmm_sfRead kod)  "_WLIST"))) (ifnull (getnth 0 (eval (read (strcat (lmm_sfRead kod)  "_WHDNLIST")))) (ifnull WHDChanger_WLIST nil)))
		(WHDChanger_tanim)
		(ak1_tanim_orj)
	)
)

(if (null uk1_tanim_orj) (setq uk1_tanim_orj uk1_tanim))
(defun  uk1_tanim ()
	(if (ifnull (eval (read (strcat (lmm_sfRead kod)  "_WLIST"))) (ifnull (getnth 0 (eval (read (strcat (lmm_sfRead kod)  "_WHDNLIST")))) (ifnull WHDChanger_WLIST nil)))
		(WHDChanger_tanim)
		(uk1_tanim_orj)
	)
)

(if (null ak2_tanim_orj) (setq ak2_tanim_orj ak2_tanim))
(defun  ak2_tanim ()
	(if (ifnull (eval (read (strcat (lmm_sfRead kod)  "_WLIST"))) (ifnull (getnth 0 (eval (read (strcat (lmm_sfRead kod)  "_WHDNLIST")))) (ifnull WHDChanger_WLIST nil)))
		(WHDChanger_tanim)
		(ak2_tanim_orj)
	)
)

(if (null uk2_tanim_orj)  (setq uk2_tanim_orj uk2_tanim))
(defun  uk2_tanim ()
	(if (ifnull (eval (read (strcat (lmm_sfRead kod)  "_WLIST"))) (ifnull (getnth 0 (eval (read (strcat (lmm_sfRead kod)  "_WHDNLIST")))) (ifnull WHDChanger_WLIST nil)))
		(WHDChanger_tanim)
		(uk2_tanim_orj)
	)
)

(defun loadCSV ( / fo satir satirData uniqueCode realUniqueCode)
	(princ (XSTR "\nImporting tableS. Please wait...\n"))

	(if (setq fo (open (strcat ad_DefPath "KodEslestirme.csv") "r"))
		(progn
			 (setq g_SopranoSubstitudeTable (list )) ; Clear all panel pairing data
			 
			 (while (setq satir (read-line fo))	
				(setq satirData (splitstr satir ";"))
				(if (equal (length satirData) 2) ;uniqueCode,realUniqueCode
						(setq uniqueCode (car satirData) realUniqueCode (cadr satirData))
						(setq uniqueCode nil realUniqueCode nil)
				)
				
				(if (and uniqueCode realUniqueCode) (setq g_SopranoSubstitudeTable (append g_SopranoSubstitudeTable (list (list uniqueCode realUniqueCode)))))
			)
			
			(close fo)
		)
	)

	(if (setq fo (open (strcat ad_DefPath "RenkSec.csv") "r"))
		(progn
			 (setq g_SopranoRenkSecTable nil)
			 
			 (while (setq satir (read-line fo))	
				(setq satirData (splitstr satir ";"))
				(setq g_SopranoRenkSecTable (append g_SopranoRenkSecTable (list (car satirData))))
			)
			
			(close fo)
		)
	)

	(princ)
)
(loadCSV)

(defun lmm_wasop_sawmap_assayPANELs_writeForBtool (outFile / fo realUniqueCode retunValue uniqueCode2 wardrobeCodeInfo visibleSidePureCode notchPanelPureCode parcaKod)
	(setq parcaKod (if (checkStrInStr "KERTIK_" stockCode) (setq notchPanelPureCode (strreplace stockCode "KERTIK_" "")) (if (checkStrInStr "SOPRANO_HINGE_SIDE_" stockCode) (setq visibleSidePureCode (getCodeWithNoVisibleSide stockCode)) stockCode)))
	(setq parcaKod (if (checkStrInStr "SOPRANO_HINGE_SIDE_" parcaKod) (setq visibleSidePureCode (getCodeWithNoVisibleSide parcaKod)) parcaKod))
	(setq realUniqueCode (getnth 1 (assoc (strcat (parse$ comParentModul) "-" parcaKod) g_SopranoSubstitudeTable)) wardrobeCodeInfo (get_mdata (car (convertsstolist (ssget "x" (list '(0 . "INSERT") (cons 2 comParentModul))))) k:WPanelUsageType))
	(setq uniqueCode2
		(if wardrobeCodeInfo
			(if (checkStrInStr "independent" wardrobeCodeInfo) (ifnull realUniqueCode "") "")
			(ifnull realUniqueCode "")
		)
	)
	(write-line "<PART2CAM_PANELS>" outFile)	
		(write-line 
			(strcat 
						"\t<GROUP>PART2CAM_PANELS</GROUP>\n"
						"\t\t\t\t\t\t\t<CODE>" stockCode "</CODE>\n"
						"\t\t\t\t\t\t\t<UNIQUECODE>" (lmm_getPartNameByMode (list comCustomCode)) "</UNIQUECODE>\n"
						"\t\t\t\t\t\t\t<UNIQUECODE2>" (if visibleSidePureCode (strcat uniqueCode2 "M") uniqueCode2) "</UNIQUECODE2>\n"
						"\t\t\t\t\t\t\t<NOTCH_STATE>" (if notchPanelPureCode "1" "0") "</NOTCH_STATE>\n"
						"\t\t\t\t\t\t\t<UNIQUECODEALNUM>" (lmm_getPartNameByMode (list (lmm_convertToAlphaNum comCustomCode))) "</UNIQUECODEALNUM>\n"
						"\t\t\t\t\t\t\t<PUREUNIQUECODE>" comCustomCode "</PUREUNIQUECODE>\n"							
						"\t\t\t\t\t\t\t<PARENT>" comParentModul "</PARENT>\n"
						"\t\t\t\t\t\t\t<PARENT2>" (parse$ comParentModul) "</PARENT2>\n"
						"\t\t\t\t\t\t\t<MODUL>" comParentModul "</MODUL>\n"
						"\t\t\t\t\t\t\t<MODULPURECODE>" (parse$ comParentModul) "</MODULPURECODE>\n"
						"\t\t\t\t\t\t\t<MODULDEF>" tempComModulDef "</MODULDEF>\n"
						"\t\t\t\t\t\t\t<MODULKCODE>" comParentModulKitchenCode "</MODULKCODE>\n"
						"\t\t\t\t\t\t\t<MODULKPURECODE>" (parse$ comParentModulKitchenCode) "</MODULKPURECODE>\n"
						"\t\t\t\t\t\t\t<MODULKPOSE>" comParentModulKitchenPose "</MODULKPOSE>\n"
						"\t\t\t\t\t\t\t<MODULKGROUPPOSE>" comParentModulKitchenGroupPose "</MODULKGROUPPOSE>\n"
						"\t\t\t\t\t\t\t<TYPE>" comType "</TYPE>\n"
						"\t\t\t\t\t\t\t<MATERIAL>" (ifnull (car (getmatcode comMaterial)) comMaterial) "</MATERIAL>\n"
						"\t\t\t\t\t\t\t<MATERIAL_NAME>" (ifnull comMaterial "") "</MATERIAL_NAME>\n"
						"\t\t\t\t\t\t\t<MATERIAL_CODE>" (ifnull (car (getmatcode comMaterial)) "") "</MATERIAL_CODE>\n"
						"\t\t\t\t\t\t\t<MATERIAL_GRAIN>" (if (equal (lmm_getMaterialGrainState comMaterial) 1) "1" "0") "</MATERIAL_GRAIN>\n"
						"\t\t\t\t\t\t\t<MATERIAL_ROTATE>" (if (equal (lmm_getMaterialGrainState comMaterial) 1) "0" "1") "</MATERIAL_ROTATE>\n"
						"\t\t\t\t\t\t\t<SFACEUNIQUECODE>" comSFCustomCode "</SFACEUNIQUECODE>\n"
						"\t\t\t\t\t\t\t<SHORTUCODE>" tempUniqueShortCode "</SHORTUCODE>\n"
						"\t\t\t\t\t\t\t<SFACESHORTUCODE>" tempSFUniqueShortCode "</SFACESHORTUCODE>\n"
						"\t\t\t\t\t\t\t<EDGETOP>" (nth 1 tempEdgesList) "</EDGETOP>\n"
						"\t\t\t\t\t\t\t<EDGETOP_MAT>" (nth 0 tempEdgesList) "</EDGETOP_MAT>\n"
						"\t\t\t\t\t\t\t<EDGERIGHT>" (nth 3 tempEdgesList) "</EDGERIGHT>\n"
						"\t\t\t\t\t\t\t<EDGERIGHT_MAT>" (nth 2 tempEdgesList) "</EDGERIGHT_MAT>\n"
						"\t\t\t\t\t\t\t<EDGEBOTTOM>" (nth 5 tempEdgesList) "</EDGEBOTTOM>\n"
						"\t\t\t\t\t\t\t<EDGEBOTTOM_MAT>" (nth 4 tempEdgesList) "</EDGEBOTTOM_MAT>\n"
						"\t\t\t\t\t\t\t<EDGELEFT>" (nth 7 tempEdgesList) "</EDGELEFT>\n"
						"\t\t\t\t\t\t\t<EDGELEFT_MAT>" (nth 6 tempEdgesList) "</EDGELEFT_MAT>\n"
						"\t\t\t\t\t\t\t<REAL_AREA>" (rtos (+ comArea (if (equal lmm-s-useOverconsumptionQuantity "1") (lmm_getOverConsQuantity comMaterial) 0.0))) "</REAL_AREA>\n"											
						"\t\t\t\t\t\t\t<H>" (rtos (if (equal lmm-s-optimikBtoolDoNotAdjustPartsDims "1") comHei (lmm_wasop_sawmap_calcDimWithoutEdgeStrip comHei (nth 1 tempEdgesList) (nth 5 tempEdgesList)))) "</H>\n"
						"\t\t\t\t\t\t\t<W>" (rtos (if (equal lmm-s-optimikBtoolDoNotAdjustPartsDims "1") comWid (lmm_wasop_sawmap_calcDimWithoutEdgeStrip comWid (nth 3 tempEdgesList) (nth 7 tempEdgesList)))) "</W>\n"
						"\t\t\t\t\t\t\t<T>" (rtos comThick) "</T>\n"
						"\t\t\t\t\t\t\t<HOLEQUANTITY>" (rtos (getHoleQuantityOnPanel (lmm_getPartNameByMode (list comCustomCode)))) "</HOLEQUANTITY>\n"
						"\t\t\t\t\t\t\t<GROOVE_POS>" (xstr tempGroovePosition) "</GROOVE_POS>\n"
						"\t\t\t\t\t\t\t<GROOVE_POS_EN>" tempGroovePosition "</GROOVE_POS_EN>\n"	
						"\t\t\t\t\t\t\t<GROOVEEXIST>" (if (not (equal tempGroovePosition "")) "OK" "") "</GROOVEEXIST>\n"							
						"\t\t\t\t\t\t\t<GROOVE_EDGEDIST>" (rtos (ifnull (getnth 0 tempGrooveProp) 0.0) 2 0) "</GROOVE_EDGEDIST>\n"
						"\t\t\t\t\t\t\t<GROOVE_WID>" (rtos (ifnull (getnth 1 tempGrooveProp) 0.0) 2 0) "</GROOVE_WID>\n"
						"\t\t\t\t\t\t\t<LENGTH>" (rtos (if (equal lmm-s-optimikBtoolDoNotAdjustPartsDims "1") comWid (lmm_wasop_sawmap_calcDimWithoutEdgeStrip comWid (nth 3 tempEdgesList) (nth 7 tempEdgesList)))) "</LENGTH>\n"
						"\t\t\t\t\t\t\t<WIDTH>" (rtos (if (equal lmm-s-optimikBtoolDoNotAdjustPartsDims "1") comHei (lmm_wasop_sawmap_calcDimWithoutEdgeStrip comHei (nth 1 tempEdgesList) (nth 5 tempEdgesList)))) "</WIDTH>\n"							
						"\t\t\t\t\t\t\t<QUANTITY>" (rtos (* (ifnull lmm-ProjectMultiple 1) (ifnull lmm-FirmFactor 1) (if (equal lmm-s-useOverconsumption "1") (lmm_getOverCons comMaterial) 1))) "</QUANTITY>\n"
						"\t\t\t\t\t\t\t<REALQUANTITY>" (rtos (* (ifnull lmm-ProjectMultiple 1) 1)) "</REALQUANTITY>\n"
						"\t\t\t\t\t\t\t<PANELPROJECT>" (ifnull lmm-ProjectName "") "</PANELPROJECT>\n"
						"\t\t\t\t\t\t\t<LABEL>" labelStr "</LABEL>\n"							
						"\t\t\t\t\t\t\t<TGSTR1>" tag1Str "</TGSTR1>\n"
						"\t\t\t\t\t\t\t<TGSTR2>" tag2Str "</TGSTR2>\n"
						"\t\t\t\t\t\t\t<MODELTG>" modeltagStr "</MODELTG>\n"
						"\t\t\t\t\t\t\t<COSTTYPETG>" (ifnull costtypetagStr "") "</COSTTYPETG>\n"
						(ifnull tagXMLStrNonParameter "")
			)	outFile
		)
	(write-line "</PART2CAM_PANELS>" outFile)
)

(defun lmm_wasop_sawmap_assayEDGESTRIPSasDIFFERENTLIST (outFile compname panelCode modulCode comHei comWid / tempComModulEnt tempPanelPureCode wardrobeCodeInfo parent2 realParent visibleSidePureCode notchPanelPureCode tempComModulDef labelStr tag1Str tag2Str modeltagStr costtypetagStr tempMaterial parcaKod)
	
	(setq tempComModulEnt  (ifnull (getnth 0  (convertsstolist (lmm_getModulCodeIncludeModuls comParentModul))) (getParentEntByPose comParentModulKitchenPose comParentModulKitchenGroupPose)))
	(setq tempComModulDef (last (read (get_Mdata tempComModulEnt k:mData))))
	(lmm_findPanelStrips compname 'edgeStripBottom 'edgeStripLeft 'edgeStripTop 'edgeStripRight 'edgeUnknownList)
	(setq tempListCount 1)

	(setq tempPanelPureCode (ifnull (lmm_getLMMdata compname lmm-k-purecode) ""))
	(setq parcaKod (if (checkStrInStr "KERTIK_" tempPanelPureCode) (setq notchPanelPureCode (strreplace tempPanelPureCode "KERTIK_" "")) (if (checkStrInStr "SOPRANO_HINGE_SIDE_" tempPanelPureCode) (setq visibleSidePureCode (getCodeWithNoVisibleSide tempPanelPureCode)) tempPanelPureCode)))
	(setq parcaKod (if (checkStrInStr "SOPRANO_HINGE_SIDE_" parcaKod) (setq visibleSidePureCode (getCodeWithNoVisibleSide parcaKod)) parcaKod))
	(setq realParent (getnth 1 (assoc (strcat (parse$ modulCode) "-" parcaKod) g_SopranoSubstitudeTable)) wardrobeCodeInfo (get_mdata (car (convertsstolist (ssget "x" (list '(0 . "INSERT") (cons 2 modulCode))))) k:WPanelUsageType))
	
	(setq parent2
		(if wardrobeCodeInfo
			(if (checkStrInStr "independent" wardrobeCodeInfo) (ifnull realParent "") "")
			(ifnull realParent "")
		)
	)
	
	(cond ( (equal outputType "btool")
			(mapcar 
				'(lambda (X Y)
					(if X 
						(progn
							(write-line "<PART2CAM_EDGESTRIPS>" outFile)
							
							(write-line
								(strcat		"\t<GROUP>PART2CAM_EDGESTRIPS</GROUP>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<CODE>" (lmm_getLMMdata X lmm-k-pureCode) "</CODE>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<UNIQUECODE>" (lmm_getLMMdata X lmm-k-code) "</UNIQUECODE>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<PARENT>" panelCode "</PARENT>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<PARENT2>" (if visibleSidePureCode (strcat parent2 "M") parent2) "</PARENT2>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MODUL>" modulCode "</MODUL>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MODULPURECODE>" (parse$ modulCode) "</MODULPURECODE>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MODULDEF>" tempComModulDef "</MODULDEF>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MODULKCODE>" comParentModulKitchenCode "</MODULKCODE>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MODULKPURECODE>" (parse$ comParentModulKitchenCode) "</MODULKPURECODE>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MODULKPOSE>" comParentModulKitchenPose "</MODULKPOSE>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MODULKGROUPPOSE>" comParentModulKitchenGroupPose "</MODULKGROUPPOSE>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<TYPE>" (lmm_getLMMdata X lmm-k-type) "</TYPE>\n"	
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MATERIAL>" (ifnull (car (getmatcode (lmm_getLMMdata X lmm-k-material))) (lmm_getLMMdata X lmm-k-material)) "</MATERIAL>\n"	
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MATERIAL_NAME>" (ifnull (lmm_getLMMdata X lmm-k-material) "") "</MATERIAL_NAME>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<MATERIAL_CODE>" (ifnull (getnth 1 (assoc (ifnull (car (getmatcode (lmm_getLMMdata X lmm-k-material))) (lmm_getLMMdata X lmm-k-material)) g_SopranoSubstitudeTable)) "") "</MATERIAL_CODE>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<POSITION>" Y "</POSITION>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<T>" (convertStr (getEntPro X 39)) "</T>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<LENGTH>" (convertstr (+ (if (equal (rem tempListCount 2) 1) comWid comHei ) (if (equal lmm-s-useOverconsumptionQuantity "1") (lmm_getOverConsQuantity (lmm_getLMMdata X lmm-k-material)) 0.0)   ) ) "</LENGTH>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<QUANTITY>" (rtos (* (ifnull lmm-ProjectMultiple 1) (ifnull lmm-FirmFactor 1) (if (equal lmm-s-useOverconsumption "1") (lmm_getOverCons (lmm_getLMMdata X lmm-k-material)) 1))) "</QUANTITY>\n"
											"\t\t\t\t\t\t\t\t\t\t\t\t\t<EDGEPROJECT>" (ifnull lmm-ProjectName "") "</EDGEPROJECT>"
								)	outFile
							)
							
							(write-line "</PART2CAM_EDGESTRIPS>" outFile)
						)
					)
					(setq tempListCount (1+ tempListCount))
				)
				(list edgeStripBottom edgeStripLeft edgeStripTop edgeStripRight)
				(list "BOTTOM" "LEFT" "TOP" "RIGHT")
			)
		)
		( (equal outputType "fastPrice")
				
		  (mapcar 
				'(lambda (X)
					(if X 
						(progn
							(write-line "<PART2CAM_FASTPRICE>" outFile)
							(write-line (sprintf "	<CODE>~s</CODE>
													<MODUL>~s</MODUL>							
													<MODULDEF>~s</MODULDEF>
													<QUANTITY>~s</QUANTITY>
													<QUANTITYUNIT>~s</QUANTITYUNIT>
												 " 											
										(list											
											(ifnull (lmm_getLMMdata X lmm-k-material) (lmm_getLMMdata X lmm-k-pureCode))											
											modulCode
											tempComModulDef											
											(convertstr	(* (+ (if (equal (rem tempListCount 2) 1) comWid comHei ) (if (equal lmm-s-useOverconsumptionQuantity "1") (lmm_getOverConsQuantity (lmm_getLMMdata X lmm-k-material)) 0.0)) 0.001))
											"m"  								
										))
								outFile
							)
							(write-line "</PART2CAM_FASTPRICE>" outFile)
						)
					)
					(setq tempListCount (1+ tempListCount))
				)
				(list edgeStripBottom edgeStripLeft edgeStripTop edgeStripRight)				
			)		
		)
	)
)

;realItemParentTag parametresini lokalle�tirmek i�in override edildi. Ba�ka bir i�lem yap�lmad�.
(defun lmm_wasop_sawmap_assayITEMs (compname  outFile  outputType / realItemParentTag comQuantity comQuantityUnit comCode comPureCode comParentModul comType stockCode tempComModulDef labelStr tag1Str tag2Str labelStr tag1Str tag2Str modeltagStr costtypetagStr tag3Str  allTagsList tagXMLStr tagXMLStrNonParameter tempComModul)
		
		(lmm_wasop_sawmap_assayITEMs_preloads compname  outputType)
		
		(cond 
			( (equal outputType "optimik")
			  (lmm_wasop_sawmap_assayITEMs_writeForOptimik outFile)
			)
		
			( (equal outputType "btool")
			  (lmm_wasop_sawmap_assayITEMs_writeForBtool outFile)	
			)
			
			( (equal outputType "fastPrice")
			  (lmm_wasop_sawmap_assayITEMs_writeForFastPriceBtool outFile)	
			)
		)
)

(defun lmm_wasop_sawmap_assayITEMs_writeForBtool (outFile /)
	(write-line "<PART2CAM_ITEMS>" outFile)	
	
	(write-line
		(strcat 	
					"\t<GROUP>PART2CAM_ITEMS</GROUP>"
					"\t\t\t\t\t\t\t\t\t<CODE>" stockCode "</CODE>\n"
					"\t\t\t\t\t\t\t\t\t<UNIQUECODE>" comCode "</UNIQUECODE>\n"
					"\t\t\t\t\t\t\t\t\t<PARENT>" comParent "</PARENT>\n"
					"\t\t\t\t\t\t\t\t\t<PARENT2>" (if realItemParentTag realItemParentTag (parse$ comParent)) "</PARENT2>\n"
					"\t\t\t\t\t\t\t\t\t<MODUL>" comParentModul "</MODUL>\n"
					"\t\t\t\t\t\t\t\t\t<MODULPURECODE>" (parse$ comParentModul) "</MODULPURECODE>\n"
					"\t\t\t\t\t\t\t\t\t<MODULDEF>" tempComModulDef "</MODULDEF>\n"
					"\t\t\t\t\t\t\t\t\t<MODULKCODE>" comParentModulKitchenCode "</MODULKCODE>\n"
					"\t\t\t\t\t\t\t\t\t<MODULKPURECODE>" (parse$ comParentModulKitchenCode) "</MODULKPURECODE>\n"
					"\t\t\t\t\t\t\t\t\t<MODULKPOSE>" comParentModulKitchenPose "</MODULKPOSE>\n"
					"\t\t\t\t\t\t\t\t\t<MODULKGROUPPOSE>" comParentModulKitchenGroupPose "</MODULKGROUPPOSE>\n"
					"\t\t\t\t\t\t\t\t\t<TYPE>" comType "</TYPE>\n"
					"\t\t\t\t\t\t\t\t\t<QUANTITY>" (convertstr (+ (* (ifnull comQuantity 1) (ifnull lmm-ProjectMultiple 1) (ifnull lmm-FirmFactor 1) (if (equal lmm-s-useOverconsumption "1") (lmm_getOverCons comCode) 1)) (if (equal lmm-s-useOverconsumptionQuantity "1") (lmm_getOverConsQuantity comCode) 0.0) )) "</QUANTITY>\n"											
					"\t\t\t\t\t\t\t\t\t<QUANTITY_UNIT>" comQuantityUnit "</QUANTITY_UNIT>\n"
					"\t\t\t\t\t\t\t\t\t<ITEMPROJECT>" (ifnull lmm-ProjectName "") "</ITEMPROJECT>\n"
					"\t\t\t\t\t\t\t\t\t<LABEL>" labelStr "</LABEL>\n"
					"\t\t\t\t\t\t\t\t\t<TGSTR1>" tag1Str "</TGSTR1>\n"
					"\t\t\t\t\t\t\t\t\t<TGSTR2>" tag2Str "</TGSTR2>\n"
					"\t\t\t\t\t\t\t\t\t<MODELTG>" modeltagStr "</MODELTG>\n"
					"\t\t\t\t\t\t\t\t\t<COSTTYPETG>" costtypetagStr "</COSTTYPETG>\n"
					(ifnull tagXMLStrNonParameter "")
		)	outFile
	)
	
	(write-line "</PART2CAM_ITEMS>" outFile)	

)

(defun lmm_wasop_sawmap_assayITEMs_preloads (compname outputType / lastTagString)
	
	;common variables	
	(setq 	comParentModul (lmm_getLMMdata compname lmm-k-parentModul )
			comQuantityUnit (lmm_getLMMdata compname lmm-k-quantityUnit)
			comCode (lmm_getLMMdata compname lmm-k-code)
			stockCode (lmm_getLMMdata compname lmm-k-pureCode)
			comQuantity (lmm_getLMMdata compname lmm-k-quantity)	
			comParentModulKitchenCode (lmm_getLMMdata compname lmm-k-pmodulkitchenCode )
			comParentModulKitchenPose (lmm_getLMMdata compname lmm-k-pmodulkitchenPose )
			comParentModulKitchenGroupPose (lmm_getLMMdata compname lmm-k-pmodulkitchenGroupPose )
			tempComModulEnt  (ifnull (getnth 0  (convertsstolist (lmm_getModulCodeIncludeModuls comParentModul))) (getParentEntByPose comParentModulKitchenPose comParentModulKitchenGroupPose))
			tempComModulDef (last (read (get_Mdata tempComModulEnt k:mData)))
	)
	;common variables	
	
	(cond 
		( (equal outputType "fastPrice")
			(princ)
		)	
		(T	
			(setq     comType (lmm_getLMMdata compname lmm-k-type) comParent (lmm_getLMMdata compname lmm-k-parent ))
			
			(setq  allTagsList (splitStrTkn (ifnull (lmm_getLMMdata compname lmm-k-tags) ""  ) lmm-tagSeperator) labelStr  (ifnull (lmm_getLMMdata compname lmm-k-label) "") tag1Str (ifnull (lmm_getLMMdata compname lmm-k-sourcetag1) "") tag2Str (ifnull (lmm_getLMMdata compname lmm-k-usagetypetag2) "") modeltagStr (ifnull (lmm_getLMMdata compname lmm-k-modeltypetag) "") costtypetagStr (ifnull (lmm_getLMMdata compname lmm-k-costtypetag) "")  tagXMLStr ""  tagXMLStrNonParameter "")
			
			(setq allTagsListCounter 10)
			(repeat (length allTagsList)
				(if (equal (- allTagsListCounter 10) (- (length allTagsList) 1)) (setq lastTagString "") (setq lastTagString "\n"))
				(setq tagXMLStr (strcat tagXMLStr "<TGSTR" (rtos allTagsListCounter)  ">~s<" "/TGSTR" (rtos allTagsListCounter) ">") )
				(if (equal allTagsListCounter 11) (setq realItemParentTag (nth (- allTagsListCounter 10) allTagsList) )) ; 11-> itemlarda, sopranonun parent item kodu.
				(setq tagXMLStrNonParameter (strcat tagXMLStrNonParameter "\t\t\t\t\t\t\t\t\t<TGSTR" (rtos allTagsListCounter)  ">" (nth (- allTagsListCounter 10) allTagsList) "<" "/TGSTR" (rtos allTagsListCounter) ">" lastTagString) )
				(setq allTagsListCounter (+ 1 allTagsListCounter))
			)					
		)
	)
)

(defun getCodeWithNoVisibleSide (sideCode / )
	(substr sideCode (+ (strlen "SOPRANO_HINGE_SIDE_") 1) (- (strlen sideCode) (strlen "SOPRANO_HINGE_SIDE_")))
)

(defun getRealUniqueCode (code / actionStatus returnValue)
	(if (not (new_dialog "createSopranoPartCode" (load_dialog (strcat ad_defpath "firmfaces.dcl")))) (exit))
	(set_tile "partCodeText" (strcat code "  :"))
	(action_tile "accept" "(setq returnValue (if (equal (get_tile \"partCode\") \"\") (getRealUniqueCode code) (get_tile \"partCode\"))) (done_dialog 1)")
	(setq actionStatus (start_dialog))
	(if (not (equal actionStatus 1))  (setq returnValue (getRealUniqueCode code)))
	returnValue
)

(setq sopranoBoyGonderilecekGolaProfilListesi (list
													"GDCP"
													"GDLP"
												)
)

(setq g_doNotUseAutoCornices T g_doNotUseAutoPLinth T g_doNotUseAutoWorktop T g_DoNotOpenFirmSettings 1)

(defun firm_showCustomerInfo (/ dialogResult)
	(if (not (new_dialog "firmfaces_customerInfo" (load_dialog (strcat ad_DefPath "firmfaces.dcl")))) (exit))
	
	(start_list "firmfaces_ci_f12")		
		(mapcar '(lambda(x) (add_list (car x))) firm_salesArchitectList)		
	(end_list)
	
	(start_list "firmfaces_ci_f18")		
		(mapcar '(lambda(x) (add_list (car x))) firm_salesShapeList)		
	(end_list)
	
	(start_list "firmfaces_ci_f19")		
		(mapcar '(lambda(x) (add_list (car x))) firm_salesTypeList)		
	(end_list)
	
	(start_list "firmfaces_ci_f20")		
		(mapcar '(lambda(x) (add_list (car x))) firm_salesAttiributeList)		
	(end_list)
  
	(action_tile "accept" "(firm_saveCustomerInfo)(done_dialog 1)")
	(firm_loadCustomerInfo)
	(set_tile "firmfaces_ci_f1" firm_Current_Account)
	(mode_tile "firmfaces_ci_f1" 1)
	(set_tile "firmfaces_ci_f2" firm_Current_Name)
	(mode_tile "firmfaces_ci_f2" 1)
	(setq dialogResult (start_dialog))
	(if (null (equal dialogResult 1))
		(firm_showCustomerInfo)
		(if (not (firm_EmptyInfoCheck)) (firm_showCustomerInfo))
	)
)

(setq firmfaces_text_dictlist
		(list 
				(list "firmfaces_ci_f1" "ADEKO_CURRENT_ACCOUNT")
				(list "firmfaces_ci_f2" "ADEKO_CURRENT_NAME")
				(list "firmfaces_ci_f3" "ADEKO_CUSTOMER_ADDRESS")
				(list "firmfaces_ci_f4" "ADEKO_CUSTOMER_DISTRICT")
				(list "firmfaces_ci_f5" "ADEKO_CUSTOMER_CITY")
				(list "firmfaces_ci_f6" "ADEKO_CUSTOMER_POSTCODE")
				(list "firmfaces_ci_f7" "ADEKO_CUSTOMER_NAME")
				(list "firmfaces_ci_f8" "ADEKO_CUSTOMER_PHONE1")
				(list "firmfaces_ci_f9" "ADEKO_CUSTOMER_EMAIL")
				(list "firmfaces_ci_f10" "ADEKO_CUSTOMER_COLORSAMPLE")
				(list "firmfaces_ci_f11" "ADEKO_CUSTOMER_NOTE")					
				(list "firmfaces_ci_f15" "ADEKO_PROJECT_TYPE")		
				(list "firmfaces_ci_f16" "ADEKO_PROJECT_FLAT")		
				(list "firmfaces_ci_f17" "ADEKO_PROJECT_NO")				
		)	
)

(setq firmfaces_combo_dictlist
		(list  
				(list "firmfaces_ci_f12" "ADEKO_DESIGNER_SALES_ARCHITECT" "firm_salesArchitectList")
				(list "firmfaces_ci_f18" "ADEKO_PROJECT_SALES_SHAPE" "firm_salesShapeList")								
				(list "firmfaces_ci_f19" "ADEKO_PROJECT_SALES_TYPE" "firm_salesTypeList")								
				(list "firmfaces_ci_f20" "ADEKO_PROJECT_SALES_ATTIRIBUTE" "firm_salesAttiributeList")								
		)	
)

(setq firmfaces_checkEmptyDictList
	(list
		"ADEKO_CURRENT_ACCOUNT"
		"ADEKO_CURRENT_NAME"
		"ADEKO_CUSTOMER_ADDRESS"
		"ADEKO_CUSTOMER_DISTRICT"
		"ADEKO_CUSTOMER_CITY"
		; "ADEKO_CUSTOMER_POSTCODE"
		"ADEKO_CUSTOMER_NAME"
		"ADEKO_CUSTOMER_PHONE1"
		; "ADEKO_CUSTOMER_EMAIL"
		"ADEKO_CUSTOMER_COLORSAMPLE"
		; "ADEKO_CUSTOMER_NOTE"	
		"ADEKO_DESIGNER_SALES_ARCHITECT"	
		"ADEKO_PROJECT_TYPE"	
		"ADEKO_PROJECT_FLAT"		
		"ADEKO_PROJECT_NO"
		"ADEKO_PROJECT_SALES_SHAPE"							
		"ADEKO_PROJECT_SALES_TYPE"							
		"ADEKO_PROJECT_SALES_ATTIRIBUTE"
	)
)

(setq firmfaces_checkEmptyDictList_V2
	(list
		"ADEKO_CURRENT_ACCOUNT"
		"ADEKO_CURRENT_NAME"
		"ADEKO_CUSTOMER_ADDRESS"
		"ADEKO_CUSTOMER_DISTRICT"
		"ADEKO_CUSTOMER_CITY"
		; "ADEKO_CUSTOMER_POSTCODE"
		"ADEKO_CUSTOMER_NAME"
		"ADEKO_CUSTOMER_PHONE1"
		; "ADEKO_CUSTOMER_EMAIL"
		"ADEKO_CUSTOMER_COLORSAMPLE"
		; "ADEKO_CUSTOMER_NOTE"	
		"ADEKO_DESIGNER_SALES_ARCHITECT"	
		; "ADEKO_PROJECT_TYPE"	
		; "ADEKO_PROJECT_FLAT"		
		; "ADEKO_PROJECT_NO"
		"ADEKO_PROJECT_SALES_SHAPE"							
		"ADEKO_PROJECT_SALES_TYPE"							
		"ADEKO_PROJECT_SALES_ATTIRIBUTE"
	)
)

(defun firm_EmptyInfoCheck( / eleman returnValue emptyList)
	(setq returnValue T)
	(setq emptyList (if (equal (getXRef "ADEKO_PROJECT_SALES_SHAPE") "P") firmfaces_checkEmptyDictList_V2 firmfaces_checkEmptyDictList))
	(foreach eleman emptyList
		(if (member (getXref eleman) (list nil ""))
			(if returnValue (setq returnValue nil))
		)
	)
	
	returnValue
)

(defun firm_saveCustomerInfo ()
	(mapcar '(lambda (x) (CreateXRef (nth 1 x) (get_tile (nth 0 x)))) firmfaces_text_dictlist)
	(mapcar '(lambda (x) (CreateXRef (nth 1 x) (cadr (nth (atoi (get_tile (nth 0 x))) (eval (read (nth 2 x))) )))) firmfaces_combo_dictlist)
	(princ)
)

(defun firm_loadCustomerInfo( / tempValue0 tempValue1 tempValue2 tempValue3)
	(mapcar '(lambda (x) (if (getXRef (nth 1 x)) (set_tile (nth 0 x) (getXRef (nth 1 x))))) firmfaces_text_dictlist)

	(if (not (member (setq tempValue0 (getxref "ADEKO_DESIGNER_SALES_ARCHITECT")) (list nil "")))
		(set_tile "firmfaces_ci_f12" (getItemIndex2 tempValue0 firm_salesArchitectList))
	)

	(if (not (member (setq tempValue1 (getxref "ADEKO_PROJECT_SALES_SHAPE")) (list nil "")))
		(set_tile "firmfaces_ci_f18" (getItemIndex2 tempValue1 firm_salesShapeList))
	)
	
	(if (not (member (setq tempValue2 (getxref "ADEKO_PROJECT_SALES_TYPE")) (list nil "")))
		(set_tile "firmfaces_ci_f19" (getItemIndex2 tempValue2 firm_salesTypeList))
	)
	
	(if (not (member (setq tempValue3 (getxref "ADEKO_PROJECT_SALES_ATTIRIBUTE")) (list nil "")))
		(set_tile "firmfaces_ci_f20" (getItemIndex2 tempValue3 firm_salesAttiributeList))
	)
)

(setq c:ci  firm_showCustomerInfo)


 (defun firm_startup ()
		; (firm_showCustomerInfo)
		(c:sc)
 )
 
; (if (null g_ExplodeCmd)
	; (progn
		; (command "_.UNDEFINE" "_EXPLODE")
		; (setq g_ExplodeCmd T)
	; )
; )

(if (null g_UndefinedCmdNew)
	(progn
		(command "_.UNDEFINE" "_NEW")
		(setq g_UndefinedCmdNew T)
		
		(defun c:yeni ()
			(command "_.NEW")
			(firm_startup)
		)

		(defun c:YEN� () (c:yeni))
		(defun c:new () (c:yeni))				
	)
)

(defun c:handle ( / refmoduls)
    (init_it)
	(setq g_noActionOnModulList 1) ;sadece �zel kulp se�ilmeye zorlanacak
	(isKitchenWardrobeLicensed)	
	(loadKulps nil)
	
	(if (equal (getKulpModel) "ENTEGRE") (setq refmoduls T))
    (nesne_yer "kulp" (Xstr "Handle Types") nil)
	(setq g_noActionOnModulList 0)
    (end_it)
	(if (or (equal (getKulpModel) "ENTEGRE") refmoduls) (refreshUnits "_ALL"))
	(setq refmoduls nil)  
)

(defun AK1 (kod ADEN ADH ADDer gor_GEN brutEN kap_PAY ceklist raflist lstData kapak_tip imalTipi note
            / yp1 yp2 yp3 aci ADEN1 ADEN2 LR kapak# sBazaType mdata SubEs i ADDer2 szMData kulpyer kulpyerkpkUst
              notch_type notch_dims dCornAng szKpmLayer blindDoorTouchWall isHbase szBodyLayer specialDoor unitCArea isManualCArea drawerRecipe bodyAuxRcpsList noBodyDrawing divDwgParamsList doorTopTrim doorAnimateScript
				previousDrawers tempDrawer iii	
				divsRafDikOffsets
				drawersListWithRcp
				drawerOffsetList
				fixedShelfList
				doorsPropList
				tempRestH
				kodFormula 
				doorAnimateScript 
				leftDwgParamsList rightDwgParamsList moduleDwgListsList noHandle coverMat  tempparamlist
				modulgolaOrderList ak1rutin tempDoorMat
				doorTopTrimReal
				zTop
           )

	;lisans kontrol�
	;(isKitchenLicensed)	
	(generateKodFormula)
	
    (setq dCornAng DEG_90)
    (if (null gor_GEN)
        (progn
            (initget "40 45 50")
            (setq gor_GEN (getkword (Xstr "\nDoor width 40,45,<50>: ")))
            (if (null gor_GEN) (setq gor_GEN "50"))
            (setq gor_GEN (atof gor_GEN))
            (setq brutEN (+ (convertLength 60.0 5) gor_GEN))
        )
    )
    (if (null ADH)   (setq ADH ad_ADH))
	(generateADHFormula)
	
    (if (isList lstData)
        (setq kapak# 	  (nth 0 lstData)
			  SubEs  	  (nth 1 lstData)
			  sBazaType   (nth 2 lstData)
              szKpmLayer  (nth 3 lstData)
			  LR		  (nth 4 lstData)
			  blindDoorTouchWall (nth 5 lstData)
			  specialDoor (nth 6 lstData)
			  unitCArea (nth 7 lstData)
			  isManualCArea (nth 8 lstData)
			  drawerRecipe (nth 9 lstData)
			  bodyAuxRcpsList (nth 10 lstData)
			  bodyScriptFiles (nth 11 lstData)
			  leftDwgParamsList (nth 12 lstData)
			  doorTopTrim (nth 13 lstData)
			  doorTopTrimReal (ifnull (lmm_atofFormula (nth 13 lstData)) 0.0)
			  doorAnimateScript (nth 14 lstData)
			  rightDwgParamsList (nth 15 lstData)
			  noHandle (nth 16 lstData)	
			  ak1rutin (nth 17 lstData)
        )
		(setq kapak# lstData)
    )
		
	(setq kulpyerkpkUst "K-")
	(setq isHbase (< ADH ad_UDY))
	; hControl modul durumunu kontrol eder. Eger cekmece varsa limitHbase degerini al�r yoksa isHbase degerini al�r.
	(setq hControl isHbase)
    (if (and ceklist (not (zerop (car ceklist))))
		(progn
			(setq limitHbase (< (- ADH (car ceklist)) ad_UDY))
			(setq hControl limitHbase)
		)
	)
	
	(setq szBodyLayer  (if isHbase "CAB_BODY_BASE" "CAB_BODY_TALL"))
	(setq zTop (if isHbase g_ClearBaseZTop g_ClearWallZTop) zBot g_ClearBaseZBot)
	
    (if (null szKpmLayer)
    	(setq szKpmLayer szBodyLayer)
    )

    (if (null sBazaType)
        (if (minusp ADEN)
            (setq ADEN (abs ADEN) sBazaType "yok")
            (setq sBazaType "duz")
        )
    )
	(if (equal "nil" LR)
		(setq LR nil)
	)
	(setq coverMat (GetBaseUnitsDoorMaterial))
    (cond ((null ADDer)    (setq ADDer  ad_ADDer  ADDer2 ad_ADDer))
          ((isList ADDer)   (setq ADDer2 (cadr ADDer)  ADDer (car ADDer)))
          (T               (setq ADDer2 ADDer))
    )
    (if (null ADDer) (setq ADDer ad_ADDer))
    (if (null ADDer2) (setq ADDer2 ad_ADDer))
	
	;kap_pay null ise derinli�in 4 cm �zerinde olsun
	(if (null kap_PAY) (setq kap_PAY (+ (convertLength 4.0 5) ADDer)))

	(setq unitH ADH unitW ADEN unitD  ADDer unitD2 ADDer2 dAltKot ad_bazaH)

	(if (and ceklist
             (not (zerop (car ceklist)))
        )	
		(if (> (car ceklist) (convertLength 25.0 5))
			(setq kulpyerkpkUst "GD")
		)
	)
	
	(setq kulpyer (if (equal noHandle "1") "0-" (if (or hControl (equal kulpyerkpkUst "GD")) "KD" "D-")))
	(setq kulpyerkpkUst (if (equal noHandle "1") "0-"  kulpyerkpkUst))
    ;(if (equal (Xstr "Left") LR) (setq ADEN1 brutEN ADEN2 kap_PAY))
    ;(if (equal (Xstr "Right") LR) (setq ADEN1 kap_PAY ADEN2 brutEN))

    (setq ADEN1 brutEN ADEN2 kap_PAY)   ;soL varsay�yoruz
    (setq szMData (convertStr
        (list kodFormula 'AK1
            ADEN
            (if (not (equal ADH ad_ADH)) ADH)   ;ADH
            (if (equal ADDer ADDer2)
                (if (not (equal ADDer ad_ADDer)) ADDer)  ;ADDer
                (cons 'QUOTE
                    (list (list
                        (if (not (equal ADDer ad_ADDer)) ADDer)  ;ADDer
                        (if (not (equal ADDer2 ad_ADDer)) ADDer2)  ;ADDer2
            ))))
            gor_GEN
            brutEN
            kap_PAY
            (if ceklist (cons 'QUOTE (list ceklist)))
            (if raflist (cons 'QUOTE (list raflist)))
            (cons 'QUOTE (list (setq tempparamlist (list kapak# SubEs sBazaType szKpmLayer LR blindDoorTouchWall  specialDoor unitCArea isManualCArea drawerRecipe bodyAuxRcpsList bodyScriptFiles leftDwgParamsList doorTopTrim doorAnimateScript rightDwgParamsList noHandle rutin))))
            kapak_tip
            imalTipi note
        )
    ))
    (cond (mozel_mUnit
               (setq yp1 mozel_yp1
                   aci mozel_aci
                   LR           (get_Mdata mozel_mUnit k:CModulDirection)
                   notch_type   (get_Mdata mozel_mUnit k:NotchType)
                   notch_dims   (get_Mdata mozel_mUnit k:NotchDims)
               )
               (if (member LR '("R" "saG"))
                   (setq LR "saG")
                   (setq LR "soL")
               )
               (entdel mozel_mUnit)
          )
          (notch_mUnit
               (setq yp1        (dfetch 10 notch_mUnit)
                    aci         (dfetch 50 notch_mUnit)
                    LR          (get_Mdata notch_mUnit k:CModulDirection)
                    notch_type  (get_Mdata notch_mUnit k:NotchType)
                    notch_dims  (get_Mdata notch_mUnit k:NotchDims)
               )
               (if (member LR '("R" "saG"))
                   (setq LR "saG")
                   (setq LR "soL")
               )
               (if (equal LR "soL")
                   (if (notchDlg (polar yp1 aci (- ADEN1 ADEN)) aci ADEN ADH ADDer)
                      (entdel notch_mUnit)
                      (exit)
                   )
                   (if (notchDlg (polar yp1 (- aci pi/2) ADEN1) (+ aci pi/2) ADEN ADH ADDer)
                      (entdel notch_mUnit)
                      (exit)
                   )
               )
          )
          (T
              (get_inputs "AK1" (list ADEN1 ADEN2) ADH (list ADDer ADDer2))   ;get_inputs sets LR			
			  (cond ((member LR '("r" "R")) (setq LR "saG"))
				    ((member LR '("l" "L")) (setq LR "soL"))
			  )
			  ;only controling for angle enough to understand get inputs correct well or not.
			  (if (null aci) (exit))
              (if (> g_iNotchMode 0)
                  (if (equal LR "soL")
                      (checkInterference (polar yp1 aci (- ADEN1 ADEN)) aci ADEN ADH ADDer)
                      (checkInterference (polar yp1 (- aci pi/2) ADEN1) (+ aci pi/2) ADEN ADH ADDer)
                  )
              )
          )
    )

    (if (equal "saG" LR)
		(setq ADEN1 kap_PAY ADEN2 brutEN)
	)

    (if (equal "soL" LR)
        (akdL_motor)
        (akdR_motor)
    )
	(cond
		((and (equal "CAB_BODY_WALL" szBodyLayer) GetWallUnitsDoorMaterial)
			(setq tempDoorMat (GetWallUnitsDoorMaterial))
		)
		((and (equal "CAB_BODY_BASE" szBodyLayer) GetBaseUnitsDoorMaterial)
			(setq tempDoorMat (GetBaseUnitsDoorMaterial))
		)
		((and (equal "CAB_BODY_TALL" szBodyLayer) GetTallUnitsDoorMaterial)
			(setq tempDoorMat (GetTallUnitsDoorMaterial))
		)
		(T
			(setq tempDoorMat (GetBaseUnitsDoorMaterial))
		)
	)

	(set_block_material (tblobjname "BLOCK" (dfetch 2 (entlast))) T "CAB_DOORS" tempDoorMat)
	(multiEvaluate ak1rutin)
	(princ)	
)

;only kulp uses this
;modes : nil -> change handle 
;		: choose -> only return name of handle
(defun nesne_yer (nesnetip nesnenot mode / QmLofL mLofL nesnepath mLofGrups ret resultName)
    (setq QmLofL (read (strcat nesnetip ":mLofL"))
          mLofL (eval QmLofL)
    )
	
    (if (setq defFile (findfile (strcat ad_DefPath nesnetip "\\" nesnetip ".DEF")))
		(setq nesnepath (strcat ad_DefPath nesnetip "\\" nesnetip))
		(setq nesnepath (strcat ad_AdekoPath nesnetip "\\" nesnetip)
			  defFile (strcat nesnepath (if ad_XTable ".ENG" "")  ".def")
		)
	)

    (if (null mLofL)
        (progn
            (set_mLofL QmLofL nil defFile)
            (setq mLofL (eval QmLofL))
        )
    )
    (setq mLofGrups
        (list
          (mapcar
              '(lambda (modul)
                  (list (car modul) (last modul))
              )
              mLofL
          )
      )
    )
    (if (setq ret (modulList (GetKulpModel) (list (list nesnepath nesnenot)) mLofGrups))  ;T means zis is kulp
		(progn	
		(if (equal nesnetip "kulp") (setq ret (replace 1 1 ret))) ; �zel kulp atmaya zorla
		(setq  resultName (car (nth (caddr ret) mLofL)))
        ;0-> t�m�n� de�i�tereb fonksiyonu �al��t�r
		;1-> �zel se�imi �al��t�r	
		;2-> default kulbu de�i�tirir
			
			(cond ((equal mode "choose")
					(princ)
				)
				((equal (cadr ret) 0)
				(ins2Dwg (nth (caddr ret) mLofL))
				)
				((equal (cadr ret) 1)
				;(changeKulpAsSpecial (car (nth (caddr ret) mLofL)) (caddr  (nth (caddr ret) mLofL)))
				(changeKulpAsSpecial (car (nth (caddr ret) mLofL)) )
				)
				((equal (cadr ret) 2)					
					(SetKulpModel (car (nth (caddr ret) mLofL)))
				)				
			)
		)
    )
	resultName	
)

(defun SPOTD_SETTER (code miktar note / curunit unitEnt unitPos)
                ;dwgParams
                ;;(list (list dwgPath (list offsetX offsetY OffsetZ) "(animateScript)" (list strectRatioX strectRatioY strectRatioZ) usageState quantity quantityunit  dwgLayer  ))
                (setq DWG_FROM_OUT_SPOT (list  code (list "{(* unitW 0.5)}" "{(* unitD 0.5)}" 0.0 ) "" (list 1.0 1.0 1.0) 1 miktar "adet" ""))
                (princ "\n")
                (setq curunit (entsel))
                (setq unitEnt (car curunit))
				(if (get_mdata unitEnt k:moduleDwgList) 
				(progn
					(setq DWG_FROM_OUT_CURRENT (car (read (get_mdata unitEnt k:moduleDwgList))))
					(setq DWG_FROM_OUT_CURRENT (remove 0 DWG_FROM_OUT_CURRENT))
					(setq DWG_FROM_OUT_CURRENT (remove nil DWG_FROM_OUT_CURRENT))
				)
				)
                (setq unitPos (car (cdr curunit)))
                (redrawUnit unitEnt (list nil nil nil))
                (setq DWG_FROM_OUT_SPOT nil DWG_FROM_OUT_CURRENT nil)
                (setq curunit nil unitEnt nil unitPos nil mozel_mUnit nil mozel_yp1 nil mozel_aci nil unitMdata nil)
)
(defun SPOTU_SETTER (code miktar note / curunit unitEnt unitPos)
                ;dwgParams
                ;;(list (list dwgPath (list offsetX offsetY OffsetZ) "(animateScript)" (list strectRatioX strectRatioY strectRatioZ) usageState quantity quantityunit  dwgLayer  ))
                (setq DWG_FROM_OUT_SPOT (list  code (list "{(* unitW 0.5)}" "{(* unitD 0.5)}" "{(- unitH ad_et)}") "" (list 1.0 1.0 1.0) 1 miktar "adet" ""))
                (princ "\n")
                (setq curunit (entsel))
                (setq unitEnt (car curunit))
				(if (get_mdata unitEnt k:moduleDwgList) 
				(progn
					(setq DWG_FROM_OUT_CURRENT (car (read (get_mdata unitEnt k:moduleDwgList))))
					(setq DWG_FROM_OUT_CURRENT (remove 0 DWG_FROM_OUT_CURRENT))
					(setq DWG_FROM_OUT_CURRENT (remove nil DWG_FROM_OUT_CURRENT))

				)
				)
                (setq unitPos (car (cdr curunit)))
                (redrawUnit unitEnt (list nil nil nil))
                (setq DWG_FROM_OUT_SPOT nil DWG_FROM_OUT_CURRENT nil)
                (setq curunit nil unitEnt nil unitPos nil mozel_mUnit nil mozel_yp1 nil mozel_aci nil unitMdata nil)
)

(defun c:om ()
	(alert (xstr "Your library is not yet suitable for automatic kitchens."))
)

(if (null c:baza_orj) (setq c:baza_orj c:baza))
(defun c:baza ()
	(modul_I "Baza")
)

(if (null c:kert_orj) (setq c:kert_orj c:kert))
(defun c:kert () (princ "\nNot allowed.") (princ))

(defun Soprano_tanim ( / curCode lstss  ssSel selectedEntity usageType)
	(if (and mozel_mUnit (equal (type mozel_mUnit) 'ENAME) (setq usageType (get_mdata mozel_mUnit k:WPanelUsageType)) (not (checkStrInStr "independent" usageType)))
		(progn
			(princ) ;Buraya raydolaptan gelen par�alar giriyor.
			(mode_tile "kod" 1)
			(mode_tile "aciklama" 1)
			(mode_tile "lmm_openAuxRcpsDialogButton" 1)
			(mode_tile "ekle" 1)			
			(mode_tile "extras" 1)
			(mode_tile "tip" 1)
			(mode_tile "subtip" 1)
			(mode_tile "showFormulCode" 1)
			(mode_tile "model" 1)
		)
		(progn
			(if (null doNotCloseBaseComp) ; bu parametre WHDChanger_tanim da setleniyor
				(progn
					(mode_tile "yuk" 1)
					(mode_tile "gen" 1)
					(mode_tile "der" 1)
				)
			)
			(mode_tile "der2" 1)
			(mode_tile "kod" 1)
			(mode_tile "aciklama" 1)
			(mode_tile "changedDesignMode" 1)
			(mode_tile "displayModeButton" 1)
			(mode_tile "lmm_openAuxRcpsDialogButton" 1)
			(mode_tile "ekle" 1)
			(mode_tile "showFormulCode" 1)
			(mode_tile "tip" 1)
			(mode_tile "subtip" 1)
			(mode_tile "90" 1)
			(mode_tile "135" 1)
			(mode_tile "F" 1)
			(mode_tile "changedoorwid" 1)
			(mode_tile "doortoptrim" 1)
			(mode_tile "nil" 1)
			(mode_tile "A" 1)
			(mode_tile "A2" 1)
			(mode_tile "B" 1)
			(mode_tile "B2" 1)
			(mode_tile "isCustomBody" 1)
			(mode_tile "nokulp" 1)
			(mode_tile "yok" 1)
			(mode_tile "duz" 1)
			(mode_tile "ayak" 1)
			(mode_tile "dwgAddButton" 1)
			(mode_tile "specialDoorToggle" 1)
			(mode_tile "doorsAnmCommand" 1)
			(mode_tile "raf" 1)
			(mode_tile "body" 1)
			(mode_tile "door" 1)
			(mode_tile "short" 1)
			(mode_tile "long" 1)
			(mode_tile "customBodyAddButton" 1)
			(mode_tile "extras" 1)
			(mode_tile "doorbottomtrim" 1)
			(mode_tile "shelfGlass" 1)
			(mode_tile "0" 1)
			(mode_tile "1" 1)
			(mode_tile "2" 1)
			(mode_tile "cek" 1)
			(mode_tile "drawerRecipeTile" 1)
			(mode_tile "leftDwgAddButton" 1)
			(mode_tile "rightDwgAddButton" 1)
			(mode_tile "G" 1)

			(mode_tile "gen1" 1)
			(mode_tile "gen2" 1)
			(mode_tile "Ggen" 1)
			(mode_tile "doorwid" 1)
			(mode_tile "olc" 1)
			(mode_tile "hozel" 1)
			(mode_tile "dozel" 1)

			(cond

;SOF ismi de�i�ebilir serbest �r�n
				(
				(equal (get_tile "aciklama") "SOF")
				(mode_tile "kod" 0)
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 0)
				)

;tefri� i�inde aynalar�n �l��lendirilmesi serbest kals�n. 
				(
				(equal (substr (get_tile "kod") 1 4) "AYNA")
				(mode_tile "yuk" 0)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

;serbest panel mod�lleri
				(
				(equal (substr (get_tile "kod") 1 6) "RAF36")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (substr (get_tile "kod") 1 7) "SPNL")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 7) "SPAN")
				(mode_tile "yuk" 0)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (substr (get_tile "kod") 1 9) "SDVL18")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)
				
				(
				(equal (substr (get_tile "kod") 1 11) "YNK0018A")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 11) "TYP0015")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)
				
				(
				(equal (substr (get_tile "kod") 1 11) "TYP0012")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)
				
								(
				(equal (substr (get_tile "kod") 1 11) "TYP0010")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 11) "YNK0018S")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 11) "YNK0018F")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

;serbest dolgu bantlar�
				(
				(equal (substr (get_tile "kod") 1 7) "SYDB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 7) "SADB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 7) "SBDB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 7) "STDB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 7) "SUDB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

;serbest dolgu bantlar� k��eler
				(
				(equal (substr (get_tile "kod") 1 8) "SAKDB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 8) "SBKDB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 8) "STKDB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 8) "SUKDB")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

;baba yanak ada d�n�� - derinlik serbest
				(
				(equal (substr (get_tile "kod") 1 6) "DBPCSW")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

;baba yanak ada d�n�� - derinlik serbest
				(
				(equal (substr (get_tile "kod") 1 6) "DBYSW")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

;baba yanak ada d�n�� - derinlik serbest
				(
				(equal (substr (get_tile "kod") 1 6) "DBPCGR")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)

;baba yanak ada d�n�� - derinlik serbest
				(
				(equal (substr (get_tile "kod") 1 6) "DBYGR")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 1)
				)


;gola profiller uzunluk serbest
				(
				(equal (substr (get_tile "kod") 1 6) "GDLPL")
				(mode_tile "yuk" 0)
				(mode_tile "der" 1)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 6) "GDLPR")
				(mode_tile "yuk" 0)
				(mode_tile "der" 1)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 5) "GYAP")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (substr (get_tile "kod") 1 5) "GYUP")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (substr (get_tile "kod") 1 6) "GDCP")
				(mode_tile "yuk" 0)
				(mode_tile "der" 1)
				(mode_tile "gen" 1)
				)

				(
				(equal (substr (get_tile "kod") 1 8) "DBYLEDHR")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (substr (get_tile "kod") 1 6) "GLED4K")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)
				
				(
				(equal (substr (get_tile "kod") 1 6) "GLED6K")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (substr (get_tile "kod") 1 6) "CLA")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (substr (get_tile "kod") 1 6) "PCO")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (substr (get_tile "kod") 1 6) "PEO")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)


; RAF PANO YANAK i�in en boy derinlik serbest
				(
				(equal (get_tile "kod") "YNK")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 0)
				)

				(
				(equal (get_tile "kod") "PN")
				(mode_tile "yuk" 0)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)

				(
				(equal (get_tile "kod") "RF")
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				(mode_tile "gen" 0)
				)
				
								(
				(equal (substr (get_tile "kod") 1 3) "RDR")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)


								(
				(equal (substr (get_tile "kod") 1 4) "MVTA")
				(mode_tile "yuk" 1)
				(mode_tile "der" 1)
				(mode_tile "gen" 0)
				)


								(
				(equal (substr (get_tile "kod") 1 3) "HRC")
				(mode_tile "yuk" 1)
				(mode_tile "der" 0)
				(mode_tile "gen" 0)
				)
;				(
;				(equal (get_tile "kod") "KPK")
;				(mode_tile "yuk" 0)
;				(mode_tile "der" 1)
;				(mode_tile "gen" 0)
;				)

;alt k�r k��e ayr�m�
				((equal (get_tile "tip") "2") (mode_tile "gen2" 0))

;�st k�r k��e ayr�m�
				((equal (get_tile "tip") "5") (mode_tile "gen2" 0))

;eski tezgah yap�s� i�in gerekli de�i�kenler.

				(
				(equal (get_tile "kod") "TZG0030G")
				(mode_tile "gen" 0)
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				)
				
				(
				(equal (get_tile "kod") "TUS60129")
				(mode_tile "gen" 1)
				(mode_tile "yuk" 0)
				(mode_tile "der" 1)
				)
				
				(
				(equal (get_tile "kod") "TUS60147")
				(mode_tile "gen" 1)
				(mode_tile "yuk" 0)
				(mode_tile "der" 1)
				)
				
				(
				(equal (get_tile "kod") "TZG0030")
				(mode_tile "gen" 0)
				(mode_tile "yuk" 0)
				(mode_tile "der" 0)
				)
				
				(
				(equal (substr (get_tile "kod") 1 3) "TZG")
				(mode_tile "gen" 0)
				(mode_tile "yuk" 1)
				(mode_tile "der" 0)
				)

			)
		)
	)
)

(defun parseLast$ (string / without$ withoutLast index returnString)
	(if (equal (length (setq without$ (splitstr string "$"))) 1)
		(setq returnString (parse$ string))
		(progn
			(setq withoutLast (removeat (- (length without$) 1) without$))
			
			(setq returnString "" index 0)	
			(repeat (length withoutLast)
				(setq returnString (strcat returnString (getnth index withoutLast)))
				(if (not (equal (- (length withoutLast) 1) index)) (setq returnString (strcat returnString "$")))
				(setq index (+ index 1))
			)
		)
	)
	returnString
)

(defun ins2Dwg (data / xstrdata ins2Dwg::userDefinedKlapaHeight)
	(firmSpesificCkecks)
	(setvar "regenmode" 0)
	(setq xstrdata (replace (lmm_sfRead (xstr (last data))) (- (length data) 1) data))
	
	(getKlapaHeight) ; klapa y�kseklik tercihi olan modullerde, yerle�tirilmeden �nce sorulmal�d�r.(ins2Dwg::userDefinedKlapaHeight)
	(eval (append (list (cadr xstrdata) (car xstrdata)) (cddr xstrdata)))
	(setKlapaHeight) ;modul olu�turulduktan sonra, xdatas�na, klapa y�kseklik bilginin g�m�lmesi gerek

	(setvar "regenmode" 1)
	(setHandleModelsAssociationModuleType)
	(runLastRutinOfModules)
)

(defun runLastRutinOfModules ( / )
	(if (checkStrInStr "rutin_Spot" (ifnull (get_mdata (entlast) k:mdata) ""))
		(insertSpotInModuleBlock)
	)
	(if (checkStrInStr "rutin_LedLight" (ifnull (get_mdata (entlast) k:mdata) ""))
		(insertLedlightInModuleBlock)
	)

	(if (member (parseLast$ (ifnull (dfetch 2 (entlast)) "")) g_SopranoRenkSecTable) (rutin_RenkSec))
)

(defun firmSpesificCkecks ( / )
	(if (member (car data) pistonModulesList)
		(getDoorCombinationInfo)
	)
)

(defun setHandleModelsAssociationModuleType ( / tempEnt tempModulLayer tempKulpModel kapakEntList kapakEnt)
	(setq tempEnt (entlast))
	(setq tempModulLayer (dfetch 8 tempEnt))
	
	(cond 
		((equal tempModulLayer "CAB_BODY_WALL")
			(setq tempKulpModel (if (not (member (getXref "AD_KULP_COB_BODY_WALL$$") (list nil ""))) (getXref "AD_KULP_COB_BODY_WALL$$") nil))
		)
		((equal tempModulLayer "CAB_BODY_BASE")
			(setq tempKulpModel (if (not (member (getXref "AD_KULP_COB_BODY_BASE$$") (list nil ""))) (getXref "AD_KULP_COB_BODY_BASE$$") nil))
		)
		((equal tempModulLayer "CAB_BODY_TALL")
			(setq tempKulpModel (if (not (member (getXref "AD_KULP_COB_BODY_TALL$$") (list nil ""))) (getXref "AD_KULP_COB_BODY_TALL$$") nil))
		)
		(T
			(setq tempKulpModel nil)
		)
	)
	(if tempKulpModel
		(progn
			(setq kapakEntList (listDoorEnamesIn tempEnt "." nil))
			(foreach kapakEnt kapakEntList
				(changeKanatsHandle kapakEnt tempKulpModel)
				(command "._REGEN")
			)
		)
	)
)

(defun getDoorCombinationInfo ( / combType actionStatus resultList doorIndexList)
	(setq combType nil)
	(if (not (new_dialog "selectDoorTypeDiolog" (load_dialog (strcat ad_defpath "firmfaces.dcl")))) (exit))
	(set_tile "comb1" "1")
	(action_tile "accept" "(cond ((equal (get_tile \"comb1\") \"1\") (setq combType 1)) ((equal (get_tile \"comb2\") \"1\") (setq combType 2)) ((equal (get_tile \"comb3\") \"1\") (setq combType 3))) (done_dialog 1)")
	(setq actionStatus (start_dialog))
	(if (equal actionStatus 1)
		(if combType
			(progn
				(setq resultList 
					(cond 
						((equal combType 1)
							(list "S" "S")
						)
						((equal combType 2)
							(list "T" "T")
						)
						((equal combType 3)
							(list "S" "T")
						)
						(T
							(list "S" "S")
						)
					)
				)
				(setq doorIndexList (findFirsTwoDoorIndex))
				(setq data (replace (replace (replace (replace (car resultList) 4 (getnth (car doorIndexList) (getnth 1 (getnth 6 data)))) (car doorIndexList) (getnth 1 (getnth 6 data))) 1 (getnth 6 data)) 6 data))
				(setq data (replace (replace (replace (replace (cadr resultList) 4 (getnth (cadr doorIndexList) (getnth 1 (getnth 6 data)))) (cadr doorIndexList) (getnth 1 (getnth 6 data))) 1 (getnth 6 data)) 6 data))
			)	
			(exit)
		)
	)
)

(defun findFirsTwoDoorIndex ( / divFirst divSecond)
	(setq divFirst nil divSecond nil index 0)
	(repeat (length (getnth 1 (getnth 6 data)))
		
		(if (equal (car (getnth index (getnth 1 (getnth 6 data)))) "CT")
			(if divFirst (if (not divSecond) (setq divSecond index)) (setq divFirst index))
		)
		(setq index (+ index 1))
	)
	(list divFirst divSecond)
)

(defun f:initoverrides ()
	(Soprano_tanim)
)

; (defun menuPanelUyar ()
; 	(alert "Bu �zelliklerin bu k�t�phane ile kullan�lmas� uygun de�ildir.")
; 	(princ)
; )

; (defun c:yanak ()
	; (menuPanelUyar)
; )

; (defun c:raf ()
	; (menuPanelUyar)
; )

; (defun c:pano ()
	; (menuPanelUyar)
; )

; (defun c:filler ()
; 	(menuPanelUyar)
; )
; 
; (defun c:FILLER ()
; 	(menuPanelUyar)
; )

 (defun c:kapama ()
 	(menuPanelUyar)
 )

(defun yanak (kod dWid dH dDep lstParam imalTipi szNote / options fullEdgeSet rutin yP yH yDer yaci fH2P szSubTyp szMetraj szMData div# sirt_L side_postprocess yp1difference orientAngle panelData yanTck panelUsageType panelUsageSubTypes bodyAuxRcpsList tempPointList tempBulgeData temperEnt tempAppList) 
    (setq fH2P 		(nth 0 lstParam)
          szSubTyp 	(nth 1 lstParam)
          szMetraj 	(nth 2 lstParam)
		  options  	(nth 3 lstParam)
		  rutin		(nth 4 lstParam)
		  bodyAuxRcpsList (nth 5 lstParam)
		  panelUsageType (ifnull (nth 6 lstParam) "independent_side")
		  panelUsageSubTypes (nth 7 lstParam)
          yanTck dWid
          isKapak (equal "door" szSubTyp)
		  panelData (nth 8 lstParam)
		  orientAngle (ifnull (nth 9 lstParam) 0.0) 
		  
    )	
	(if (and options 
			 (eval (car options))
		)
		(progn
			(setq temp (eval options))
			(if (null temp) (exit))
		)
	)		

	(setq kod (parse$ kod))
	
    (if (equal (type mozel_mUnit) 'ENAME)
        (progn	 
				
				(setq yp1difference (list 0.0 0.0 0.0)  )
					
					;(if (minusp (getnth 0 (decideBlockMoveVector mozel_mUnit 1)))
					;	(list 0.0 0.0 0.0)  
					;	(progn								
					;		(polar (list 0 0 0) (if (<ang< (* -1 DEG_90) mozel_aci DEG_90) mozel_aci (- mozel_aci DEG_180)) (getnth 0 (decideMoveVector mozel_mUnit (- yanTck (ifnull (get_Mdata mozel_mUnit k:panelThickToProfiles) (get_Mdata mozel_mUnit k:WID )))) ) )							
					;	)
					;)
				
				 
              (setq yP (mapcar '+ mozel_yp1 yp1difference)
                    yaci mozel_aci
                    yDer dDep
              )
			  
			  (cleanDrawerPanelConnectors (dfetch 2 mozel_mUnit))
			  
			  (setq tempAppList (getnth 0 (listtrans (cdr (assoc -3 (entget mozel_mUnit (list "*")))))))			  
              (entdel mozel_mUnit)
        )
        (yanak_inputs) ;return yP yH yDer yaci
    )
	
    (if (null dH) (setq dH yH))
    (if (null dDep) (setq dDep yDer))

    (setq szMData (convertStr
        (list kod 'YANAK
            dWid
            dH
            dDep
            (cons 'QUOTE (list (list fH2P szSubTyp szMetraj options rutin bodyAuxRcpsList panelUsageType panelUsageSubTypes panelData orientAngle)))
            imalTipi
            szNote
        )
    ))
	
    (setq blok_list (entlast))
    (yanak_noktalari)
	
    (if isKapak
        (progn
            (setq ents (collectMents blok_list))
            (if isSirt
                (progn
                    (setq yP (polar yP (- yaci pi/2) sirt_L))
                    (command "_.MOVE" ents "" '(0 0 0) yP)
                    (command "_.ROTATE" ents "" yP (r2d (+ yaci pi/2)))

					(setq i 0)
                    (repeat (sslength ents)
                    	(blok_tanim kod '(0 0 0) (ssname ents i) 0)
			            (set_Mdata (entlast)
							(side_xdata_list (* dH (/ sirt_L div#)) dDep)
			            )
                    	(i++)
                    )
                )
                (progn
                    (command "_.MOVE" ents "" '(0 0 0) yP)
                    (command "_.ROTATE" ents "" yP (r2d yaci))					
            		(blok_tanimFromEnt kod '(0 0 0) blok_list 0.0)					
					(set_Mdata (entlast)
						(side_xdata_list (* dH dDep) dDep)
					)
                )
            )

        )
        (progn
            (setlayer "PANELS_SIDE" 40)					
			(if panelData
				(progn					
					(setq tempPointList (car (setq onlyPolylineData (lmm_convertPolylineDataToPointListBulgeList (car (cdr panelData))))))
					(setq tempBulgeData (car (cdr onlyPolylineData)))		
					(setq temperEnt (createGPanel tempPointList tempBulgeData dWid orientAngle yaci yP panelUsageType panelUsageSubTypes nil "SIDE" (if (equal kod "") nil kod)))				
				)
				(progn
					(blok_tanimFromEnt kod yP blok_list yaci)
					(setq temperEnt (entlast))
				)
			)						
             (set_Mdata temperEnt
                 (append
					 (side_xdata_list (* dH (if isSirt dWid dDep)) (if isSirt dDep dH))
					 (transprof (list '(0.0 0.0 0.0) (list 0.0 (- yDer) 0.0)  (list yanTck (- yDer) 0.0)  (list yanTck 0.0 0.0)) yP yaci)
                 )
             )
			(lmm_setLabelApps temperEnt tempAppList)
			
        )
    )
	(if rutin (eval rutin))
    (if side_postprocess
        (side_postprocess)
    )
	(if (equal (get_mdata (entlast) '(620 . 1000)) "independent_side")
		(progn
			(set_block_material (entlast) T (dfetch 8 (entlast)) (getKapakMat))
			(command "._REGEN")
		)
	)
)

(defun raf (kod dWid dShelfTck dDep lstParam lstLevel imalTipi szNote
            /   1mesafept 2mesafept side rkot yp1 aci szMData mdata blok_list ptlst
                fH2P szSubTyp szMetraj temp raf# dH SubEs ozel-er bodyAuxRcpsList panelUsageType
				options  ;options : extra procedures eg: (somefunc data)
				shelf_postprocess panelData orientAngle extraMdataList generalMdata temperpanel tempAppList
           )
	(init_it)
	
	(defun ozel-er (s)
		(EchoUnexpectedErr s)
		(facesel_exit)
		(facesel_exit)
		(end_it)
		(redraw)
		(prin1)
    )
	
	(setErrorFunc ozel-er)
	(facesel_init)
	
	(if (equal dWid 2.0) (setq dWid 2.00001))
	(if (equal dDep 2.0) (setq dDep 2.00001))
	(if (equal dShelfTck 2.0) (setq dShelfTck 2.00001))
	
    (setq fH2P      (getnth 0 lstParam)
          szSubTyp  (getnth 1 lstParam)
          szMetraj  (getnth 2 lstParam)
		  options   (getnth 3 lstParam)		
		  SubEs		(getnth 4 lstParam)
		  bodyAuxRcpsList  (getnth 5 lstParam)
		  panelUsageType (getnth 6 lstParam)
		  panelUsageSubTypes (getnth 7 lstParam)
		  panelData (getnth 8 lstParam)
		  orientAngle (ifnull (getnth 9 lstParam) 0.0) 
		  
          raf#      (car lstLevel)
          dH   		(cadr lstLevel)
		  
    )
	
	(if (and options 
			 (eval (car options))
		)
		(progn
			(setq temp (eval options))
			(if (null temp) (exit))
		)
	)		

    (if (equal (type mozel_mUnit) 'ENAME)
        (progn
              (setq yp1 mozel_yp1
                    aci mozel_aci
              )
			  (cleanDrawerPanelConnectors (dfetch 2 mozel_mUnit))
			  (setq tempAppList (getnth 0 (listtrans (cdr (assoc -3 (entget mozel_mUnit (list "*")))))))
              (entdel mozel_mUnit)
        )
        (progn
            (raf_inputs)
			(if (equal fH2P 0) ; nokta gir
				(setq yp1 (xyofz yp1 (- rkot dH)))
				(setq yp1 (xyofz 1mesafept (- rkot dH)))				
			)
            ; (setq ;dH      ad_ET
                  ; yp1     (xyofz 1mesafept (- rkot dH))
            ; )
        )
    )

    (setq szMData (convertStr
        (list kod 'RAF dWid dShelfTck dDep
              (cons 'QUOTE (list (list fH2P szSubTyp szMetraj nil SubEs bodyAuxRcpsList panelUsageType panelUsageSubTypes panelData orientAngle)))
              nil
              imalTipi
              szNote)
    ))

    (setq blok_list (entlast))

	;comment nedeni: raydolaptaki panelleri standartla�t�rmak ad�na
   ; (setlayer "SHELVES" 6)
   ; (box_ciz '(0 0 0) dWid (- dDep) dShelfTck)


   ;(drawWardobeMainPanel dWid dDep dShelfTck 0.0 0.0 0.0 (list 0.0 (- 0.0 dDep) 0) "SHELVES")
	
	(if (null (gpanels_controlPanelData panelData)	)
		(progn					
			(setq panelData (list dShelfTck (lmm_convertHeiWidToPolyLineData dWid dDep )))
		)
	)
    (setq ptlst (list '(0 0 0) (list 0 (- dDep) 0) (list dWid (- dDep) 0) (list dWid 0 0)))
	
	(setq generalMdata 
		(append
			(list
			(if (and SubEs (not (equal SubEs ""))) 	(cons k:subE SubEs))
			(cons k:billUnit (cdr (assoc szMetraj '(("number" . 0) ("m2" . 1)))))
			; (cons k:CMtip (if (and imalTipi (not (equal "" imalTipi))) imalTipi "RAF_SHELF"))
	
			(cons k:CMtip imalTipi)
			(cons k:CAuxRcps (convertStr bodyAuxRcpsList))
			(cons k:WPanelUsageType (ifnull panelUsageType "independent_shelf"))
			(if tempfullEdgeSet (cons k:edgeStripsList (convertStr tempfullEdgeSet)))
			(cons k:mData szMData)
			(if panelUsageSubTypes (cons k:WPanelUsageSubTypes panelUsageSubTypes  ))
			)	
		)
	)	

	(if panelData
		(progn
			
			(if (null (gpanels_controlPanelData panelData))
				(progn					
					(setq panelData (list dShelfTck (lmm_convertHeiWidToPolyLineData dWid dDep )))
				)
			)
			(setq tempPointList (car (setq onlyPolylineData (lmm_convertPolylineDataToPointListBulgeList (car (cdr panelData))))))
			(setq tempBulgeData (car (cdr onlyPolylineData)))
			(setq temperpanel (createGPanel tempPointList tempBulgeData dShelfTck orientAngle aci yp1 (ifnull panelUsageType "independent_shelf") panelUsageSubTypes nil "SHELF" (if (equal kod "") nil kod)))
			;(setq ptlst tempPointList)
		)
		(progn
			(setlayer "SHELVES" 6)
			(blok_tanimFromEnt kod yp1 blok_list aci)		
			(setq temperpanel (entlast))
			(setq generalMdata (append  generalMdata  
					(list
						(cons k:MTYP "URAF")
						(cons k:WID dWid)
						(cons k:HEI dShelfTck)
						(cons k:DEP dDep)					
						(cons k:mData szMData)						
						(cons k:Cen dWid)
						(cons k:CBoy dShelfTck)
						(cons k:CDer dDep)
						(cons k:Length dWid)					
						(cons k:Dists (strcat "(" (rtos dWid 2 1) ")"))
						
					)			
				)
			)
		)
	)


	
    (set_Mdata temperpanel (append generalMdata
        (transprof ptlst yp1 aci)
		)
    )
	
	(lmm_setLabelApps temperpanel tempAppList)
	
	(if raf#
		(repeat (1- raf#)
			(COPYd (entlast) (list 0 0 (/ (- dH dShelfTck) (1- raf#))))
		)
	)
	
    (if shelf_postprocess
        (shelf_postprocess)
    )
	(if (equal (get_mdata (entlast) '(620 . 1000)) "independent_shelf")
		(progn
			(set_block_material (entlast) T (dfetch 8 (entlast)) (getKapakMat))
			(command "._REGEN")
		)
	)
	(facesel_exit)
	(end_it)
)

(defun upano (kod UDEN UDH UDDer lstParam imalTipi note /
              fH2P szSubTyp szMetraj dBoy yp1 aci blok_list dummy szMData x z
              pano_L pano_Dists osmod i ptlist alpha delta temp SubEs
			  options ;options : extra procedures eg: (somefunc data)
			  upano_postprocess altZ dDefElev rutin bodyAuxRcpsList panelUsageType
			  orientAngle panelData yanTck panelUsageType panelUsageSubTypes bodyAuxRcpsList tempPointList tempBulgeData temperEnt isConnector 
			  )
	;opencascade bug??
    (if (equal UDEN 2.0) (setq UDEN 2.00001))
	(if (equal UDH 2.0) (setq UDEN 2.00001))
	
	(if (null UDDer) (setq UDDer ad_ET))
	
    (if (isList (caddr lstParam))  ;yeni sistem
		(setq fH2P      (nth 0 lstParam) ;NOT used currently
			  szSubTyp  (nth 1 lstParam)
			  szMetraj  (car (nth 2 lstParam))
			  dBoy		(cadr (nth 2 lstParam))
			  options   (nth 3 lstParam)
			  SubEs		(nth 4 lstParam)
			  rutin		(nth 5 lstParam)
			  bodyAuxRcpsList (nth 6 lstParam)
			  panelUsageType (nth 7 lstParam)
			  panelUsageSubTypes (nth 8 lstParam)
			  panelData (nth 9 lstParam)
			  orientAngle (ifnull (nth 10 lstParam) 0.0) 
			  isConnector (nth 11 lstParam) 
		)
		(setq fH2P      nil				 ;NOT used currently
			  szSubTyp  lstParam
			  szMetraj  "m2"
			  dBoy      (convertLength 300.0 5)
			  bodyAuxRcpsList nil
			  panelUsageType nil
		)
	)
	
	(if (and options 
			 (eval (car options))
		)
		(progn
			(setq temp (eval options))
			(if (null temp) (exit))
		)
	)	

    (cond ((equal (type mozel_mUnit) 'ENAME)
             (setq yp1 mozel_yp1
                   aci mozel_aci
             )
			 (cleanDrawerPanelConnectors (dfetch 2 mozel_mUnit))
             (entdel mozel_mUnit)
          )
          (stretch_mUnit
              (setq yp1 stretch_yp1
                    aci stretch_aci
              )
			  (cleanDrawerPanelConnectors (dfetch 2 stretch_mUnit))
              (entdel stretch_mUnit)
          )
          (T
		  (if UDEN
    	         (get_inputs "US" UDEN UDH UDDer)
			(progn
				(setq dDefElev (+ ad_TezH ad_ADH ad_bazaH))
				(setq altz (getreal (sprintf (Xstr "\nInsertion elevation <~d>: ") dDefElev)))
				(if (null altz) (setq altz dDefElev))
				(setq yp1 (xyofz yp1 altz))
			
			
				(setq osmod (getvar "OSMODE"))
				(setvar "OSMODE" 1) ;ENDPT
				(setq pano_Dists "")
				(getSweepRoute altZ (Xstr "\nWorktop side: ") 'ptlist 'pano_L 'pano_Dists)
				(setvar "OSMODE" osmod)
			)
    	     )
    	  )
    )
    (if pano_Dists
        (progn
            (setq i 0)
            (repeat (1- (length ptlist))
                (setq yp1 (nth i ptlist)
                      aci (angle (nth i ptlist) (nth (1+ i) ptlist))
                      UDEN (distance (xyof (nth i ptlist)) (xyof (nth (1+ i) ptlist)))
                )
                (if (> i 0)
                    (progn
                        (setq alpha (ang3p (xyof (nth (1- i) ptlist)) (xyof (nth i ptlist)) (xyof (nth (1+ i) ptlist))))
                        (cond ((equal alpha pi 0.001) (setq delta 0.0))
                              ((equal alpha pi/2 0.001) (setq delta UDDer))
                              (T (setq delta (+ (/ UDDer (tan alpha)) (/ UDDer (sin alpha)))))
                        )
                        (setq yp1 (polar yp1 aci delta)
                              UDEN (- UDEN delta)
                        )
                    )
                )
                (upano_ciz)
                (i++)
            )
        )
        (upano_ciz)
    )
	(if (equal (get_mdata (entlast) '(620 . 1000)) "independent_panelwall")
		(progn
			(set_block_material (entlast) T (dfetch 8 (entlast)) (getKapakMat))
			(command "._REGEN")
		)
	)
)

;;;;;;;;;;;;;;;;;;;	Adeko 17.6.2 den itibaren SIL SIL SIL SIL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun set_block_material (inBlock namedAs onLayer matName / ss slen i entName entType entLayer entHideFlag)
	(if (equal "INSERT" (dfetch 0 inBlock))
		(setq inBlock (tblobjname "BLOCK" (dfetch 2 inBlock)))
	)
	(setq ss (collectMents inBlock))
	(setq slen (ifnull (sslength ss) 0) i 0)
	(repeat slen
		(setq entName (ssname ss i))
		(setq entType (dfetch 0 entName))
		(if (equal "INSERT" entType)
			(progn
				(set_block_material entName namedAs onLayer matName)
			)
			(progn
				(setq entLayer (dfetch 8 entName) entHideFlag (dfetch 60 entName))
				(if (and (equal entLayer onLayer)
						 (regexMatch (dfetch 2 inBlock) namedAs)
						 (equal entHideFlag 0)
					)
					(grx_setmaterial entName matName)
				)
			)
		)
		(i++)
	)
)
;;;;;;;;;;;;;;;;;;;	Adeko 17.6.2 den itibaren SIL SIL SIL SIL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun yanak_inputs (/ mp1 mp2 1H mpck  dummy inspt)    ; modul
    (setq mpck (_entselRAY (Xstr "\nUnit: ") 1))
    (setq 1mod (car mpck)
          mpck (cadr mpck)
    )

    (setq inspt (dfetch 10 1mod)
          maci (dfetch 50 1mod)
          mp2 (polar inspt maci (get_Mdata 1mod k:WID))   ;(xfetch 1 1mod)
          1H  (+ (get_Mdata 1mod k:HEI) (if (isUstDolap 1mod) 0.0 (caddr inspt)))   ;(xfetch 3 1mod)
          mp1 (if (isKose 1mod)
                  (polar inspt (- maci pi/2) (get_Mdata 1mod k:WID2))       ;(xfetch 2 1mod)
                  inspt)
    )

    (if (< (distance mp1 mpck) (distance mp2 mpck))
        (setq yaci (if (iskose 1mod) (+ maci pi/2) maci)
              yP (if isKapak mp1 (polar mp1 yaci (- yanTck)))
              ori -90.0 ;(if (isKose 1mod) 0.0 -90.0)
              isL T
        )
        (setq yaci maci yP mp2 ori 90.0 isL nil)
    )

    (cond ((or (isAltdolap 1mod) (isBoydolap 1mod))
            (if (equal fH2P 0)
                (while (progn
                          (setq 2mod (_initgetentselRAY 0 "" (Xstr "\n<ENTER=Single unit>/Wall unit: ") 1))
                          (if (equal 2mod RTNONE)
                              (setq 2mod nil)
                          )
                          (and 2mod (not (isUstDolap 2mod)))
                        )
                        ;nothing in the body
                )
            )
            (cond
                ((equal fH2P 1)
                    (setq isSirt T
                          yH (- 1H ad_bazaH)
                          yP (xyofz yP ad_bazaH)
                          ;laynam (if (isBoydolap 1mod) "CAB_BODY_TALL" "CAB_BODY_BASE")
                    )
                    (sirt_ipt)
                )
                (2mod
                    (setq yH (+ (cadddr (assoc 10 (entget (car 2mod))))
                                (get_Mdata 2mod k:HEI))    ;(xfetch 3 2mod)
                          ;laynam "CAB_BODY_TALL"
                    )
                )
                (T  ;Tek dolap
                    (if (or (= "no" (iniread ad_MOD-INI "options" "tamyanak"))
                            isKapak
                        )
                      (setq
                          yH (- 1H ad_bazaH)
                          yP (xyofz yP ad_bazaH)
                      )
                      (setq
                          yH 1H
                          yP (xyofz yP 0.0)
                      )
                    )
                    ;(setq laynam (if (isBoydolap 1mod) "CAB_BODY_TALL" "CAB_BODY_BASE"))
                )
            )
            (if (and (not isSirt)
                     (not (setq yDer (get_Mdata 1mod (if isL k:derL k:derR))))
                )
                (setq yDer (get_Mdata 1mod k:DEP))    ;(xfetch 4 1mod)
            )
          )
          ((isUstdolap 1mod)
            (setq 2mod (_entselRAY (Xstr "\n<ENTER=Single unit>/Base unit or Fridge: ") 1))
            (while (and 2mod (not (or (isAltDolap 2mod) (isbuzdolap 2mod))))
                (setq 2mod (_entselRAY (Xstr "\n<ENTER=Single unit>/Base unit or Fridge: ") 1))
            )
            (if 2mod
                (if (isbuzdolap 2mod)
                    (setq yH (+ (caddr mp1)
                                1H)
                          yDer  (get_Mdata 2mod k:DEP)    ;(xfetch 4 2mod)
                          dummy (xyofz (dfetch 10 2mod) 0.0)
                          yP (inters dummy (polar dummy maci (convertLength 1.0 5))
                                     (setq dummy (xyofz yP 0.0)) (polar dummy (- maci pi/2) (convertLength 1.0 5))
                                     nil
                             )
                          ;laynam "CAB_BODY_TALL"
                    )
                    (setq yH (+ (caddr mp1)
                                1H)
                          yDer  (get_Mdata 2mod k:DEP) ;(xfetch 4 2mod)
                          yP (xyofz yP 0.0)
                          ;laynam "CAB_BODY_TALL"
                    )
                )
                (progn
                    (setq yH 1H) ; laynam "CAB_BODY_WALL"
                    (if (not (setq yDer (get_Mdata 1mod (if isL k:derL k:derR))))
                        (setq yDer (get_Mdata 1mod k:DEP))
                    )
                )
            )
          )
          ((isBoyDolap 1mod)
            (setq
                yH 1H ;(- 1H ad_bazaH)
                yP (xyofz yP ad_bazaH)
                ;laynam "CAB_BODY_TALL"
            )
            (if (not (setq yDer (get_Mdata 1mod (if isL k:derL k:derR))))
                (setq yDer (get_Mdata 1mod k:DEP))
            )
          )
          ((isBuzdolap 1mod)
            (setq 2mod (_entselRAY (Xstr "\n<ENTER=Single unit>/Wall unit: ") 1))
            (while (and 2mod (not (isUstDolap 2mod)))
                (setq 2mod (_entselRAY (Xstr "\n<ENTER=Single unit>/Wall unit: ") 1))
            )
            (if 2mod
                (setq yH (+ (cadddr (assoc 10 (entget (car 2mod))))
                            (get_Mdata 2mod k:HEI))
                      ;laynam "CAB_BODY_TALL"
                )
                (setq yH 1H) ; laynam "CAB_BODY_TALL"
            )
            (setq yDer (get_Mdata 1mod k:DEP))
          )
          ((isCihaz 1mod)
            (setq 2mod (_entselRAY (Xstr "\n<ENTER=Single unit>/Wall unit: ") 1))
            (cond
                (2mod
                    (setq yH (+ (cadddr (assoc 10 (entget (car 2mod))))
                                (get_Mdata 2mod k:HEI))
                          ;laynam "CAB_BODY_TALL"
                ) )
                (T
;                    (if (= "yes" (iniread ad_MOD-INI "options" "tamyanak"))
                    (setq
                        yH (+ 1H (caddr yP))
                        yP (xyofz yP 0.0)
                    )
                    ;(setq laynam "CAB_BODY_BASE")
                )
            )
            (setq yDer (get_Mdata 1mod k:DEP))
          )
		  (T			
			(setq	yH (- 1H ad_bazaH)
					yP (xyofz yP ad_bazaH))
			(setq yDer (ifnull (get_Mdata 1mod k:DEP) 0.0))
		  )		  
          ;(T (getstring "\nElse what?"))
    )	
	(setq yDer (+ yDer (if (equal fH2P 0) (cu2cm 2.0) 0.0))) ;e�er yanak module ba�l� at�lacaksa kapaga kadar gitmesi istendi. (soprano taraf�ndan)
)

(defun set_ortaktiles (tip code grupnam imalTipi note /  )
	
	;her a��ld���nda doldursun imalat listesini dedik ama bu optimik 3'te sorun yarat�yor vazge�tik
	
	(setq codeFormula code)
	(setq code (lmm_sfRead code))
    (set_tile "kod" code)
    (set_TipTile tip)
;    (set_GrupTile grupnam)
	(set_ImalatTile imalTipi)
    (set_tile "aciklama" (xstr note))

    (action_tile "aciklama" "(do_note)")
    (action_tile "kod" "(do_kod)")
    (action_tile "tip" "(done_dialog (+ 4 (atoi $value)))")
;    (action_tile "grup" "(set_tile \"mgrup\" (do_grup (get_tile \"mgrup\")))")
    (action_tile "imal" "(do_imal $value)")
	(mode_tile "lmm_openAuxRcpsDialogButton" 1)
	(if (and (member tip (list 'A1 'U1 'AK1 'AK2 'GM1 'UK1 'UK2 'XX 'AB 'UB 'UE 'UC 'BC 'YANAK 'UPANO 'RAF 'PCONN 'BE ) ) (equal (iniread ad_MOD-INI "manufacturing" "mode" "2") "6"))
		(progn
			(mode_tile "lmm_openAuxRcpsDialogButton" 0) 
			(mode_tile "imal" 1) 
		)
	)
	(action_tile "lmm_openAuxRcpsDialogButton" "(lmm_showAuxRcpsDialog 'newBodyauxRcpsList nil)")	
	(action_tile "showFormulCode" "(showEnterFormulationDialog 'codeFormula 'code \"STR\") (set_tile \"kod\" (lmm_sfRead codeFormula))")
	

    (if (or 
			(null grupnam ) ; if it comes from commands like c:gm1
			mozel_mUnit
			(equal "0" (iniread ad_MOD-INI "options" "addbutton" "1"))
			(equal "0" (iniread ad_MOD-INI "options" "modifyunit"))
		)
		(progn
			
			(action_tile "ekle" "(set_tile \"kod\" codeFormula)(do_kod) (setq QmLofL nil) (if (f:pick_tiles) (add2DefLst (f:mdata)  (askForGrupName) )) (set_tile \"kod\" (lmm_sfRead codeFormula)) ")
		)
       ; (mode_tile "ekle" 1)
		(progn
			;malesef �o�u yere dokunmamak i�in g�sterirken normal, g�nderirken formull� de�eri ata �eklinde bir kurnazl�k yapt�k			
			(action_tile "ekle" "(set_tile \"kod\" codeFormula)(do_kod)  (if (f:pick_tiles) (add2DefLst (f:mdata) ad_DefGrup )) (set_tile \"kod\" (lmm_sfRead codeFormula)) ")
		)
    )


	(if (equal "0" (iniread ad_MOD-INI "options" "modifyunit"))
		(progn
			(mode_tile "ins" 1)
			(set_tile "error" (Xstr "You aren't allowed to modify this unit"))
		)
		(action_tile "ins" "(set_tile \"kod\" codeFormula)(do_kod) (if (and (set_ortaktiles:checkWHDLimits) (set_ortaktiles:checkThicknessOfIndependents)) (if (f:pick_tiles) (done_dialog 50)))")
	)

    (action_tile "cikis" "(done_dialog 0)")
    
    (if (not g_ballowUnitDimEdit)
     	(progn
     		(if (not (member tip '(YANAK RAF UPANO TZG TZK)))
     			(progn
		     		(mode_tile "gen" 1)
		     		(mode_tile "yuk" 1)
		      		(mode_tile "der" 1)
		      	)
		    )
      		(mode_tile "kod" 1)
      		(mode_tile "tip" 1)
      		(mode_tile "imal" 1)
      		(mode_tile "gen1" 1)
      		(mode_tile "gen2" 1)
      		(mode_tile "der2" 1)
      		(mode_tile "olc" 1)
      		(mode_tile "hozel" 1)
      		(mode_tile "dozel" 1)
      	)
    )
)

(defun set_ortaktiles:checkWHDLimits( / kontrolKod kontrolList kod i goOn returnValue limitt)
	(setq returnValue T)
	(if soprano_modulWHDlimit
		(progn
			(setq kod (get_tile "kod"))
			
			(setq i 0 goOn T)
			(while (and (setq kontrolList (getnth i soprano_modulWHDlimit)) goOn)
				(if (equal kod (setq kontrolKod (getnth 0 kontrolList)))
					(progn
						(setq goOn nil)
						
						;limit degerleri kontrol et (GENISLIK)
						(if (and (setq limitt (getnth 1 kontrolList)) (> (atof (get_tile "gen")) limitt))
							(progn
								(setq returnValue nil)
								(set_tile "error" (sprintf (xstr "Maximum width limit is ~f on this module.\t\ Current width is ~s !!!") (list limitt (get_tile "gen"))))
							)
						)
						
						;limit degerleri kontrol et (YUKSEKLIK)
						(if (and (setq limitt (getnth 2 kontrolList)) (> (atof (get_tile "yuk")) limitt))
							(progn
								(setq returnValue nil)
								(set_tile "error" (sprintf (xstr "Maximum height limit is ~f on this module.\t\ Current height is ~s !!!") (list limitt (get_tile "yuk"))))
							)
						)
						
						;limit degerleri kontrol et (DERINLIK)
						(if (and (setq limitt (getnth 3 kontrolList)) (> (atof (get_tile "der")) limitt))
							(progn
								(setq returnValue nil)
								(set_tile "error" (sprintf (xstr "Maximum depth limit is ~f on this module.\t\ Current depth is ~s !!!") (list limitt (get_tile "der"))))
							)
						)
					)
				)
				(i++)
			)
		)
	)
	returnValue
)

(defun set_ortaktiles:checkThicknessOfIndependents ( / kod returnValue checkThick checkValue)
	(setq returnValue T checkThick nil)
	(if (member (setq kod (get_tile "kod")) (list "RF" "PN" "YNK"))
		(progn
			(cond
				((and mozel_mUnit (not (equal mozel_mUnit T)))
					(if (member (get_mdata mozel_mUnit k:WPanelUsageType) (list "independent_side" "independent_shelf" "independent_panelwall"))
						(setq checkThick T)
					)
				)
				((equal mozel_mUnit T)
					(setq checkThick T)
				)
			)
			(if checkThick
				(progn
					(if
						(setq checkValue
							(cond
								((equal kod "PN")
									(get_tile "der")
								)
								((equal kod "RF")
									(get_tile "shlftck")
								)
								((equal kod "YNK")
									(get_tile "gen")
								)
							)
						)
						
						(if (not (member checkValue (list "1.8" "3.6")))
							(progn
								(setq returnValue nil)
								(set_tile "error" (xstr "Disallowed thickness value.\t\ Set 1.8 or 3.6 !!!"))
							)
						)
					)
				)
			)
		)
	)
	returnValue
)

(defun lmm_showOutPutDialog:initoverrides ( / )
	(setq lmm-sr-exportTempDirect "1")
	(mode_tile "lmm_exportTempDirect" 1)
)

(defun mergeGOLAProfiles (ssmodullistt golaLayer / curStartIndex subEnt funresult golaFlist startPoint angleProfil codeList curIndex profilWid profilCode startPointsListcp profilHei profilHeiList)
	(setq funresult "")
	(command "_.UNDO" "_M")
	(command "_.EXPLODE" ssmodullistt)
	(setq golaFlist (ssget "x" (list (cons 8 golaLayer))))
	(setq codeList nil)
	(setq startPointsList nil)
	(setq endPointsList nil)
	(setq profilHeiList nil)
	(foreach golaProfil (convertsstolist golaFlist)			
		(setq startPoint (getEntpro golaProfil 10))
		(setq angleProfil (getEntpro golaProfil 50))
		(setq profilWid (get_mdata golaProfil k:WID))
		
		(if (setq profilHei (get_mdata golaProfil k:HEI))
			(setq profilHeiList (append (list profilHei) profilHeiList))
		)
		
		(if profilWid 
			(progn 
				(setq profilCode (get_mdata golaProfil k:WPanelUsageType))
				(setq endPoint (polar startPoint angleProfil  profilWid))		
				(setq startpoint (mapcar '(lambda(x) (round (+ x 0.00001) 1) ) startpoint))
				(setq endPoint (mapcar '(lambda(x) (round (+ x 0.00001) 1) ) endPoint))
				(setq codeList (append (list profilCode) codeList))
				(setq startPointsList (append (list startPoint) startPointsList))
				(setq endPointsList (append (list endPoint) endPointsList))	
			)
		)
	)

	(setq startPointsListcp startPointsList)
	(foreach startPointt startPointsListcp
		(setq curStartIndex (getItemIndex startPointt startPointsList) )
		(setq newEndPoint (getnth curStartIndex endPointsList))
		(if 
			(and 
				(setq curIndex (getItemIndex startPointt endPointsList)) 
				(equal (getnth curIndex codeList) (getnth curStartIndex codeList)) 
				(isItLinear (getnth curIndex startPointsList) (getnth curIndex endPointsList) newEndPoint)
			)
			(progn
				(setq endPointsList (replace newEndPoint curIndex endPointsList))
				(setq endPointsList (removeAt curStartIndex endPointsList))
				(setq startPointsList (removeAt curStartIndex startPointsList))
				(setq codeList (removeAt curStartIndex codeList))
				(setq profilHeiList (removeAt curStartIndex profilHeiList))
			)
		)
	)
	(command "_.UNDO" "_BA")
	(list codeList startPointsList endPointsList profilHeiList)	
)

(defun isItLinear (p1 p2 p3)
	(or
		(equal (ang3p p1 p2 p3) pi 0.1)
		(equal (ang3p p1 p2 p3) 0.0 0.1)
		(equal (ang3p p1 p2 p3) (* 2 pi) 0.1)
	)
)

(defun createGOLAProfiles (ssmodullistt layerName / golasList myCode startPList endPList funTemp golaMat CProfileitemCount LProfileItemCount LProfileCornerItemCount string1 string2 string3 shareStart shareEnd)
	(setq golasList (mergeGOLAProfiles ssmodullistt layerName) myind 0 funresult "")	
	(setq golaCodeList (getnth 0 golasList))
	(setq startPList (getnth 1 golasList))
	(setq endPList (getnth 2 golasList))
	(setq heiList (getnth 3 golasList))

	(setq CProfileitemCount 0.0 LProfileItemCount 0.0 LProfileCornerItemCount 0.0)

	(setq golaMat (if (and (tblobjname "LAYER" layerName) (setq golaMat (car (getmatname (tblobjname "LAYER" layerName))))) golaMat ""))
	(foreach myCode golaCodeList
		(cond
			((equal myCode "GYAP") ; MODUL.ini de tan�mlanm�� C profil
				(setq CProfileitemCount (+ CProfileitemCount 2))
			)
			((equal myCode "GYUP") ; MODUL.ini de tan�mlanm�� L profil
				(setq shareStart (member (getnth myind startPList) endPList) shareEnd (member (getnth myind endPList) startPList))
				(if (or shareStart shareEnd)
					(progn
						(if (not (and shareStart shareEnd))
							(setq LProfileItemCount (+ LProfileItemCount 1.0))
						)
						(if shareStart
							(setq LProfileCornerItemCount (+ LProfileCornerItemCount 0.5))
						)
						(if shareEnd
							(setq LProfileCornerItemCount (+ LProfileCornerItemCount 0.5))
						)
					)
					(setq LProfileItemCount (+ LProfileItemCount 2.0))
				)
			)
		)
		
		(setq funTemp (sprintf "<GOLAPROFILE>\n<GROUP>GOLAPROFILE</GROUP>\n<CODE>~s</CODE>\n<UNIQUECODE>~s</UNIQUECODE>\n<MATERIAL>~s</MATERIAL>\n<QUANTITY>~f</QUANTITY>\n<DMOD_DMAT>~s</DMOD_DMAT>\n<UNITNOTE>~s</UNITNOTE>\n</GOLAPROFILE>\n" (list myCode (strcat myCode "$" (rtos (+ 1 myind)))  golaMat (if (member myCode sopranoBoyGonderilecekGolaProfilListesi) (/ (getnth myind heiList) 100.0) (/ (distance (getnth myind startPList) (getnth myind endPList)) 100.0)) (strcat (getKapakname) (substr golaMat 1 2)) "Gola profil")))
		(setq funresult (strcat funTemp funresult))
		(setq myind (+ myind 1))		
	)
	
	(setq string1 "" string2 "" string3 "")
	(if (not (equal LProfileItemCount 0.0))
		(progn
			(setq string1 (strcat "<GOLAPROFILE_ITEM>\n<CODE>" golaLprofileEndingItemCodeLeft "</CODE>\n<QUANTITY>" (rtos (/ LProfileItemCount 2)) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
			(setq string1 (strcat string1 "<GOLAPROFILE_ITEM>\n<CODE>" golaLprofileEndingItemCodeRight "</CODE>\n<QUANTITY>" (rtos (/ LProfileItemCount 2)) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
		)
	)
	(if (not (equal CProfileitemCount 0.0))
		(setq string2 (strcat "<GOLAPROFILE_ITEM>\n<CODE>" golaCProfileEndingItem "</CODE>\n<QUANTITY>" (rtos CProfileitemCount) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
	)
	(if (not (equal LProfileCornerItemCount 0.0))
		(setq string3 (strcat "<GOLAPROFILE_ITEM>\n<CODE>" golaLProfileCornerItem "</CODE>\n<QUANTITY>" (rtos LProfileCornerItemCount) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
	)
	
	(strcat funresult string1 string2 string3)
)

(defun createExternalHandles (ssmodullistt / modull resultString)
	(setq resultString "")
	(foreach modull (convertsstolist ssmodullistt)
		(if (checkStrInStr "rutin_ItsHandle" (get_mdata modull k:mdata))
			(progn
				(setq resultString
					(strcat 
						resultString "\n"
						"<HANDLES>\n"
						"<GROUP>HANDLES</GROUP>\n"
						"<CODE>" (parse$ (dfetch 2 modull)) "</CODE>\n"
						"<MODEL>" (parse$ (dfetch 2 modull)) "</MODEL>\n"
						"<QUANTITY>" (itoa (ifnull (getDataFromFirmSpesific modull 'handleQuantity) 0)) "</QUANTITY>\n"
						"<MODUL>" (dfetch 2 modull) "</MODUL>\n"
						"</HANDLES>\n"
					)
				)
			)
		)
	)
	resultString
)

(defun createLedLights (ssmodullistt / resultString leds led)
	(setq resultString "")
	(foreach modull (convertsstolist ssmodullistt)
		(if (setq leds (getSopranoLedLightsData modull))
			(foreach led leds
				(setq resultString
					(strcat 
						resultString 
						"\n"
						"<SOPRANO_LEDLIGHTS>\n"
						"<GROUP>SOPRANO_LEDLIGHTS</GROUP>\n"
						"<CODE>" (car led) "</CODE>\n"
						"<POSITION>" (caddr led) "</POSITION>\n"
						"<QUANTITY>" (cadr led) "</QUANTITY>\n"
						"<MODUL>" (dfetch 2 modull) "</MODUL>\n"
						"</SOPRANO_LEDLIGHTS>"
					)
				)
			)
		)
	)
	resultString
)

(defun createSpotLights (ssmodullistt / modull resultString spots firstPickStyle externalSpots)
	(setq resultString "")
	(foreach modull (convertsstolist ssmodullistt)
		(if (setq spots (getSpotData modull))
			(foreach spot spots
				(setq resultString
					(strcat 
						resultString 
						"\n"
						"<SOPRANO_SPOTLIGHTS>\n"
						"<GROUP>SOPRANO_SPOTLIGHTS</GROUP>\n"
						"<CODE>" (car spot) "</CODE>\n"
						"<POSITION>" (cadr spot) "</POSITION>\n"
						"<QUANTITY>1</QUANTITY>\n"
						"<MODUL>" (dfetch 2 modull) "</MODUL>\n"
						"</SOPRANO_SPOTLIGHTS>"
					)
				)
			)
		)
	)

	(setq firstPickStyle (getvar "PICKSTYLE"))
		(setvar	"PICKSTYLE" 0)
		(setq externalSpots (convertsstolist (ssget "x" (list	'(0 . "INSERT") '(8 . "SPOT_LIGHT_HARICI") ))))

		(foreach spot externalSpots
			(setq resultString
				(strcat 
					resultString 
					"\n"
					"<SOPRANO_SPOTLIGHTS>\n"
					"<GROUP>SOPRANO_SPOTLIGHTS</GROUP>\n"
					"<CODE>" (parse$ (dfetch 2 spot)) "</CODE>\n"
					"<POSITION></POSITION>\n"
					"<QUANTITY>1</QUANTITY>\n"
					"<MODUL>HARICI</MODUL>\n"
					"</SOPRANO_SPOTLIGHTS>"
				)
			)
		)

		(setvar	"PICKSTYLE" firstPickStyle)

	resultString
)

(defun createExternalGolaEndingItem (ssmodullistt / modull resultString itemCode endingRightCount endingLeftCount endingCCount endingCornerCount endingCornerOutCount golaMat)
	(setq resultString "" golaMat (getlayermat "GOLAPROFILE"))
	(mapcar '(lambda(x) (set x 0)) (list 'endingRightCount 'endingLeftCount 'endingCCount 'endingCornerCount 'endingCornerOutCount))
	(foreach modull (convertsstolist ssmodullistt)
		(setq itemCode (parse$ (dfetch 2 modull)))
		(cond
			((equal itemCode golaLprofileEndingItemCodeLeft)
				(setq endingLeftCount (+ 1 endingLeftCount))
			)
			((equal itemCode golaLprofileEndingItemCodeRight)
				(setq endingRightCount (+ 1 endingRightCount))
			)
			((equal itemCode golaCProfileEndingItem)
				(setq endingCCount (+ 1 endingCCount))
			)
			((equal itemCode golaLProfileCornerItem)
				(setq endingCornerCount (+ 1 endingCornerCount))
			)
			((equal itemCode golaLProfileCornerItemOut)
				(setq endingCornerOutCount (+ 1 endingCornerOutCount))
			)
		)
	)
	(if (not (equal endingLeftCount 0.0))
		(setq resultString (strcat resultString "<GOLAPROFILE_ITEM>\n<CODE>" golaLprofileEndingItemCodeLeft "</CODE>\n<QUANTITY>" (rtos endingLeftCount) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
	)
	(if (not (equal endingRightCount 0.0))
		(setq resultString (strcat resultString "<GOLAPROFILE_ITEM>\n<CODE>" golaLprofileEndingItemCodeRight "</CODE>\n<QUANTITY>" (rtos endingRightCount) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
	)
	(if (not (equal endingCCount 0.0))
		(setq resultString (strcat resultString "<GOLAPROFILE_ITEM>\n<CODE>" golaCProfileEndingItem "</CODE>\n<QUANTITY>" (rtos endingCCount) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
	)
	(if (not (equal endingCornerCount 0.0))
		(setq resultString (strcat resultString "<GOLAPROFILE_ITEM>\n<CODE>" golaLProfileCornerItem "</CODE>\n<QUANTITY>" (rtos endingCornerCount) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
	)
	(if (not (equal endingCornerOutCount 0.0))
		(setq resultString (strcat resultString "<GOLAPROFILE_ITEM>\n<CODE>" golaLProfileCornerItemOut "</CODE>\n<QUANTITY>" (rtos endingCornerOutCount) "</QUANTITY>\n<MATERIAL>" golaMat "</MATERIAL>\n </GOLAPROFILE_ITEM>\n"))
	)
	
	resultString
)

(defun lmm_createBilltempUnitLISPXMLADDS ( / modul modulList varName)
	(setq modulList (lmm_getOnlyKitchenModuls))
	(foreach modul (convertsstolist modulList)			
		(setq varName (strcat  (dfetch 5 modul) "_LISPXMLADDS"))			
		(set (read varName) "")
		(set (read varName) (strcat (lmm_createBilltempDwgAccs modul) (if (eval (read varName)) (eval (read varName)) "")))		
		(set (read varName) (strcat (createProcessDif modul) (if (eval (read varName)) (eval (read varName)) "")))
		
		
		(setq varName (strcat  (dfetch 5 modul) "_LISPXMLGROUPADDS"))
		(set (read varName) "")
		(addSpotLightDataToUnit modul varName)
		(addLedLightDataToUnit modul varName)
		(addKlapaDataToUnit modul varName)
		(addSinkCoverDataToUnit modul varName)
		(addbzdUstMekanizmaDataToUnit modul varName)
		(addColorDataToUnit modul varName)
	)
	(addDataToVisibleSides modulList)
)

(defun addColorDataToUnit (modul varName / renk renkData xmlString)
	(setq renkData (getDataFromFirmSpesific modul 'renk))
	(setq xmlString (strcat "<SOPRANO_COLOR>" (ifnull renkData "") "</SOPRANO_COLOR>\n"))

	(set (read varName) (strcat xmlString (if (eval (read varName)) (eval (read varName)) "")))
	(setq dynamiclyCreatedSymbols (append dynamiclyCreatedSymbols (list varName)))
)

(defun addKlapaDataToUnit (modul varName / klapaHeight xmlString kHeight)
	(setq kHeight (getDataFromFirmSpesific modul 'klapaHeight))
	(setq xmlString (strcat "<SOPRANO_KLAPA_HEIGHT>" (if kHeight (rtos (cu2mm kHeight)) "") "</SOPRANO_KLAPA_HEIGHT>\n"))

	(set (read varName) (strcat xmlString (if (eval (read varName)) (eval (read varName)) "")))
	(setq dynamiclyCreatedSymbols (append dynamiclyCreatedSymbols (list varName)))
)

(defun addSinkCoverDataToUnit (modul varName / xmlString cover)
	(setq cover (getDataFromFirmSpesific modul 'sinkCover))
	(setq xmlString (strcat "<SOPRANO_SINK_COVER>" (if (member cover '(nil "" "Yok")) "" (last (splitstr cover "-"))) "</SOPRANO_SINK_COVER>\n"))

	(set (read varName) (strcat xmlString (if (eval (read varName)) (eval (read varName)) "")))
	(setq dynamiclyCreatedSymbols (append dynamiclyCreatedSymbols (list varName)))
)

(defun addbzdUstMekanizmaDataToUnit (modul varName / xmlString data)
	(setq data (getDataFromFirmSpesific modul 'bzdUstDolapMekanizma))
	(setq xmlString (strcat "<SOPRANO_FRIDGE_MECHANISM>" (if (member data '(nil "" "Yok")) "" (last (splitstr data "-"))) "</SOPRANO_FRIDGE_MECHANISM>\n"))

	(set (read varName) (strcat xmlString (if (eval (read varName)) (eval (read varName)) "")))
	(setq dynamiclyCreatedSymbols (append dynamiclyCreatedSymbols (list varName)))
)

(defun addLedLightDataToUnit (modul varName / leds reverseLeds sideLedString downLedString)
	(setq leds (getSopranoLedLightsData modul))
	(mapcar '(lambda (x) (setq reverseLeds (append reverseLeds (list (list (caddr x) (cadr x) (car x)))))) leds)
	(setq sideLedString (strcat "<SOPRANO_LED_LIGHTS_SIDE>" (ifnull (caddr (assoc "SIDE" reverseLeds)) "") "</SOPRANO_LED_LIGHTS_SIDE>\n"))
	(setq downLedString (strcat "<SOPRANO_LED_LIGHTS_DOWN>" (ifnull (caddr (assoc "DOWN" reverseLeds)) "") "</SOPRANO_LED_LIGHTS_DOWN>\n"))
	(set (read varName) (strcat sideLedString downLedString (if (eval (read varName)) (eval (read varName)) "")))
	(setq dynamiclyCreatedSymbols (append dynamiclyCreatedSymbols (list varName)))
)

(defun addSpotLightDataToUnit (modul varName / spots spot reverseSpots upSpotString downSpotString)
	(setq spots (getSpotData modul))
	(mapcar '(lambda (x) (setq reverseSpots (append reverseSpots (list (list (cadr x) (car x)))))) spots)
	(setq upSpotString (strcat "<SOPRANO_SPOT_LIGHTS_UP>" (ifnull (cadr (assoc "UP" reverseSpots)) "") "</SOPRANO_SPOT_LIGHTS_UP>\n"))
	(setq downSpotString (strcat "<SOPRANO_SPOT_LIGHTS_DOWN>" (ifnull (cadr (assoc "DOWN" reverseSpots)) "") "</SOPRANO_SPOT_LIGHTS_DOWN>\n"))
	(set (read varName) (strcat upSpotString downSpotString (if (eval (read varName)) (eval (read varName)) "")))
	(setq dynamiclyCreatedSymbols (append dynamiclyCreatedSymbols (list varName)))
)

(defun addDataToVisibleSides (modules / modul entHandles entHandleData varName LMat RMat)
	(foreach modul (convertsstolist modules)
		(if (setq LMat (get_block_material modul "LSIDE"))
			(setq entHandles (append entHandles (list (list modul (getHandleNameOnFirstEntityByLayer modul "LSIDE") LMat))))
		)
		(if (setq RMat (get_block_material modul "RSIDE"))
			(setq entHandles (append entHandles (list (list modul (getHandleNameOnFirstEntityByLayer modul "RSIDE") RMat))))
		)
	)
	
	(foreach entHandleData entHandles
		(setq varName (strcat  (cadr entHandleData) "_LISPXMLGROUPADDS"))
		(set (read varName) "")
		(set (read varName) (createVisibleSideData entHandleData))
		(setq dynamiclyCreatedSymbols (append dynamiclyCreatedSymbols (list varName)))
	)
)

(defun createVisibleSideData (entHandleData / allDoorsOnModule dMat dMod i stop)
	(setq dMat (substr (caddr entHandleData) 1 2))
	(setq allDoorsOnModule (listDoorModelAndTypesIn (car entHandleData) "." nil))
	
	(if (apply 'and (mapcar '(lambda (x) (equal "B" (_CONVERTDOORTYPETOCOSTTYPE (cadr x)))) allDoorsOnModule))
		(setq dMod
			(substr
				(cond
					((equal (dfetch 8 (car entHandleData)) "CAB_BODY_BASE")
						(GetKapakName)
					)
					((equal (dfetch 8 (car entHandleData)) "CAB_BODY_WALL")
						(GetUpKapakName)
					)
					((equal (dfetch 8 (car entHandleData)) "CAB_BODY_TALL")
						(GetTallKapakName)
					)
				)
				1 4
			)
		)
		(progn
			(setq i 0)
			(while (and (null stop) (< i (length allDoorsOnModule)))
				(if (equal (_CONVERTDOORTYPETOCOSTTYPE (cadr (getnth i allDoorsOnModule))) "A")
					(progn
						(setq stop T)
						(setq dMod (substr (car (getnth i allDoorsOnModule)) 1 4))
					)
				)
				(setq i (+ i 1))
			)
		)
	)
	(if (and dMod dMat)
		(strcat "<DMOD_DMAT>" dMod dMat "</DMOD_DMAT>\n" "<KITCHENDMODEL>" dMod "</KITCHENDMODEL>\n")
		""
	)
)

(defun getHandleNameOnFirstEntityByLayer ( entNam layNam / subEnt devam returnValue)
	(setq subEnt (cdr (assoc -2  (tblsearch "block" (dfetch 2 entNam)))) devam T)
	(while (and subEnt devam)
		(if (equal (dfetch 8 subEnt) layNam)
			(progn
				(setq devam nil)
				(setq returnValue (dfetch 5 subEnt))
			)
		)
		(setq subEnt (entnext subEnt))
	)
	returnValue
)

(defun analyzeKitchenForXML ( / modulListt varName )
	(princ)
	(setq modulListt (lmm_getOnlyKitchenModuls))
	(setq G_LISPGLOBALXMLADDS nil)

	(if modulListt (setq G_LISPGLOBALXMLADDS (strcat (createGOLAProfiles modulListt "GOLAPROFILE") "\n" (createGOLAProfiles modulListt "GOLAVERTICALPROFILE") "\n" (createExternalHandles modulListt) "\n" (createExternalGolaEndingItem modulListt) "\n")))
	
	(setq G_LISPGLOBALXMLADDS (strcat (ifnull G_LISPGLOBALXMLADDS "") (createSpotLights modulListt)))
	(setq G_LISPGLOBALXMLADDS (strcat (ifnull G_LISPGLOBALXMLADDS "") (createLedLights modulListt)))

	(princ)
)

(defun createProcessDif (modull / modulmdata dataList widDelta heiDelta heiMDataListElement depDelta depMDataListElement funresult modulCode mdataList kertmeNumber backPanelIsFronter)
	(setq funresult "")
	(if (and 
			(setq modulmdata (get_mdata modull k:mdata))
			(ifnull (eval (read (strcat (lmm_sfRead (parse$ (dfetch 2 modull)))  "_WLIST"))) (ifnull (getnth 0 (eval (read (strcat (lmm_sfRead (parse$ (dfetch 2 modull)))  "_WHDNLIST")))) (ifnull WHDChanger_WLIST nil)))
		)
		(progn
			(setq modulCode (car (read modulmdata)))
			(if (null ad_allModulsInOne) (genAllModulsIndexList))					
			(if (last (assoc modulCode ad_allModulsInOne ))
				(progn						
					(setq mdataList (assoc (getnth 1 (assoc modulCode ad_allModulsInOne )) (eval (read (strcat (last (assoc modulCode ad_allModulsInOne )) ":mlofl")))))					
					
					; If modul is corner unit
					(if (or (equal (GETNTH 1 MDATALIST) 'AK2) (equal (GETNTH 1 MDATALIST) 'UK2))
						(progn
							(setq heiMDataListElement (getnth 4 mdataList))
							(setq depMDataListElement (getnth 5 mdataList))
						)
						(progn
							(setq heiMDataListElement (getnth 3 mdataList))
							(setq depMDataListElement (getnth 4 mdataList))
						)
					)
					
					(setq widDelta (if (and (get_mdata modull k:CEn) (getnth 2 mdataList) (not (equal (type (getnth 2 mdataList)) 'LIST)) )  (- (lmm_atofFormula (getnth 2 mdataList))  (get_mdata modull k:CEn ) ) 0.0))			
					(setq heiDelta (if (and (get_mdata modull k:CBoy) heiMDataListElement (not (equal (type heiMDataListElement) 'LIST))) (- (lmm_atofFormula heiMDataListElement) (get_mdata modull k:CBoy) ) 0.0))
					
					(if depMDataListElement
						(if (isList depMDataListElement)
							(progn	
								(setq depDelta (- (lmm_atofFormula (ifnull (car (eval depMDataListElement)) (get_mdata modull k:CDer) )) (get_mdata modull k:CDer) ))				
							)
							(setq depDelta (- (lmm_atofFormula depMDataListElement) (get_mdata modull k:CDer) ))
						)
						(setq depDelta 0.0)
					)
				)
				(progn
						(setq widDelta 0.0 heiDelta 0.0 depDelta 0.0)
				)
			)

			(if (not (equal widDelta 0.0 0.001))
				(setq funresult (strcat funresult (createProcessDif::createWHDStateNode 1)))
			)
			(if (not (equal heiDelta 0.0 0.001))
				(setq funresult (strcat funresult (createProcessDif::createWHDStateNode 2)))
			)
			(if (not (equal depDelta 0.0 0.001))
				(setq funresult (strcat funresult (createProcessDif::createWHDStateNode 3)))
			)
		)
	)
	(if (equal (getDataFromFirmSpesific modull 'backPanelIsFronter) "1") 
		(setq funresult (strcat funresult (createProcessDif::createWHDStateNode 4)))
	)

	funresult
)

;1-> width change
;2-> height change
;3-> depth change
;4-> BackPanel change
(defun createProcessDif::createWHDStateNode (changeType / )
	(strcat 
		"<CHANGESTATE>\n"
		"<GROUP>CHANGESTATE</GROUP>\n"
		"<TYPE>" (rtos changeType) "</TYPE>\n"
		"<UNIQUECODE>" (dfetch 2 modull) "</UNIQUECODE>\n"
		"<QUANTITY>1</QUANTITY>\n"
		"</CHANGESTATE>\n"
	)
)

(defun purgeAndwriteBillItemsToXML ( / dynamiclyCreatedSymbols sembolStr)
	(purgeUBlocks)
	(if (tblobjname  "LAYER" "{GIZLI-CEPHE}")
		(progn
			(setvar "TILEMODE" 1)
			(gorunus (Xstr "PLan")) ;Modul layerlar�n� orjinaline set etmek i�in konuldu.
		)
	)
	(lmm_createBilltempUnitLISPXMLGROUPADDS)
	(lmm_createBilltempUnitLISPXMLADDS)
	(writeBillItemsToXML)
	
	(foreach sembolStr dynamiclyCreatedSymbols
		(set (read sembolStr) nil)
	)
)

(defun lmm_createBilltempUnitLISPXMLGROUPADDS ()
	(analyzeKitchenForXML)
	(createRuleInfoForXML)
	(createProjectDetailInfoForXML)
)

(defun createRuleInfoForXML ( / emptyHandleModel baseUnitHandle wallUnitHandle tallUnitHandle hingeType railType)
	(setq emptyHandleModel (splitstr (iniread ad_MOD-INI "exportProject" "emptyHandleModels" "") "|"))

	(setq 
		baseUnitHandle 	(ifnull (getxref "AD_KULP_COB_BODY_BASE$$") "")
		wallUnitHandle 	(ifnull (getxref "AD_KULP_COB_BODY_WALL$$") "")
		tallUnitHandle 	(ifnull (getxref "AD_KULP_COB_BODY_TALL$$") "")
		hingeType 		(getxref "SOPRANO_HINGE_TYPE")
		railType 		(getxref "SOPRANO_DRAWER_RAIL_TYPE")
	)
	
	(setq G_LISPGLOBALXMLADDS
		(strcat
			(ifnull G_LISPGLOBALXMLADDS "")
			"\n"
			"<RULEINFORMATION>\n"
				"<PROJECTTYPE>" (ifnull (getxref "SOPRANO_PROJECT_TYPE") "") "</PROJECTTYPE>\n"

				"<PROJECTFEATURE>" (ifnull (getxref "SOPRANO_PROJECT_FEATURE") "") "</PROJECTFEATURE>\n"

				"<HINGEMARK>" (if (member hingeType '("-" nil "")) "" hingeType) "</HINGEMARK>\n"
				"<DRAWERRAILMARK>" (if (member railType '("-" nil "")) "" hingeType) "</DRAWERRAILMARK>\n"

				"<BASEBODYMATERIAL>" (ifnull (getlayermat "CAB_BODY_BASE") "") "</BASEBODYMATERIAL>\n"
				"<WALLBODYMATERIAL>" (ifnull (getlayermat "CAB_BODY_WALL") "") "</WALLBODYMATERIAL>\n"
				"<TALLBODYMATERIAL>" (ifnull (getlayermat "CAB_BODY_TALL") "") "</TALLBODYMATERIAL>\n"
				"<GLASSSHELFMATERIAL>" (ifnull (getxref "SOPRANO_GLASS_SHELF_MAT") "") "</GLASSSHELFMATERIAL>\n"
				
				"<BOTTOMCOVER>" (ifnull (getxref "AD_DOOR_NAME$$") "") "</BOTTOMCOVER>\n"
				"<BOTTOMCOVERMATERIAL>" (ifnull (getxref "AD_BASEUNITDOOR_MATERIAL$$") "") "</BOTTOMCOVERMATERIAL>\n"
				"<BOTTOMCOVEREDGEBAND>" (ifnull (getxref "DOOR_EDGESTRIP_CODE") "") "</BOTTOMCOVEREDGEBAND>\n"
				"<BOTTOMCOVERHANDLETYPE>" (if (member baseUnitHandle emptyHandleModel) "" baseUnitHandle) "</BOTTOMCOVERHANDLETYPE>\n"
				
				"<TOPCOVER>" (ifnull (getxref "AD_UPDOOR_NAME$$") "") "</TOPCOVER>\n"
				"<TOPCOVERMATERIAL>" (ifnull (getxref "AD_WALLUNITDOOR_MATERIAL$$") "") "</TOPCOVERMATERIAL>\n"
				"<TOPCOVEREDGEBAND>" (ifnull (getxref "DOORWALL_EDGESTRIP_CODE") "") "</TOPCOVEREDGEBAND>\n"
				"<TOPCOVERHANDLETYPE>" (if (member wallUnitHandle emptyHandleModel) "" wallUnitHandle) "</TOPCOVERHANDLETYPE>\n"
				
				"<COLUMNCOVER>" (ifnull (getxref "AD_BODYDOOR_NAME$$") "") "</COLUMNCOVER>\n"
				"<COLUMNCOVERMATERIAL>" (ifnull (getxref "AD_BODYDOOR_MATERIAL$$") "") "</COLUMNCOVERMATERIAL>\n"
				"<COLUMNCOVEREDGEBAND>" (ifnull (getxref "DOORTALL_EDGESTRIP_CODE") "") "</COLUMNCOVEREDGEBAND>\n"
				"<COLUMNCOVERHANDLETYPE>" (if (member tallUnitHandle emptyHandleModel) "" tallUnitHandle) "</COLUMNCOVERHANDLETYPE>\n"
				
				"<GOLAPROFILEMATERIAL>" (ifnull (getlayermat "GOLAPROFILE") "") "</GOLAPROFILEMATERIAL>\n"
				"<ALUMINIUMCOVERPROFILEMATERIAL>" (ifnull (getlayermat "ALUMINIUM_FRAME") "") "</ALUMINIUMCOVERPROFILEMATERIAL>\n"
				"<ALUMINIUMCOVERGLASSMATERIAL>" (ifnull (getlayermat "ALUMINIUM_DOOR_GLASS") "") "</ALUMINIUMCOVERGLASSMATERIAL>\n"
				"<CLASSICCOVERGLASSMATERIAL>" (ifnull (getlayermat "CAB_DOOR_GLASS") "") "</CLASSICCOVERGLASSMATERIAL>\n"
				"<PLINTHMATERIAL>" (ifnull (getlayermat "PLINTHS") "") "</PLINTHMATERIAL>\n"
				"<BASEHEIGHT>" (rtos ad_bazah 2 0) "</BASEHEIGHT>\n"
			"</RULEINFORMATION>\n"
		)
	)
)

(defun createProjectDetailInfoForXML ()
	(setq G_LISPGLOBALXMLADDS
		(strcat
			(ifnull G_LISPGLOBALXMLADDS "")
			"\n"
			"<PROJECTDETAILINFORMATION>\n"
				"<ADEKO_CURRENT_ACCOUNT>" (ifnull (getxref "ADEKO_CURRENT_ACCOUNT") "") "</ADEKO_CURRENT_ACCOUNT>\n"
				"<CURRENT_NAME>" (ifnull (getxref "ADEKO_CURRENT_NAME") "") "</CURRENT_NAME>\n"
				"<DESIGNER_SALES_ARCHITECT>" (ifnull (getxref "ADEKO_DESIGNER_SALES_ARCHITECT") "") "</DESIGNER_SALES_ARCHITECT>\n"
				"<CUSTOMER_NAME>" (ifnull (getxref "ADEKO_CUSTOMER_NAME") "") "</CUSTOMER_NAME>\n"
				"<CUSTOMER_ADDRESS>" (ifnull (getxref "ADEKO_CUSTOMER_ADDRESS") "") "</CUSTOMER_ADDRESS>\n"
				"<CUSTOMER_DISTRICT>" (ifnull (getxref "ADEKO_CUSTOMER_DISTRICT") "") "</CUSTOMER_DISTRICT>\n"
				"<CUSTOMER_CITY>" (ifnull (getxref "ADEKO_CUSTOMER_CITY") "") "</CUSTOMER_CITY>\n"
				"<CUSTOMER_POSTCODE>" (ifnull (getxref "ADEKO_CUSTOMER_POSTCODE") "") "</CUSTOMER_POSTCODE>\n"
				"<CUSTOMER_PHONE>" (ifnull (getxref "ADEKO_CUSTOMER_PHONE1") "") "</CUSTOMER_PHONE>\n"
				"<CUSTOMER_EMAIL>" (ifnull (getxref "ADEKO_CUSTOMER_EMAIL") "") "</CUSTOMER_EMAIL>\n"
				"<CUSTOMER_COLORSAMPLE>" (ifnull (getxref "ADEKO_CUSTOMER_COLORSAMPLE") "") "</CUSTOMER_COLORSAMPLE>\n"
				"<CUSTOMER_NOTE>" (ifnull (getxref "ADEKO_CUSTOMER_NOTE") "") "</CUSTOMER_NOTE>\n"
				"<PROJECT_TYPE>" (ifnull (getxref "ADEKO_PROJECT_TYPE") "") "</PROJECT_TYPE>\n"
				"<PROJECT_FLAT>" (ifnull (getxref "ADEKO_PROJECT_FLAT") "") "</PROJECT_FLAT>\n"
				"<PROJECT_NO>" (ifnull (getxref "ADEKO_PROJECT_NO") "") "</PROJECT_NO>\n"
				"<PROJECT_SALES_SHAPE>" (ifnull (getxref "ADEKO_PROJECT_SALES_SHAPE") "") "</PROJECT_SALES_SHAPE>\n"
				"<PROJECT_SALES_TYPE>" (ifnull (getxref "ADEKO_PROJECT_SALES_TYPE") "") "</PROJECT_SALES_TYPE>\n"
				"<PROJECT_SALES_ATTIRIBUTE>" (ifnull (getxref "ADEKO_PROJECT_SALES_ATTIRIBUTE") "") "</PROJECT_SALES_ATTIRIBUTE>\n"
			"</PROJECTDETAILINFORMATION>\n"
		)
	)
)

(defun getItemIndex2 (itemMember itemList / item i returnValue)
	(setq returnValue "0")
	(setq i 0)
	(foreach item itemList
		(if (equal (cadr item) itemMember) (setq returnValue (rtos i)))
		(i++)
	)
	returnValue
)

(defun existXref (refString / deger)
	(if (member (setq deger (getxref refString)) (list nil "")) nil deger)
)

(defun _ITEMMAIN_V2 (itemCode parentCode Parameters / currentModulCode uniqueCode uniqueParentCode itemEnt itemOffset tempContinue ItemCodesAndParameterList itemCodeParam quantityParam quantityUnitParam)
	(if (isList itemCode)
		(progn
			(if (isList parentCode)
				(progn
					(setq ItemCodesAndParameterList(listtrans (list itemCode parentCode Parameters)))

					(foreach Param ItemCodesAndParameterList
						(mapcar 'set '(itemCodeParam parentCodeParam Param2) Param)
						(mapcar 'set '(quantity quantityUnit) Param2)
						
						(setq tempContinue T quantityUnit (XSTR quantityUnitParam) )
						(if (and (or (equal lmm-outPutType 2) (equal lmm-outPutType 3)) (equal lmm-s-drawModeShowItems "0"))
							(setq tempContinue nil)	
						)
						
						(if (and (not (equal quantity 0.0)) (not (null quantityUnit)) (not (null quantity)) (not (null itemCodeParam)) (not (null parentCodeParam)) tempContinue) 
							(progn
								(lmm_createUniqueCodes nil parentCodeParam itemCodeParam)
								
								(setq itemOffset 0)
								(if (setq itemEnt  (lmm_getComponentFromCodes itemCodeParam parentCodeParam nil) )
									(progn
										(lmm_setLMMdata itemEnt (list ( cons lmm-k-quantity (+ quantity (lmm_getLMMdata itemEnt lmm-k-quantity)) )))
									)
									(progn			
										(setq itemEnt (lmm_drawPolyline (list (list 0 itemOffset 0) 0 (list 35 (+ 10 itemOffset) 0) 0 (list 70 itemOffset 0) 0 (list 0 itemOffset 0) 0) "0" "nil"))
										(lmm_createObjsLMMDATA itemEnt itemCodeParam parentCodeParam nil "ITEM" (list 0 1))
									)
									
								)
							)
						)
					)
				)
				(progn
					(setq ItemCodesAndParameterList(listtrans (list itemCode Parameters)))

					(foreach Param ItemCodesAndParameterList
						(mapcar 'set '(itemCodeParam Param2) Param)
						(mapcar 'set '(quantity quantityUnit) Param2)
						
						(setq tempContinue T quantityUnit (XSTR quantityUnit) )
						(if (and (or (equal lmm-outPutType 2) (equal lmm-outPutType 3)) (equal lmm-s-drawModeShowItems "0"))
							(setq tempContinue nil)	
						)
						
						(if (and (not (equal quantity 0.0)) (not (null quantityUnit)) (not (null quantity)) (not (null itemCodeParam)) (not (null parentCode)) tempContinue) 
							(progn
								(lmm_createUniqueCodes nil parentCode itemCodeParam)
								
								(setq itemOffset 0)
								(if (setq itemEnt  (lmm_getComponentFromCodes itemCodeParam parentCode nil) )
									(progn
										(lmm_setLMMdata itemEnt (list ( cons lmm-k-quantity (+ quantity (lmm_getLMMdata itemEnt lmm-k-quantity)) )))
									)
									(progn
										(setq itemEnt (lmm_drawPolyline (list (list 0 itemOffset 0) 0 (list 35 (+ 10 itemOffset) 0) 0 (list 70 itemOffset 0) 0 (list 0 itemOffset 0) 0) "0" "nil"))
										(lmm_createObjsLMMDATA itemEnt itemCodeParam parentCode nil "ITEM" (list 0 1))
									)
									
								)
							)
						)
					)
				)
			)
		)
		(progn
			(mapcar 'set '(quantity quantityUnit) Parameters)
	
			(setq tempContinue T quantityUnit (XSTR quantityUnit) )
			(if (and (or (equal lmm-outPutType 2) (equal lmm-outPutType 3)) (equal lmm-s-drawModeShowItems "0"))
				(setq tempContinue nil)	
			)
			
			(if (and (not (equal quantity 0.0)) (not (null quantityUnit)) (not (null quantity)) (not (null itemCode)) (not (null parentCode)) tempContinue) 
				(progn
					(lmm_createUniqueCodes nil parentCode itemCode)
					
					(setq itemOffset 0)
					(if (setq itemEnt  (lmm_getComponentFromCodes itemCode parentCode nil) )
						(progn
							(lmm_setLMMdata itemEnt (list ( cons lmm-k-quantity (+ quantity (lmm_getLMMdata itemEnt lmm-k-quantity)) )))
						)
						(progn
							(setq itemEnt (lmm_drawPolyline (list (list 0 itemOffset 0) 0 (list 35 (+ 10 itemOffset) 0) 0 (list 70 itemOffset 0) 0 (list 0 itemOffset 0) 0) "0" "nil"))
							(lmm_createObjsLMMDATA itemEnt itemCode parentCode nil "ITEM" (list 0 1))
						)
						
					)
				)
			)
		)
	)
)

(setq _ITEMMAIN nil)
(defun _ITEMMAIN (itemCode parentCode Parameters)
	(princ)
)

(defun lmm_runMacro_DWGS (objnameInput / tempDivRcp tempDwgList tempDwgLists tempDwgPath settingsIntBinary)
	(if (get_Mdata objnameInput k:moduleDwgList)
		(foreach tempDwgLists (read (get_Mdata objnameInput k:moduleDwgList))
			(if (not (member tempDwgLists (_ " " "")))
				(progn
					(setq settingsIntBinary (last tempDwgLists))			
					(foreach tempDwgList tempDwgLists									
						(if (and (not (null (GETNTH 0 TEMPDWGLIST))) (equal (getnth 4 tempDwgList) 1))
							(progn
								(set '__MODELACCPARAMS TEMPDWGLIST)
								(setq tempDwgPath (last (splitStrTkn (getnth 0 tempDwgList) "\\")))
								(if (getAccessDWGSettings settingsIntBinary "runDwgRecipes")
									(progn
										(lmm_loadRcp (strcat lmm-divDWGRcpsPath "\\" tempDwgPath "." lmm-rcpsExtention) (list T))
										(if (not (equal lmm-NoNotch T)					)
												(lmm_runMacro_Notch tempDwgPath __NOTCHTYPE)
										)								
									)
								)							
								(if (getAccessDWGSettings settingsIntBinary "addDwgItemName")
									(progn
										;_ITEMMAIN fonksiyonu _ITEMMAIN_V2 ye cevrilmi�tir
										(_ITEMMAIN_V2  (getnth 0 tempDwgList) (getnth 0 tempDwgList) (list  (getnth 5 tempDwgList) (getnth 6 tempDwgList)))						
									)
								)
							)
						)					
					)
				)
			)
		)
	)
)

(defun lmm_runMacro_HandlesExtras ( / tempHandles tempExtras tempCountPlinthLeg tempItem codeTmp dakItemFileName)
		
	(if (and (setq tempHandles (lmm_countModulHandles objname)) (equal lmm-s-createItemHandles "1"))
		(progn
			(foreach tempItem tempHandles	
				;_ITEMMAIN fonksiyonu _ITEMMAIN_V2 ye cevrilmi�tir
				(_ITEMMAIN_V2 (car tempITEM) (car tempITEM) (list (cadr tempITEM) "pc"))
			)
		)
	)
	(if (and (setq tempCountPlinthLeg (lmm_countModulPlinthLegs objname)) (equal lmm-s-createItemPlinthLegs "1"))
		(progn
			(foreach tempItem tempCountPlinthLeg
				(setq codeTmp (strcat lmm-s-plinthLegsCode (rtos (car tempITEM))))
				;_ITEMMAIN fonksiyonu _ITEMMAIN_V2 ye cevrilmi�tir
				(_ITEMMAIN_V2 codeTmp codeTmp (list (cadr tempITEM) "pc"))
			)
		)
	)
	(if (and (setq tempExtras (lmm_countModulExtras objname)) (equal lmm-s-createItemExtras "1"))
		(progn
			
			(foreach tempItem tempExtras
				;dak ekran�ndan girilen item isminde p2ctagit re�etesi var ise, re�etesini kullan. Kendisini item olarak yazd�rtma.
				(setq dakItemFileName (strcat lmm-itemsRcpsPath "\\" (strrep (car tempItem) " " "_") "_TI.p2c"))
				(if (findfile dakItemFileName)
					(lmm_loadRcp dakItemFileName (list T))
					;_ITEMMAIN fonksiyonu _ITEMMAIN_V2 ye cevrilmi�tir
					(_ITEMMAIN_V2  (car tempItem) (car tempItem) (list (atof (ifnull (cadr tempItem) "0")) (getnth 2 tempItem)))
				)
			)
		)
	)
	
	(if (and (equal lmm-s-createItemNotchLobour "1") __NOTCHTYPE (not (equal __NOTCHTYPE 0)))
		(progn
			(setq codeTmp (strcat lmm-s-notchLobourCode (rtos __NOTCHTYPE))) 
			;_ITEMMAIN fonksiyonu _ITEMMAIN_V2 ye cevrilmi�tir
			(_ITEMMAIN_V2 codeTmp codeTmp (list 1.0 "pc"))
		)
	)
)


;kapaksetsde kenarband� i�in listenin i�ine "{(getSopranoDoorEdgestrip)}" yaz�larak kullan�l�r. kullan�lm�yor. silinecek. 
(defun getSopranoDoorEdgestrip ( / laynam)
	(setq laynam (dfetch 8 g_CurrentModulEnt))
	(strcat 
		"PVC_"
		(cond
			((equal laynam "CAB_BODY_BASE")
			(getXref "DOOR_EDGESTRIP_CODE")
			)
			((equal laynam "CAB_BODY_WALL")
			(getXref "DOORWALL_EDGESTRIP_CODE")
			)
			((equal laynam "CAB_BODY_TALL")
			(getXref "DOORTALL_EDGESTRIP_CODE")
			)
		)
	)
)


(defun isSopranoEntegreKulp (kulpModel / model finded)
	(if kulpModel
		(progn
			(foreach model g_SopranoEntegreKulpList
				(if (checkStrInStr model kulpModel) (setq finded T))
			)
			finded
		)
	)
)
;;;;;;;;;;;;;;;;;;;;;ADEKO 17.6.1 DEN �T�BAREN sil sil sil sil sil sil silsilsilsilsil;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq _GETHANDLEMODEL nil)
(defun _GETHANDLEMODEL (doorId / tempEnt tempIns handleInfo)
	(setq tempEnt (getDoorEntFromDoorId_C  doorId (getEntPro g_currentModulEnt 2)) )
	(setq tempIns (entnext (cdr (assoc -2 (tblsearch "block" (dfetch 2 tempEnt))))))
	(while tempIns 
		(if (equal (getEntPro tempIns 8) "HANDLES_AND_KNOBS")
			(setq handleInfo (parseRGT (getEntPro tempIns 2) "HANDLES_AND_KNOBS_" ))
		)
		(setq tempIns (entnext tempIns))
	)
	handleInfo
)
;;;;;;;;;;;;;;;;;;;;;ADEKO 17.6.1 DEN �T�BAREN sil sil sil sil sil sil silsilsilsilsil;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun getPlinthCode ( / )
	(cond
		((equal ad_bazaH 10.0) "167001")
		((equal ad_bazaH 12.0) "167002")
		((equal ad_bazaH 15.0) "167003")
		(T "UNDEFINED_PLINTH")
	)
)

(defun lmm_getPlinths( / firstPickStyle ssPart)
	(setq firstPickStyle (getvar "PICKSTYLE"))
	(setvar	"PICKSTYLE" 0)
	(setq ssPart
		(ssget "x" (list	'(0 . "INSERT") 
							'(8 . "PLINTHS")
					)
		)
	)
	(setvar	"PICKSTYLE" firstPickStyle)
	ssPart
)

;;;;;;;;;;;;;;;;;;;;;ADEKO 17.6.1.beta6 DEN �T�BAREN sil sil sil sil sil sil silsilsilsilsil;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun lmm_getItemRcp (unitCode / itemFileName)
	(if (findfile (strcat lmm-itemsRcpsPath "\\" (setq itemFileName (strcat (strrep unitCode " " "_") "_TI")) ".p2c")) (strcat "ITEMS\\\\" itemFileName))
)

(defun c:raf (/ kod mLofL mozel_mUnit allRcps itemRcp)  ;/ 1mesafept 2mesafept side rkot raf# raflen rafdist rafgen aci temp)
	(init_it)
	(setq allRcps (if (setq itemRcp (lmm_getItemRcp "RF")) (append g_generalPanelRcpPath (list itemRcp)) g_generalPanelRcpPath))
    (setq kod "RF"
          mLofL (list (list kod 'RAF nil ad_ET nil (cons 'QUOTE (list (list 0 "panel" "m2" nil nil allRcps "independent_shelf"))) nil "" "Raf"))
          mozel_mUnit T ; disable EKLE button
    )
    ;(raf kod 60 ad_ET ad_UDDer "" "Raf")
    (raf_tanim)
    (end_it)
)

(defun c:pano (/ kod mLofL blok_list mozel_mUnit tip kod pano_L pano_GEN pano_Kot pano_ET pano_Dists
                 osmod entprev pt ptlist frzllst lnxt temp allRcps itemRcp) ;altz
	(init_it)
	(if (yesNo  (XSTR "Do you want to hide layers which are not related with panels?") )
	(progn
		(setq frzllst (freeze "WALLS,WORKTOPS,TABLE_TOPS,UPSTANDS,PANELS_WALL,CAB_BODY_BASE,CAB_BODY_WALL,CAB_BODY_TALL"))  ;remove "SEALER_STRIPES" later see:ADEKO14-271
			)
	)
		
	(setq allRcps (if (setq itemRcp (lmm_getItemRcp "PN")) (append g_generalPanelRcpPath (list itemRcp)) g_generalPanelRcpPath))
    (setq kod "PN"
          mLofL (list (list kod 'UPANO nil (- ad_UDY (+ ad_TezH ad_ADH ad_bazaH)) ad_ET (cons 'QUOTE (list (list nil "duz" (list "m2" nil) nil nil nil allRcps "independent_panelwall"))) "" (xstr "panel")))
          ;altZ (+ ad_TezH ad_ADH ad_bazaH)
          mozel_mUnit T ; disable EKLE button
    )
    (pano_tanim)
	(redrawUnit (entlast) (list nil  nil nil))
	(defLAYER frzllst '("_T"))

)
 
(defun c:yanak (/ kod mLofL mozel_mUnit yP yH yDer 1mod 2mod maci yaci yanTck ori isL isSirt isKapak allRcps itemRcp)
    (init_it)
	(setq allRcps (if (setq itemRcp (lmm_getItemRcp "YNK")) (append g_generalPanelRcpPath (list itemRcp)) g_generalPanelRcpPath))
    (setq kod "YNK"
          mLofL (list (list kod 'YANAK ad_ET nil nil (cons 'QUOTE (list (list 0 "panel" "m2" nil nil allRcps "independent_side" nil nil 0.0))) "" "Yanak"))
          mozel_mUnit T ; disable EKLE button
    )
    (yanak_tanim)
    ;(yanak_inputs)
;    (yanak kod dWid dH dDep "panel" "" "Yanak")
    (end_it)
)
;;;;;;;;;;;;;;;;;;;;;ADEKO 17.6.1.beta6 DEN �T�BAREN sil sil sil sil sil sil silsilsilsilsil;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun lmm_writeAndSendCRM_createOutPutXMLFile ( xmlMode outputType / outFilereadFile  temptxt currentLine billtempExtension)
		(setq billtempExtension (iniread ad_MOD-INI "dataextract" "extension" ".xml"))
		(moveFile (strcat ad_ADekoPath "billtemp" billtempExtension) (strcat ad_ADekoPath "billtemp_orj.xml") T T)	
		
		(setq readFile (open (strcat ad_ADekoPath "billtemp_orj.xml") "r"))
		(setq outFile (open (strcat ad_ADekoPath "billtemp" billtempExtension) "w"))
		(while (setq currentLine (read-line readFile) )
				(if (not (equal currentLine "</PROJECT>") )
					(progn
						; (setq currentLine (replaceNaturalXML currentLine))
						(write-line currentLine outFile) 
					)
				)		
		)
		(close readFile)
		(lmm_wasop_sawmap outFile outputType (list xmlMode))		
		(lmm_writeAndSendBTool_writeProjectInfo outputType)
		(write-line "</PROJECT>" outFile)
		(close outFile)		
		
		(movefile (strcat ad_adekopath "billtemp" billtempExtension) (strcat ad_adekopath "billtemp_p2c.xml") t t)
		(setq dwgName (parseX (getvar "DWGNAME") ".dwg"))
		(movefile (strcat ad_adekopath "billtemp" billtempExtension) (strcat (lmm_prepareCRMOutDirs) (lmm_getDictValue "DWGPROJECTID") "_" (cond ((equal lmm-s-CrmXmlType "lmm_CrmXmlTypeModular") "M") ((equal lmm-s-CrmXmlType "lmm_CrmXmlTypePart2CamBased") "P2C"))  ".xml") t t)
		(lmm_deleteSelectedAuxRcpFile (strcat ad_ADekoPath "billtemp_orj.xml"))
)

(defun DidYouMakePoz ( / blokNamList firstPickStyle blokNam poseNumber poseNumberList wrongPoseAlert bigPose thisPose poseCount modulString)
	(setq firstPickStyle (getvar "PICKSTYLE"))
	(setvar	"PICKSTYLE" 0)
	
	(setq blokNamList 
		(convertsstolist
			(ssget "x" (list	
							'(0 . "INSERT") 
							'(-4 . "<OR")
								'(8 . "CAB_BODY_*")
								'(8 . "CAB_DOORS")
								'(8 . "PANELS_SIDE") 
								'(8 . "PANELS_WALL")
								'(8 . "PANELS_WALL_DRAWER")
								'(8 . "PANELS_WALL_2ND_BACK") 
								'(8 . "SHELVES") 
								'(8 . "PLINTHS") 
							'(-4 . "OR>")
							'(-4 . "<NOT") '(1001 . "DONTACTLIKEMODULE") '(-4 . "NOT>")
						)
			)
		)
	)
	(setvar	"PICKSTYLE" firstPickStyle)
	
	(foreach blokNam blokNamList
		(if (null (setq poseNumber (get_AppMdata blokNam '(1800 . 1070) "POSEDATA")))
			(progn
				(alert "�nce pozland�rma yap�n�z!")			
				(exit)
			)
			(progn
				(setq poseNumberList (append poseNumberList (list poseNumber)))
			)
		)
	)
	
	(setq wrongPoseAlert "Pozlar uyu�muyor. Proje �zerindeki pozlar� temizleyip tekrardan pozland�rma yap�n�z!")
	(if poseNumberList
		(progn
			(if (not (member 1 poseNumberList)) ;proje 1 nolu pozdan ba�lamal�!
				(progn
					(alert wrongPoseAlert)
					(exit)
				)
				(progn
					(setq bigPose 0)
					(foreach thisPose poseNumberList
						(if (> thisPose bigPose) (setq bigPose thisPose))
					)
					(setq poseCount 1)
					(repeat bigPose
						(if (not (member poseCount poseNumberList)) 
							(progn
								(alert wrongPoseAlert)
								(exit)
							)
						)
						(setq poseCount (+ poseCount 1))
					)
				)
			)
		)
	)
)

(defun isItAcceptableVersion ( / )
	(if (not (equal (getXRef g_sopranoVersionDictName) g_SopranoAcceptableVersion))
		(progn
			(alert (xstr "This project drawn on an old version. Cannot be processed!"))
			(exit)
		)
	)
)

(defun c:imalat (/ productionInfo manufMode)
	(init_it)
	(isItAcceptableVersion)
	(checkClonedImproperlyBlock)
	(checkClonedPose)
	(cdz)
	(DidYouMakePoz)
	(setq manufMode (atoi (iniread ad_MOD-INI "manufacturing" "mode" "2")))
	(if (equal 6 manufMode) 
		(progn
			(lmm_showOutPutDialog)
		)
		(progn
		
			(setq productionInfo (getProjectInfo))	
			(if productionInfo
				(progn
					
					(cond ((equal 1 manufMode) 			(imalat2 productionInfo))
						  ((member manufMode '(2 3 4)) 	(imalat3 productionInfo))
						  ((equal 5 manufMode) 			(eCabinet productionInfo))
						  
					)
				)
			)
		)
	)

	(princ)
	(end_it)
)

;;;;;ADEKO 17.6.2.beta7 den itibaren SIL SIL SIL SIL;;;;;;;;;;;;;;;
(defun getProfileSubEntCodes (path cornerEntsList / cnt90 cnt135 cnt270 szSubEs szTmp)
	(setq cnt90 0 cnt135 0 cnt270 0)
    (countAngles path 'cnt90 'cnt135 'cnt270)

    (setq szSubEs "")	
	(if (setq szTmp (cdr (assoc 0 cornerEnts)))  ;SOL ba�daki eleman kodu
		(setq szSubEs (strcat szSubEs (lmm_sfRead szTmp) ","))
	)
    (if (setq szTmp (cdr (assoc 90 cornerEnts)))
	   	(repeat cnt90 (setq szSubEs (strcat szSubEs (lmm_sfRead szTmp) ",")))
	)
    (if (setq szTmp (cdr (assoc 135 cornerEnts)))
	    (repeat cnt135 (setq szSubEs (strcat szSubEs (lmm_sfRead szTmp) ",")))
	)
    (if (setq szTmp (cdr (assoc 270 cornerEnts)))
	    (repeat cnt270 (setq szSubEs (strcat szSubEs (lmm_sfRead szTmp) ",")))
	)
	(if (setq szTmp (cdr (assoc 360 cornerEnts)))  ;SA� biti�deki eleman kodu
		(setq szSubEs (strcat szSubEs (lmm_sfRead szTmp) ","))
	)
	szSubEs
)
;;;;;ADEKO 17.6.2.beta7 den itibaren SIL SIL SIL SIL;;;;;;;;;;;;;;;

;;;;;ADEKO 17.6.2.beta8 den itibaren SIL SIL SIL SIL;;;;;;;;;;;;;;;
(defun baza (kod prof cornerEnts lstParam imalTipi szNote
			 / 
			 	bazaH bazaThick bant_L ptlst v:profileType szMData szMetraj dBoy
			 	szSubEs routeDists i route rutin ; isBaza
			) ;blok_list
    (init_it)
	
	;(setq isBaza T)
	(setq v:profileType "plinth")

    (setq szMetraj  (car lstParam)
          dBoy      (lmm_atofFormula (cadr lstParam))
		  rutin     (caddr lstParam)
          ;bazaH 	(caddr prof)  see below
    )
	
	(if (apply 'AND (mapcar '(lambda(x) (equal (type x) 'LIST)) prof))
		(setq prof (mapcar '(lambda(x) (mapcar '(lambda(y) (lmm_atofformula y)) x)) prof))
	)
	
    (setq szMData (convertStr
        (list kod 'BAZA
              (cons 'QUOTE (list prof)) ;prof
              cornerEnts
              lstParam
              imalTipi
              szNote)
    ))
;    (setq cornerEnts (cdr cornerEnts))

	;(setq v:profileType "plinth") ;flag for tac_iput
    (tac_iput)
    (setlayer "PLINTHS" 1)	
	(if (equal (type (getnth 1 prof)) 'STR)
		(setq prof (subst (lmm_convertToRealIfReal (lmm_fConvertString (lmm_sfread (getnth 1 prof)))) (getnth 1 prof) prof))
	)
    (setq prof (MakeProf prof T))
	
	(setq bazaH (apply 'max (mapcar 'cadr prof)))
	(setq bazaThick (- (apply 'max (mapcar 'car prof)) (apply 'min (mapcar 'car prof))))

	(regApp "DONTACTLIKEMODULE")

	(foreach route ptlst
		(sweep "PLINTHS" prof route T T)
		;(setq route (pOffset route (- ad_bazaY ad_ADDer)))
		(setq routeDists (calcSegLengths prof route))
		(setq bant_L (lengthOfRoute route))
		
		(setq szSubEs "")
		(if cornerEnts
			(setq szSubEs (getProfileSubEntCodes route cornerEnts))
		)
		(setq kod (lmm_sfRead kod) )
		(blok_tanim (parse$ kod) '(0 0 0) (entlast) 0.0)
		(set_Mdata (entlast)
			(append
				(list
					(cons k:MTYP "PLINTHS")
					(cons k:HEI bazaH)
					(cons k:DEP bazaThick)
					(cons k:billUnit (cdr (assoc szMetraj '(("mtul" . 2) ("size" . 3)))))
					(if (equal szMetraj "size")
						(cons k:LengthSize dBoy)
					)
					(if (and (not (equal szSubEs "")) (splitstr szSubEs ","))
						(cons k:subE szSubEs)
					)
					(if (> (- (length route) 2) 0)
						(cons k:cntCorners (- (length route) 2))
					)
					(cons k:CMtip "BAZA_PLINTH") ;imalTipi
					(cons k:Length bant_L)
					(cons k:CAuxRcps (convertStr (list (iniread ad_MOD-INI "lmm-Sets" "lmm-rcp-plinth"))))
				)
				(divXstr255 k:mData szMData)
				(divXstr255 k:Dists (convertStr routeDists))
			)
		)
		(entmod (append (entget (entlast)) (list (list -3 (cons "DONTACTLIKEMODULE" (list '(1070 . 0)))))))
		(if rutin (eval rutin))
	)
    (end_it)
)
;;;;;ADEKO 17.6.2.beta8 den itibaren SIL SIL SIL SIL;;;;;;;;;;;;;;;

;;;;;ADEKO 17.6.2 den sonraki setuplarda SIL SIL SIL SIL;;;;;;;;;;;;;;;
(defun tac_iput ( / 1mod kot dummy qwe defaultInput upLimit ii tempmydistance upLimit ) ;i
;    (setq 1mod (_entselRAY (Xstr "\n<ENTER=Pick point>/Starting unit: ") 1))
	(init_it)
	(setq upLimit  (tac_iput::setUplimit))
	(if (equal isForOM 1)
		(setq ptlst 
				(eval 
					(cdr 
						(assoc v:profileType 
							'(
								("cornice" . (autocornice))
								("lightbaffle" . (autolightbaffle))
								("plinth" .  (autoplinth))
							)
						)
					)
				)
		)
	(progn	
		(if (or (and g_doNotUseAutoCornices (member v:profileType '("cornice" "lightbaffle" ))) 
				(and g_doNotUseAutoPLinth (member v:profileType '("plinth" )))
			)
			
			(setq 1mod (_initgetentselRAY 0 (strcat (Xstr "Manual") " " (Xstr "Replace")  " ~_Manual ~_Replace") (Xstr "\n<ENTER=Manual>/Replace/Starting unit: ") 1) defaultInput "Manual")
			
			(setq 1mod (_initgetentselRAY 0 (strcat (Xstr "Automatic Manual Replace")  " ~_Automatic ~_Manual ~_Replace") (Xstr "\n<ENTER=Automatic>/Manual/Replace/Starting unit: ") 1) defaultInput "Automatic")
		)

		(if (member 1mod (list nil RTNONE))
			(setq 1mod defaultInput)
		)
		
		(cond 
			((and (equal "Automatic" 1mod) (member v:profileType '("cornice" "lightbaffle" "plinth")))			
				(setq ptlst 
					(eval 
						(cdr 
							(assoc v:profileType 
								'(
									("cornice" . (autocornice))
									("lightbaffle" . (autolightbaffle))
									("plinth" .  (autoplinth))
								)
							)
						)
					)
				)
			)
			((equal "Manual" 1mod)
				(getSweepRoute nil (Xstr "\nOuter side: ") 'ptlst 'bant_L nil)
				(setq kot (caddar ptlst))
				(setq dummy (getreal (sprintf (Xstr "\nInsertion elevation <~d>: ") kot)))
				(if dummy (setq kot dummy))
				(setq ptlst (list (mapcar '(lambda (pt) (xyofz pt kot)) ptlst)))
			)
			((equal "Replace" 1mod)
				(setq replaceSelected 1)
				; (if (equal v:profileType "cornice")
						; (replaceCornice (Xstr "\nSelect cornice: ") )
						; (replaceCornice (Xstr "\nSelect light baffle: ") )
				; )
				 ;(if ptlst (setq replaceSelected 1))
			)
			(T
				(GetRota 1mod)
				(setq ptlst (list ptlst))
			)
		)
		(if upLimit
			(progn
				(setq ii 0)
				(foreach ptlistset ptlst
					(setq ptlstlen (length ptlistset))
					(repeat (- ptlstlen 1)
						(if (> (setq tempmydistance (distance (getnth (rem ii ptlstlen) ptlistset) (getnth (rem (+ ii 1) ptlstlen) ptlistset))) upLimit)
							(progn
								(alert (sprintf (xstr "Maximum length limit is ~f .\n\ Current one exceeds by ~f .") (list upLimit tempmydistance)))    
								(exit)
							)
						)
						(setq ii (+ 1 ii))		
					)
				)
			)
		)
	))
	(end_it)
)
;;;;;ADEKO 17.6.2 den sonraki setuplarda SIL SIL SIL SIL;;;;;;;;;;;;;;;

(defun GetRota (1mod / 2mod Nmod tackot pt i
                        ins1 sapla1
                        ins2 sapla2
                        modQue filterOut gr:ssFiltered
						k:prf
						prf1
						prf10
						mainIndex
                    )
	(gr:CompleteInputs)

	(if (equal v:profileType "plinth")
		(setq k:prf k:wb)
		(setq k:prf k:wp)
	)
	
	(setq filterOut
		(cond 
			((equal v:profileType "cornice") ; isTac                                  ;Kapamas lies on KAPAK
				'((-4 . "<OR") (8 . "CAB_BODY_WALL") (8 . "USTADOLAP") (8 . "CAB_BODY_TALL") (8 . "BOYADOLAP") (8 . "CAB_DOORS") (8 . "PANELS_SIDE") (8 . "SHELVES") (-4 . "OR>"))
			)
			((equal v:profileType "ledLight") ; is led light                                  ;Kapamas lies on KAPAK
				'((-4 . "<OR") (8 . "CAB_BODY_WALL") (8 . "USTADOLAP") (8 . "CAB_BODY_TALL") (8 . "BOYADOLAP") (8 . "CAB_DOORS") (8 . "PANELS_SIDE") (8 . "SHELVES") (-4 . "OR>"))
			)
			((equal v:profileType "plinth") ;isBaza
				'((-4 . "<OR") (8 . "CAB_BODY_BASE") (8 . "ALTADOLAP") (8 . "CAB_BODY_TALL") (8 . "BOYADOLAP")  (8 . "CAB_DOORS") (8 . "PANELS_SIDE") 			 		(-4 . "OR>"))
			)
			(T  ;���k band�
				'((-4 . "<OR") (8 . "CAB_BODY_WALL") (8 . "USTADOLAP")                 					 		(8 . "CAB_DOORS") (8 . "PANELS_SIDE") (8 . "SHELVES") (-4 . "OR>"))
			)
		)	
	)
	(setq gr:ssFiltered (ssget "X" filterOut))
    (setq modQue (getRota:findSequence 1mod 2mod))
	(if (null modQue)
        (progn
            (swap '1mod '2mod)
			(swap 'sapla1 'sapla2)
			(setq gr:ssFiltered nil)
			(setq gr:ssFiltered (ssget "X" filterOut))
			(setq modQue (getRota:findSequence 1mod 2mod))
        )
	)
	(if (null modQue)
		(progn (uyar (Xstr "Gap percieved between units.") "s") (exit))
	)
	
	;bazalar geriden ba�las�n �eklinde ayarland�. Fakat yanj d�n��ler de i�ten ba�l�yor. Yan d�n��lerin tam kenarda olabilmesi i�in 5.5 birim �teleme i�lemi yap�ld�.
	(setq modQue (reverse modQue))
	(setq prf1 (xyofz (get_Mdata (car modQue) (k:prf 1)) tackot))

    (if sapla1 (setq ptlst (list (if (and (equal v:profileType "plinth") (member (get_Mdata (car modQue) k:mtyp) (list "AD" "BD"))) (polar prf1 (+ DEG_180 (dfetch 50 (car modQue))) 5.5)  prf1))))

	(setq mainIndex 1)
	(foreach Nmod modQue
        (setq i 2)
        (while (<= i 9)
            (if (setq pt (if (and sapla1 (equal v:profileType "plinth") (equal i 2) (equal mainIndex 1) (member (get_Mdata Nmod k:mtyp) (list "AD" "BD"))) (polar (get_Mdata Nmod (k:prf i)) (+ DEG_180 (dfetch 50 Nmod)) 5.5) (if (and sapla2 (equal v:profileType "plinth") (equal i 9) (equal mainIndex (length modQue)) (member (get_Mdata Nmod k:mtyp) (list "AD" "BD"))) (polar (get_Mdata Nmod (k:prf i)) (dfetch 50 Nmod) 5.5) (get_Mdata Nmod (k:prf i)))))
                (setq ptlst (append ptlst (list (xyofz pt tackot))))
            )
            (i++)
        )
		(setq mainIndex (+ mainIndex 1))
	)
	(setq prf10 (xyofz (get_Mdata (last modQue) (k:prf 10)) tackot))
    (if sapla2 (setq ptlst (append ptlst (list (if (and (equal v:profileType "plinth") (member (get_Mdata (last modQue) k:mtyp) (list "AD" "BD")))  (polar prf10 (dfetch 50 (last modQue)) 5.5) prf10)))))
	
    (setq ptlst (ayikla ptlst))  ;e� noktalari sil
    (setq ptlst (ayikla2 ptlst)) ;eliminate 180 & 360d vertii
)

(defun dp:createProfileDims (/ rb67 ppeget fromEnt route dists i p1 p2 p3 ssProfileGroup)
	(setq rb67 (assoc 67 eget))
	(if rb67
		(setq ppeget (subst '(67 . 1) rb67 eget))
		(setq ppeget (cons '(67 . 1) eget))
	)
	
	(setq fromEnt (entlast))
	(entmake ppeget)
	
	(setq route (read (get_Mdata ppeget k:route)))
	(if eseypiyegicigimabi
		(setq dists (read (get_Mdata ppeget k:RDists)))
		(setq dists (read (get_Mdata ppeget k:Dists)))
	)

	(setlayer "ROUTE_PLINE" 7)
	(command "_.POLYLINE") (mapcar 'command route) (command "")
	
	(command "_.TEXT" (car route) (* 2 (getvar "DIMTXT")) 0.0 blockName)
	
	(setlayer "0" 3) ;;;;;-> SOPRANO, RENG� S�YAH GELS�N �STED�. BEN DE LAYER I 0 A �EKT�M
	(setq i 0)
	(repeat (1- (length route))
		(setq p1 (nth i route))
		(setq p2 (nth (1+ i) route))
		(setq p3 (polar p1 (- (angle p1 p2) DEG_90) (* 2 (getvar "DIMTXT"))))
		(command "_.DIMALIGNED" p1 p2 "_TEXT" (sprintf "<> [~f]" (nth i dists)) p3)
		(i++)
	)
	(setq ssProfileGroup (collectMents fromEnt))
	(command "_.-GROUP" "" "*" "" ssProfileGroup "")
	(command "_.ZOOM" "_EXT")
)

(defun gm1_selectToCreate ( / isHboy)	
	(setq isHboy (> (+ (lmm_atofFormula BDH) ad_bazaH) ad_UDY))

	(cond (g_gm1_Preview
			(setq gm1:layerName "GM1PREVIEW")			
		  )
	
			(mozel_mUnit
			  (gm1_defaultSetPossibleDirection)
              (setq yp1 mozel_yp1
                    aci mozel_aci
                    gm1:layerName (if isHboy "CAB_BODY_TALL" "CAB_BODY_BASE")
                    szModulType   (if isHboy "BD" "AD")	
					szCustomerSpesific   (get_Mdata mozel_mUnit k:customerSpesific)

                    notch_type  (get_Mdata mozel_mUnit k:NotchType)
                    notch_dims  (get_Mdata mozel_mUnit k:NotchDims)					
              )
              (entdel mozel_mUnit)
          )
          (notch_mUnit
			  (gm1_defaultSetPossibleDirection)
              (setq yp1 (dfetch 10 notch_mUnit)
                    aci (dfetch 50 notch_mUnit)
                    tmp       (get_Mdata notch_mUnit k:bzH)
                    gm1:layerName (if isHboy "CAB_BODY_TALL" "CAB_BODY_BASE")
                    szModulType   (if isHboy "BD" "AD")	
					szCustomerSpesific   (get_Mdata notch_mUnit k:customerSpesific)
					;possibleDirection (get_Mdata notch_mUnit k:myon)
                    notch_type  (get_Mdata notch_mUnit k:NotchType)
                    notch_dims  (get_Mdata notch_mUnit k:NotchDims)
              )
              (setq tmp (if tmp (xyofz yp1 tmp) yp1))			  
			  (if (null ADDer) (setq ADDer ad_ADDer))
              (if (notchDlg tmp aci BDEN BDH ADDer)
				(progn
					(if (equal notch_type 3) ;;;soprano, 3 nolu kertmenin yap�labilmesini istemedi�i i�in bu k�s�mda d�zenleme yap�ld�.
						(progn
							(alert (Xstr "You aren't allowed to perform this notch type!"))
							(exit)
						)
						(entdel notch_mUnit)
					)
				)
                  (exit)
              )
          )
          (stretch_mUnit
			  (gm1_defaultSetPossibleDirection)
              (setq yp1 stretch_yp1
                    aci stretch_aci
                    gm1:layerName (if isHboy "CAB_BODY_TALL" "CAB_BODY_BASE")
                    szModulType   (if isHboy "BD" "AD")	
					szCustomerSpesific   (get_Mdata stretch_mUnit k:customerSpesific)					
                    notch_type  (get_Mdata stretch_mUnit k:NotchType)
                    notch_dims  (get_Mdata stretch_mUnit k:NotchDims)
					
              )
              (entdel stretch_mUnit)
          )
          (T
			  (displayFlush)
			  (if (and g_gm1_yp1 g_gm1_aci g_gm1:layerName g_gm1_modulType)
				(progn
					(setq yp1 g_gm1_yp1 aci g_gm1_aci gm1:layername g_gm1:layerName szModulType g_gm1_modulType )
					(setq g_gm1_yp1 nil g_gm1_aci nil g_gm1:layerName nil g_gm1_modulType nil)
				)
				(gm1_decideBlockInputs how2put BDEN BDH ADDer) ;sets gm1:layerName too			  				
			   )

			   
			  (if (and  (not (null yp1)) (not (null aci)) BDEN BDH ADDer)
				(if (> g_iNotchMode 0)					  
							(checkInterference yp1 aci BDEN BDH ADDer)
							(progn
								(setq notch_type 0)
								(setq notch_dims (list 0 0 0))
							)
				)				  
				(exit)
			   )
          )
    )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ADEKO17.6.11.beta5 ten sonraki setuplarda SIL SIL SIL SIL SIL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun WARD_INS_explodeGroupRutin ( / tempSil)
	(lmm_setNoMuttEnabled)
	(setq  tempSil (entlast))
	(command "_.EXPLODE" (entlast))
	(setq tempSil (collectMents tempSil))
	(command "_.-GROUP" "" "*" "" tempSil "") 
	(setq tempSil nil)
	(lmm_setNoMuttDisabled)
)

(defun lmm_wasop_sawmap (outFile outputType params / tempmodulCode panelList panelName xmlMode)
	(setq ssobjMain (lmm_getModulsSS))
	(if (not (or (null ssobjMain) (< (sslength ssobjMain) 1)))
		(progn
			(setq iz 0)
			(setq xmlMode (getnth 0 params) )
					
			(setq tempmodulCode (lmm_wasop_sawmap_writeModul outFile outputType))
			
			(setq panelList (convertSStoList (ssget "x" (list   (list -3  (list (strcat lmm-l-type "PANEL")) )))))
			
			(foreach panelName  panelList
				(lmm_wasop_sawmap_assayPANELs panelName outFile outputType )
			)
					
			(if (not (equal xmlMode 1))
				(progn
					(setq itemList (convertSStoList (ssget "x" (list   (list -3 (list (strcat lmm-l-type "ITEM")) )))))
					
					(foreach itemName  itemList
						(lmm_wasop_sawmap_assayITEMs itemName outFile outputType )
					)
				)
			)
			(setq iz (1+ iz))	
		)
		(if (null lmm_createCRMData::continue) 
			(progn
				(alert (Xstr "Nothing to extract!"))
				(exit)
			)
		)
	)
)

(defun lmm_createCRMData (/ lmm_createCRMData::continue)
	; License control for CRM settings
	(if (not (member T (list (controlFeature AF_U1) (controlFeature AF_U2) (controlFeature AF_U3) (controlFeature AF_U4) (controlFeature AF_U5) (controlFeature AF_U6))))
		(setq lmm-s-CrmXmlType "lmm_CrmXmlTypeModular")
	)
	
	(purgeAndwriteBillItemsToXML)

	(cond ((equal lmm-s-CrmXmlType "lmm_CrmXmlTypeModular")
			(princ)
		  )
		  ((equal lmm-s-CrmXmlType "lmm_CrmXmlTypePart2CamBased")
		    (lmm_runMacro)		
			(lmm_OLD_correctGrainOfPanelsMetarial)
			
			(lmm_doPanelMirror)
			(lmm_doMainRotation)
			
			(lmm_doCutListRotation)
		  )
		  (T 
			(princ)
			;no change
		  )
	)
	
	(setq lmm_createCRMData::continue T)
	(lmm_writeAndSendCRM_createOutPutXMLFile 0 "btool")
	(dos_execute (strcat "explorer.exe \"" (lmm_prepareCRMOutDirs) "\""))
	(lmm_clearDrawings)
)

(defun soprano_changeWorktopCodes ( / firstPickStyle worktops worktopWid worktopCode worktop oldCodeData pureCode codeIndex)
	(setq firstPickStyle (getvar "PICKSTYLE"))
	(setvar	"PICKSTYLE" 0)
	(setq worktops (ssget "x" (list '(0 . "POLYLINE") '(8 . "WORKTOPS") '(-3 ("AD_MODATA")))))
	(setvar	"PICKSTYLE" firstPickStyle)
	
	(foreach worktop (convertsstolist worktops)
		(if (setq worktopWid (get_mdata worktop k:tezGen))
			(progn
				(setq worktopCode
					(if (<= worktopWid 64.0)
						"TZG0030"
						"TZG0030G"
					)
				)
				
				(setq oldCodeData (splitstr (get_mdata worktop k:code) "$"))
				(setq codeIndex (cadr oldCodeData))
				(set_Mdata worktop  (list (cons k:code (strcat worktopCode "$" codeIndex))))
			)
		)
	)
)

(defun lmm_start ( productionInfo / lmm-ProjectName lmm-ProjectDesc lmm-ProjectDate lmm-ProjectMultiple unUseTemp lmm-outPutType ssobjMain tmpFile lmm-modulesToProcess lmm-btoolArguments g_objectPoseCounter hiddenDoorsSS)	
	(if (null *debug*)
		(setq *error* lmm_doWhenError)
	)
	
	(soprano_changeWorktopCodes) ;sopranonun iste�i �zerine

	;If there is hidden entities, make them visible before process them
	(setq ss (ssget "x" '((8 . "{GIZLI-CEPHE}"))))
	(kesme "{GIZLI-CEPHE}" ss)
	
	(setq hiddenDoorsSS (ssget "x" '((8 . "{HIDDEN_WDOORS}"))))
    (if (and hiddenDoorsSS (> (sslength hiddenDoorsSS) 0) (yesno (_ (XSTR "There are hidden wardrobe doors. Would you like them to be processed?"))))
		(progn
			(showWardrobeDoors)
		)
	)

	(mapcar 'set '(lmm-ProjectName lmm-ProjectDesc lmm-ProjectDate lmm-ProjectMultiple unUseTemp unUseTemp lmm-outPutType lmm-btoolArguments ) productionInfo)
	(command "_.2DWIRE")
	(purgeUBlocks)

	(princ (strcat "\n" (Xstr "Please Wait")))
	(setq ssobjMain (lmm_getModulsSS))
	
	(lmm_explodeGroupConns)
	(setq lmm-modulesToProcess (lmm_askModulesToProcess))
	 
	(displaySuppress) 
	 
	(cond ((equal lmm-outPutType 0)			(if (equal lmm-s-createAllReports "1") (lmm_exportAllData) (lmm_exportAllDxfsAndGCodes)))
		  ((equal lmm-outPutType 1)			(princ))
		  ((equal lmm-outPutType 2)			(lmm_createOnlyDrawings))
		  ((equal lmm-outPutType 3)			(lmm_createOnlyDrawingsFile))			  
		  ((equal lmm-outPutType 4)			(lmm_exportOptimik))				
		  ((equal lmm-outPutType 5)			(lmm_exportBtool lmm-btoolArguments 0))				
		  ;kesim listesi
		  ((equal lmm-outPutType 6)			(lmm_exportBtool lmm-btoolArguments 1))							
		  ;fast Pricing
		  ((equal lmm-outPutType 7)			(lmm_exportFastPricing lmm-btoolArguments))
			;Cut Plan
		  ((equal lmm-outPutType 8)			(lmm_exportCutPlan))
		 	;CRM
		  ((equal lmm-outPutType 9)			(lmm_createCRMData))				
	)
	(displayFlush) ;;liberate i�levsel olsun diye daha �nceden liberate yap�lm��sa diye tekrar suppress
	(displayLiberate)
	
	;Make entities hidden again those made visible before process
	(kes "{GIZLI-CEPHE}" ss)
	
	(princ (strcat "\n" (Xstr "OK")))
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun listDoorModelAndTypesIn (insEnt typeFilter handleFilter / subEnt listDModels doorName tempEnt)
	(setq subEnt (cdr (assoc -2  (tblsearch "block" (dfetch 2 insEnt)))))
	(while subEnt
		(if (isEntDoor subEnt typeFilter handleFilter)
			(progn
				(setq doorName nil)
				(setq tempEnt (getDoorEntFromDoorId_C  (get_mdata subEnt k:CDoorId) (getEntPro insEnt 2)) )	
				(kpk:getData (getEntPro tempEnt 2)  'tip 'kulp 'doorName 'EN 'BOY nil nil nil nil)
				(if (or (null doorName) (equal doorName ""))
					(progn
						(setq doorName (GetKapakNameFor tip kulp))
					)
				)	
				
				(if doorName (setq listDModels (append listDModels (list (list doorName tip)))))
			)
		)
		(setq subEnt (entnext subEnt))
	)
	(reverse listDModels)
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ADEKO17.6.11.beta6 ten sonraki setuplarda SIL SIL SIL SIL SIL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun gm1_createMData ( / )
	(setq szMData (convertStr
        (list kodFormula 'GM1
            BDEN BDHFormula
            ADDerFormula 
            (cons 'QUOTE (list (list how2put sBazaType simetri subEs_orj specialDoor unitCArea isManualCArea bodyauxRcpsList divsCommonRcpsList isSinkUnit bodyScriptFiles gm1rutin gm1Dims isModuleForAccesory)))
            (cons 'QUOTE (list divisionList))
            imalTipi
            note
        )
    ))
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun getDataFromFirmSpesific (entityName dataName / value dataStr firmData)
	(if 
		(and
			(setq dataStr (get_mdata entityName k:customerSpesific))
			(setq firmData (read dataStr))
		)
		(setq value (cadr (assoc dataName firmData)))
	)
	value
)

(defun setDataFromFirmSpesific (entityName datas  / dataStr firmData newData data repeatedData)
	(if 
		(and
			(setq dataStr (get_mdata entityName k:customerSpesific))
			(setq firmData (read dataStr))
		)
		(progn
			(setq newData firmData)
			
			(foreach data datas
				(if (setq repeatedData (assoc (car data) newData))
					(setq newData (removeat (getitemindex repeatedData newData) newData))
				)
				(setq newData (append newData (list data)))
			)
		)
		(progn
			(setq newData datas)
		)
	)
	(if newData
		(set_Mdata entityName  (list (cons k:customerSpesific (convertstr newData))))
	)
	(princ)
)

(defun soprano_spot (dummyCode dummyParams / spotCodes mode szSupplier 1mod ortomod ptIns Z temp block_list spotCode position yerTip)
    (init_it)
	(if (setq spotCodes (mapcar '(lambda(x) (strcase (car x))) g_soprano_spots))
		(progn
			(if (setq spotCode (spot:getSpotCode))
				(progn
					(setq temp (findblock "spot3_"))
					(setlayer "SPOT_LIGHT_HARICI" 6)
					
					(setq spotBlkNam (nxtBnam_v2 spotCode))
					(INSERTAS
							(cond 
							((findfileex (strcat ad_DefPath "TEFRIS\\LIGHTS\\" "spot3_" ".DWG")))
							((findfileex (strcat ad_AdekoPath "TEFRIS\\LIGHTS\\" "spot3_" ".DWG")))
						) 
						spotBlkNam
					)
					(setq ptIns (getpoint (Xstr "\nInsertion point: ")))
					(if (null ptIns) (exit))
					(command cmdINSERT spotBlkNam ptIns "" "" 0.0)
					(setq spotEnt (entlast))

					(setlayer "SPOT_LIGHT_TEXT" 6)
					(setq recentTextSize (getvar "TEXTSIZE"))
					(command "_.TEXT" '(0 0 0.5) 2 0.0 spotCode)
					(setvar "TEXTSIZE" recentTextSize)
					(combinesub_V2 (collectents spotEnt) spotEnt)
				)
			)
		)
	)
    (end_it)
)

(defun spot:getSpotCode ( / code)
	(initget (createInitgetStringForSpots))
	(setq code (getkword "\nModel Se�...\n"))
	(if code (if (member code spotCodes) code) (exit))
)

(defun attach_spot_soprano (1mod dwgnam mode / ptIns dAng dWid dHei dDep temp isCorner k block_list entsLed spotEnt recentTextSize)
	(setq ptIns '(0 0 0)
		  dAng  0.0
		  dWid  (get_Mdata 1mod k:WID)
		  isCorner (iskose 1mod)
	)

	(if isCorner
		(setq dAng (- dAng (* 0.5 pi/2)))
		(setq ptIns (polar ptIns dAng (* 0.5 dWid))
			  dAng (- dAng pi/2)
		)
	)

	(if (> mode 0)
		(progn
			(setq dHei  (get_Mdata 1mod k:HEI)
				  dDep  (get_Mdata 1mod k:DEP)
			)

			(setq k (if isCorner
						(if (equal "UK3" (get_Mdata 1mod k:MTYP))
							1.7
							1.4142
						)
						1.0
					)
			)

			(cond 
				((equal mode 2)
					(setq ptIns (polar (xyofz ptIns (+ (caddr ptIns) dHei)) dAng (* k (+ dDep (cm 8.0)))))
				)
				((equal mode 1)
					(setq ptIns (polar ptIns dAng (* k (- dDep (cm 8.0)))))
				)
				((equal mode 3)
					(setq ptIns (polar (xyofz ptIns (+ (caddr ptIns) dHei)) dAng (* k (- dDep (cm 4.2)))))
				)
				((equal mode 4)
					(setq ptIns (polar (xyofz ptIns (+ (caddr ptIns) dHei (- ad_ET))) dAng (* k (- dDep (cm 8.0)))))
				)
			)
		)
	)

	(setq block_list (entlast))
	(setlayer (strcat "SPOT_LIGHT_" position) 6)
	
	(setq spotBlkNam (nxtBnam_v2 spotCode))
	(INSERTAS
			(cond 
			((findfileex (strcat ad_DefPath "TEFRIS\\LIGHTS\\" dwgnam ".DWG")))
			((findfileex (strcat ad_AdekoPath "TEFRIS\\LIGHTS\\" dwgnam ".DWG")))
		) 
		spotBlkNam
	)
	
	(command cmdINSERT spotBlkNam ptIns "" "" (r2d (- dAng DEG_180)))
	(setq spotEnt (entlast))

	(setlayer "SPOT_LIGHT_TEXT" 6)
	(setq recentTextSize (getvar "TEXTSIZE"))
	(command "_.TEXT" '(0 0 0.5) 2 (r2d dAng) spotCode)
	(setvar "TEXTSIZE" recentTextSize)
	(combinesub_V2 (collectents spotEnt) spotEnt)

	(setq entsLed (collectents block_list))
	(combinesub_V2 entsLed 1mod)
)

(defun createInitgetStringForSpots ( / response1 response2 code index lastString)
	(setq index 1)
	(foreach code spotCodes
		(setq lastString (if (equal (length spotCodes) index) "" " "))
		(setq response1 (strcat (ifnull response1 "") code lastString))
		(setq response2 (strcat (ifnull response2 " ") "~_" code lastString))
		(setq index (+ index 1))
	)
	(strcat response1 response2)
)

(defun createInitgetStringForPosition (code / spotData availableDown downString1 upString1 downString2 upString2)
	(setq spotData (getnth (- (length spotCodes) (length (member code spotCodes))) g_soprano_spots))
	(setq downString1 (if (cadr spotData) "ALT " ""))
	(setq upString1 (if (caddr spotData) "UST " ""))
	(setq downString2 (if (cadr spotData) "~_ALT " ""))
	(setq upString2 (if (caddr spotData) "~_UST " ""))

	(strcat downString1 upString1 " " downString2 upString2)
)

(defun getSpotState (unitEntName / blockEntName laynam found res)
	(setq blockEntName (tblobjname "BLOCK" (dfetch 2 unitEntName)))
	
	(while (setq blockEntName (entnext blockEntName))
		(setq laynam (dfetch 8 blockEntName))
		(setq found (assoc laynam '(("SPOT_LIGHT_ALT" "SPOT_LIGHT_ALT") ("SPOT_LIGHT_UST" "SPOT_LIGHT_UST"))))
		(if found
			(setq res (cons (cadr found) res))
		)
	)
	
	(removeDuplicated res)
)

(defun getSpotData (unitEntName / blockEntName laynam res)
	(setq blockEntName (tblobjname "BLOCK" (dfetch 2 unitEntName)))
	(while (setq blockEntName (entnext blockEntName))
		(setq laynam (dfetch 8 blockEntName))
		(if (member laynam '("SPOT_LIGHT_ALT" "SPOT_LIGHT_UST"))
			(setq res (append res (list (list (parse$ (dfetch 2 blockEntName)) (cond ((equal laynam "SPOT_LIGHT_ALT") "DOWN") ((equal laynam "SPOT_LIGHT_UST") "UP"))))))
		)
	)
	res
)

(defun getSopranoLedLightsData (unitEntName / blockEntName laynam ledInfo res)
	(setq blockEntName (tblobjname "BLOCK" (dfetch 2 unitEntName)))
	(while (setq blockEntName (entnext blockEntName))
		(setq laynam (dfetch 8 blockEntName))

		(if (member laynam '("LED_LIGHT_ALT" "LED_LIGHT_YAN"))
			(progn
				(setq ledInfo (splitstr (parse$ (dfetch 2 blockEntName)) "&"))
				(setq res (append res (list (list (car ledInfo) (cadr ledInfo) (cond ((equal laynam "LED_LIGHT_ALT") "DOWN") ((equal laynam "LED_LIGHT_YAN") "SIDE"))))))
			)
		)
	)
)

(defun rutin_LedLight:attach_led_soprano (1mod mode / cutL ptIns dAng dWid dHei dDep isCorner pt1 pt2 mtype recentTextSize entFlag ledBlkNam ledLayer mData)
	(setq
		cutL  3.7
		ptIns '(0 0 0)
		dAng  0.0
		dWid  (get_Mdata 1mod k:WID)
		dHei  (get_Mdata 1mod k:HEI)
		cHei  (get_mdata 1mod  k:CBoy)
		dDep  (get_Mdata 1mod k:DEP)
		isCorner (iskose 1mod)
		mtype (get_Mdata 1mod k:MTYP)
		mData (get_mdata 1mod k:mData)
	)
	
	(if (equal (cadr (read mData)) 'GM1)
		(progn
			(setq ledLayer (strcat "LED_LIGHT_" rutin_LedLight:position))
			(setq entFlag (entlast))
			(cond 
				((equal mode 2) ;alt
					(setq pt1 (polar (replace (* 0.5 cutL) 0 (replace (+ -0.2 (- dHei cHei)) 2  ptIns)) (- dAng DEG_90) (/ dDep 2.0)))
					(setq pt2 (polar pt1 dAng (- dWid cutL)))
					
					(setlayer ledLayer 6)
					(drawLed 3.0 nil nil nil nil (list pt1 pt2))
					
					(setlayer "LED_LIGHT_TEXT" 6)
					(setq recentTextSize (getvar "TEXTSIZE"))
					(command "_.TEXT" (polar pt1 (angle pt1 pt2)  (/ (distance pt1 pt2) 2.0)) 2 (r2d dAng) rutin_LedLight:ledCode)
					(setvar "TEXTSIZE" recentTextSize)
					
					(setq ledBlkNam (nxtBnam_v2 (strcat rutin_LedLight:ledCode "&" (convertstr (- dWid cutL)))))
					(command "_.BLOCK" ledBlkNam "0,0,0" (collectents entFlag) "")
					(setlayer ledLayer 6)
					(command cmdINSERT ledBlkNam '(0 0 0) "" "" 0.0)
					
					(combinesub_V2 (collectents entFlag) 1mod)
				)
				((equal mode 1) ;yan
					(UCS_Y 90.0)
					(setq pt1 (replace (* 0.5 cutL) 0 (replace (+ 2.0 (zof ptIns)) 2  ptIns)))
					(setq pt2 (replace (/ dDep -2) 1 pt1))
					(setq pt2 (replace (- cHei dHei (* 0.5 cutL)) 0 pt2))
					(setq pt1 (replace (- (* 0.5 cutL) dHei) 0 pt2))
					
					
					
					(setlayer ledLayer 6)
					(drawLed 3.0 nil nil nil nil (list pt1 pt2))
					
					(setlayer "LED_LIGHT_TEXT" 6)
					(setq recentTextSize (getvar "TEXTSIZE"))
					
					(command "_.TEXT" (polar pt1 (angle pt1 pt2)  (/ (distance pt1 pt2) 2.0)) 2 (r2d dAng) rutin_LedLight:ledCode)
					(setvar "TEXTSIZE" recentTextSize)
					
					(UCS_W)
					(setq ledBlkNam (nxtBnam_v2 (strcat rutin_LedLight:ledCode "&" (convertstr (- cHei cutL)))))
					(command "_.BLOCK" ledBlkNam "0,0,0" (collectents entFlag) "")
					 (setlayer ledLayer 6)
					(command cmdINSERT ledBlkNam '(0 0 0) "" "" (r2d dAng))
					
					(combinesub_V2 (collectents entFlag) 1mod)
					
					(UCS_Y 90.0)
					(setq pt1 (replace (* 0.5 cutL) 0 (replace (- dWid 2.0) 2  ptIns)))
					(setq pt2 (replace (/ dDep -2) 1 pt1))
					(setq pt2 (replace (- cHei dHei (* 0.5 cutL)) 0 pt2))
					(setq pt1 (replace (- (* 0.5 cutL) dHei) 0 pt2))
					
					(setlayer ledLayer 6)
					(drawLed 3.0 nil nil nil nil (list pt1 pt2))
					
					(setlayer "LED_LIGHT_TEXT" 6)
					(setq recentTextSize (getvar "TEXTSIZE"))
					
					(command "_.TEXT" (polar pt1 (angle pt1 pt2)  (/ (distance pt1 pt2) 2.0)) 2 (r2d dAng) rutin_LedLight:ledCode)
					(setvar "TEXTSIZE" recentTextSize)
					
					(UCS_W)
					(setq ledBlkNam (nxtBnam_v2 (strcat rutin_LedLight:ledCode "&" (convertstr (- cHei cutL)))))
					(command "_.BLOCK" ledBlkNam "0,0,0" (collectents entFlag) "")
					 (setlayer ledLayer 6)
					(command cmdINSERT ledBlkNam '(0 0 0) "" "" (r2d dAng))
					
					(combinesub_V2 (collectents entFlag) 1mod)
					
				)
			)
		)
		(progn
			(alert "Led ���klar�, bu mod�l tipi i�in desteklenmiyor. K�t�phane y�neticiniz ile ileti�ime ge�in.")
		)
	)
)

(defun getLedState (unitEntName / blockEntName laynam found res)
	(setq blockEntName (tblobjname "BLOCK" (dfetch 2 unitEntName)))
	
	(while (setq blockEntName (entnext blockEntName))
		(setq laynam (dfetch 8 blockEntName))
		(setq found (assoc laynam '(("LED_LIGHT_ALT" "LED_LIGHT_ALT") ("LED_LIGHT_YAN" "LED_LIGHT_YAN"))))
		(if found
			(setq res (cons (cadr found) res))
		)
	)
	
	(removeDuplicated res)
)


(defun rutin_LedLight:createInitgetStringForLeds ( / response1 response2 code index lastString)
	(setq index 1)
	(foreach code rutin_LedLight:ledCodes
		(setq lastString (if (equal (length rutin_LedLight:ledCodes) index) "" " "))
		(setq response1 (strcat (ifnull response1 "") code lastString))
		(setq response2 (strcat (ifnull response2 " ") "~_" code lastString))
		(setq index (+ index 1))
	)
	(strcat response1 response2)
)

(defun rutin_LedLight:getLedCode ( / code)
	(initget (rutin_LedLight:createInitgetStringForLeds))
	(setq code (getkword "\nModel Se�...\n"))
	(if code (if (member code rutin_LedLight:ledCodes) code) (exit))
)

(defun nxtBnam_V2 (kod / defined blok_nam i)
  	(purgeUBlocks)
    (setq defined t i 1)
    (setq kod (strrep kod " " "_")) ;replace spaces wis _ if any

    (while defined
        (setq defined (tblsearch "block" (setq blok_nam (strcat kod "$" (rtos i 2 0)))))
        (i++)
    )
    blok_nam
)

(defun combinesub_V2 (subs main / enxt)
	(setq enxt (entlast))
    (command cmdINSERT (strcat "*" (dfetch 2 (entget main))) '(0 0 0) "" "")
    (idefBLOCK (dfetch 2 main) '(0 0 0) (sscat (collectMents enxt) subs))
)

(defun getKlapaHeight ( / kHeight klapaHeight)
	(if (checkstrinstr "{(KLAPAHEIGHT)}" (strcase (convertstr xstrdata)))
		(if (new_dialog "setKlapaHeight" (load_dialog (strcat ad_defpath "firmfaces.dcl")))
			(progn
				(setq kHeight (ifnull (if mozel_mUnit (getDataFromFirmSpesific mozel_mUnit 'klapaHeight) nil) 7.2))
				(setq ins2Dwg::userDefinedKlapaHeight kHeight)
				
				(set_tile "codeText" (car xstrdata))
				(set_tile "klapaHeight" (rtos kHeight))

				(action_tile "accept" "(setq kHeight (atof (get_tile \"klapaHeight\"))) (done_dialog 1)")

				(if (equal (start_dialog) 1)
					(if (equal kHeight 0.0)
						(getKlapaHeight)
						(setq ins2Dwg::userDefinedKlapaHeight kHeight)
					)
				)
			)
		)
	)
)

(defun setKlapaHeight (/ )
	(if ins2Dwg::userDefinedKlapaHeight (setDataFromFirmSpesific (entlast) (list (list 'klapaHeight ins2Dwg::userDefinedKlapaHeight))))
)

(defun KlapaHeight ( / kHeight)
	(ifnull ins2Dwg::userDefinedKlapaHeight 7.2)
)

;;;;;;;;;;;;;;;;Adeko 17.6.14.beta8 den sonraki setuplarda s�l s�l s�l s�l s�l;;;;;;;;;;;;;;;;;;;;;;;;;
(defun multiEvaluate (str / readData x)
	(setq readData (lmm_readEvenIfnil str))
	(if (and 
			(equal (type readData) 'LIST) 
			(apply 'AND (mapcar '(lambda(x) (equal (type x) 'LIST)) readData))
		)
		(mapcar '(lambda(x) (eval x)) readData)
		(eval readData)
	)
	(princ)
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;Adeko 17.6.14.beta8 den sonraki setuplarda s�l s�l s�l s�l s�l;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun GM1 (kod BDEN BDH ADDer params divisionList imalTipi note /
					how2put  sBazaType   simetri	   SubEs   specialDoor  unitCArea   isManualCArea  bodyauxRcpsList divsAuxRcpsListsList divsCommonRcpsList bodyScriptFiles gm1rutin isModuleForAccesory
                       ;Yerleşim noktası ve açısı get_inputs'dan geliyor
					szModulType notch_type notch_dims  yp1 aci
                    gm1:layerName      ;On which layer, modul to be put. Read from GetKapakNameFor to decide in which type the doors to be drawn                   
					rafTotal
					dikTotal
                    rDer        ;Referans modülün derinliği
                    yerlesimDer ;Önden bağlama varsa parçanın yerleşim derinliği. ADDer parçanın kendi başına ölçüldüğündeki deriniği olüyr monşer.
                    dAltKot 
					dAltKpkKot	;Kapakların alttan başlangıcı
					clearZTop clearZBtw clearXSide clearXBtwn clearZBot ;kapaklar arası boşluklar
					doorWidth leftMid rightMid
                    szMData H&Vlist                     
                    tmp plinthLegOffset ktip is2CekmeceArasi
					 shflayer et SubEs specialDoor unitCArea isManualCArea
					fixedShelf applianceShelf					
					isAppliance env_var
					drawCurrElev drawCurrDoorElev										
					divsRafDikOffsets
					drawersListWithRcp
					drawerOffsetList
					fixedShelfList
					doorsPropList
					isLRexist
					possibleDirection
					isSinkUnit
					bodyScriptFiles
					GM1-H 
					BDHFormula
					ADDerFormula
					doorHFormulation divHFormulation divDoorAnimateScript  moduleDwgListsList
					modulgolaOrderList
					GM1DIM0
					GM1DIM1
					GM1DIM2
					subEs_orj
					ptlst ptlstPlinth
					szCustomerSpesific
             )

	(isKitchenLicensed)
	;set parameters values to variables 	
	(getParamsGM1 params)
	
	(GM1_prepareGeneralFormulaParams 0)

	(if (null ADDer) (setq ADDer ad_ADDer))
	
	(gm1_setElevationOffsets)
	
	(gm1_selectToCreate)

	(gm1_setClearence how2put sBazaType BDH)		

	(gm1_addXDatas (gm1_drawALL kod BDEN BDH ADDer divisionList sBazaType ) )
	
	(multiEvaluate gm1rutin)
	(GM1_cleanFormulaParams)
	(princ)
	
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(firm_startup)
(princ)