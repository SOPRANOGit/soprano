firmfaces_customerInfo :dialog
{
	label="Project Detail Defination";
	:boxed_column
	{
	label="";
	:edit_box { label="Current Account:"; key="firmfaces_ci_f1"; width=100; edit_width = 95;}
	:edit_box { label="Current Name:"; key="firmfaces_ci_f2"; edit_width = 95;}
	}
	
	:boxed_column
	{
	label="Designer";
	:popup_list { label="Sales Architect:"; key="firmfaces_ci_f12"; edit_width = 95;}
	}
	
		:boxed_column
	{
	label="Customer";
	:edit_box { label="Name Surname:"; key="firmfaces_ci_f7"; edit_width = 95;}
	:edit_box { label="Delivery Address:"; key="firmfaces_ci_f3"; edit_width = 95;}
	:edit_box { label="District:"; key="firmfaces_ci_f4"; edit_width = 95;}
	:edit_box { label="City:"; key="firmfaces_ci_f5"; edit_width = 95;}
	:edit_box { label="Zip Code:"; key="firmfaces_ci_f6"; edit_width = 95;}
	:edit_box { label="Phone:"; key="firmfaces_ci_f8"; edit_width = 95;}
	:edit_box { label="E-Mail:"; key="firmfaces_ci_f9"; edit_width = 95;}	
	:toggle { label="Color Sample Will Be Sent"; key="firmfaces_ci_f10"; edit_width = 95;}
	:edit_box { label="Note:"; key="firmfaces_ci_f11"; edit_width = 95;}
	}
	
	:boxed_column
	{
	label="Project";
	:edit_box { label="Type:"; key="firmfaces_ci_f15"; edit_width = 95;}
	:edit_box { label="floor:"; key="firmfaces_ci_f16"; edit_width = 95;}
	:edit_box { label="Flat:"; key="firmfaces_ci_f17"; edit_width = 95;}
	:popup_list { label="Sales Shape:"; key="firmfaces_ci_f18"; edit_width = 94;}
	:popup_list { label="Order Type:"; key="firmfaces_ci_f19"; edit_width = 94;}
	:popup_list { label="Sales Attiribute:"; key="firmfaces_ci_f20"; edit_width = 94;}
	}
	ok_button;
}

selectDoorTypeDiolog :dialog
{
	label = "Door Type Combinations" ; 

      : row {	

			  :boxed_column {
							  label = "" ; 

							   : radio_button {
								  key = "comb1" ;				
								  label = "Plain - Plain" ;
													
										}					

								: radio_button {
								  key = "comb2" ;
								  label = "Glass - Glass" ;
										}
										
								: radio_button {
								  key = "comb3" ;
								  label = "Plain - Glass" ;
										}
							}
			ok_button;
			}
}

createSopranoPartCode :dialog
{	
	label = "Enter Soprano Unit Part Code" ; 
	:text { label="Type:"; key="partCodeText"; edit_width = 50;}
	:row
	{
		:edit_box { label=""; key="partCode"; edit_width = 50;}
	}
	  ok_button;
}

enterQuantityValueOfHandleAccessories :dialog
{
		label = "Kulp Adedi" ; 
		:boxed_column
		{
			:text { label=""; key="codeText"; edit_width = 30;}
			:edit_box { key="quantityValue";      label="Adedi:"; width=30;}
		}
	ok_button;
}

getBackPanelIsFronterData :dialog
{
	label = "Arkal�k Bilgisi" ;
	:boxed_column
	{
		:text { label=""; key="codeText"; edit_width = 30;}
		:toggle { label="Arkal�k �nde"; key="backPanelIsFronter";}
	}
	ok_button;
}

setKlapaHeight :dialog
{
	label = "Klapa Bilgisi";
	:boxed_column
	{
		:text { label=""; key="codeText"; edit_width = 30;}
		:edit_box { key="klapaHeight";  label="Klapa y�ksekli�i(cm):"; width=30;}
	}
	ok_button;
}

sinkCover :dialog
{
		label = "Evye �rt�s�" ; 
		:boxed_column
		{
			:text { label=""; key="codeText"; edit_width = 25;}
			spacer_1;
			:popup_list { key="plSinkCover"; edit_width = 25;}
		}
	ok_button;
}

bzdUstDolapMekanizma :dialog
{
		label = "Buzdolab� �st Dolap Mekanizmas�" ; 
		:boxed_column
		{
			:text { label=""; key="codeText"; edit_width = 25;}
			spacer_1;
			:popup_list { key="plbzdUstDolapMekanizma"; edit_width = 25;}
		}
	ok_button;
}

renkSec : dialog
{
	label = "Renk Se�";
	:boxed_column
	{
		:text { label=""; key="codeText"; edit_width = 25;}
		spacer_1;
		:popup_list { key="renk"; edit_width = 25;}
	}
	ok_button;
}