
(defun GR31AKANAT_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun GR31_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "GR31" "_" x "KANATSETS")) GR31_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun SW32AKANAT_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun SW32_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "SW32" "_" x "KANATSETS")) SW32_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun JY33AKANAT_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun JY33_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "JY33" "_" x "KANATSETS")) JY33_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun LG30ENTGRE_AKANATSETS(x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list nil PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun LG30CLASSIC_AKANATSETS(x z)
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun LG30_AKANATSETS(x z)
	(if (member  tempkulpmodel '("SP.ENTEGRE"))
	  (LG30ENTGRE_AKANATSETS x z)
	  (LG30CLASSIC_AKANATSETS x z)
	)
)

(defun LG30_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "LG30" "_" x "KANATSETS")) LG30_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun LG40ENTGRE_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list nil PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun LG40CLASSIC_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun LG40_SETS()
	(if (member  tempkulpmodel '("SP.ENTEGRE"))
	  (LG40ENTGRE_SETS)
	  (LG40CLASSIC_SETS)
	)
)

	(defun LG40_BKANATSETS(x z)	
		(setq doorFlags::p2cDoorSource "INSOURCE")
		(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
		(setq doorFlags::p2cFrameThick 5.0)
	)
	(mapcar '(lambda (x) (set (read (strcat "LG40" "_" x "KANATSETS")) LG40_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun BL50ENTGRE_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list nil PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun BL50CLASSIC_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(defun BL50_SETS()
	(if (member  tempkulpmodel '("SP.ENTEGRE"))
	  (BL50ENTGRE_SETS)
	  (BL50CLASSIC_SETS)
	)
)

	(defun BL50_BKANATSETS(x z)	
		(setq doorFlags::p2cDoorSource "INSOURCE")
		(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
		(setq doorFlags::p2cFrameThick 5.0)
	)
	(mapcar '(lambda (x) (set (read (strcat "BL50" "_" x "KANATSETS")) BL50_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun BL51ENTGRE_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list nil PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun BL51CLASSIC_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(defun BL51_SETS()
	(if (member  tempkulpmodel '("SP.ENTEGRE"))
	  (BL51ENTGRE_SETS)
	  (BL51CLASSIC_SETS)
	)
)

(defun BL51_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "BL51" "_" x "KANATSETS")) BL51_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun FR70ENTGRE_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list nil PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun FR70CLASSIC_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)
(defun FR70_SETS()
	(if (member  tempkulpmodel '("SP.ENTEGRE"))
	  (BL50ENTGRE_SETS)
	  (BL50CLASSIC_SETS)
	)
)

(defun FR70_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "FR70" "_" x "KANATSETS")) FR70_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun NT80ENTGRE_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list nil PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun NT80CLASSIC_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun NT80_SETS()
	(if (member  tempkulpmodel '("SP.ENTEGRE"))
	  (NT80ENTGRE_SETS)
	  (NT80CLASSIC_SETS)
	)
)

(defun NT80_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "NT80" "_" x "KANATSETS")) NT80_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun NT81ENTGRE_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list "ENTEGRE_KULP" nil nil nil))
	(setq doorFlags::p2cEdges (list nil PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun NT81CLASSIC_SETS()
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfileHandles (list nil nil nil nil))
	(setq doorFlags::p2cEdges (list PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK PvcKoduKAPAK))
	(setq doorFlags::p2cProfiles (list nil nil nil nil))
	(setq doorFlags::p2cFrameThick 55.0)
)

(defun NT81_SETS()
	(if (member  tempkulpmodel '("SP.ENTEGRE"))
	  (NT81ENTGRE_SETS)
	  (NT81CLASSIC_SETS)
	)
)

(defun NT81_BKANATSETS(x z)	
	(setq doorFlags::p2cDoorSource "INSOURCE")
	(setq doorFlags::p2cProfiles (list "PRF_" "PRF_" "PRF_" "PRF_"))
	(setq doorFlags::p2cFrameThick 5.0)
)
(mapcar '(lambda (x) (set (read (strcat "NT81" "_" x "KANATSETS")) NT81_BKANATSETS) ) (list "B" "K" "J" "F" "T"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
